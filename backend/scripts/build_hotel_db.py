import requests
from app import engine, City, Hotel
from sqlalchemy.orm import Session
import time
import json

request_distance = 15
request_unit = "KM"

# get access_token to access the amadeus api
def get_access_token():
    endpoint = "https://api.amadeus.com/v1/security/oauth2/token"
    data = {
        "grant_type": "client_credentials",
        "client_id": "WwAlcj4Jknns4D8SA7SCCVse3z3ySbeA",
        "client_secret": "agE7eZhk89uxLqgb",
    }
    response = requests.post(url=endpoint, data=data)
    json_data = response.json()
    return json_data["access_token"]


# get hotels by city
def get_hotels(latitude, longitude):
    token = get_access_token()
    url = (
        "https://api.amadeus.com/v2/shopping/hotel-offers?latitude="
        + str(latitude)
        + "&longitude="
        + str(longitude)
        + "&radius="
        + str(request_distance)
        + "&radiusUnits="
        + request_unit
    )
    headers = {"Authorization": "Bearer " + token}
    hotels = requests.get(url=url, headers=headers)
    return hotels.json()


def get_hotel_room(hotelId):
    token = get_access_token()
    url = "https://api.amadeus.com/v2/shopping/hotel-offers/by-hotel?hotelId=" + hotelId
    headers = {"Authorization": "Bearer " + token}
    hotel_rooms = requests.get(url=url, headers=headers)
    return hotel_rooms.json()


print("testing")

complete_list = []

session = Session(engine)
hotels = session.query(Hotel).all()
id = 2203

while id <= 12630:
    hotel = session.query(Hotel).get(id)
    hotel_json = get_hotel_room(hotel.hotel_id)

    if "data" in hotel_json:
        hotel_json = hotel_json["data"]
        name = hotel_json["hotel"]["name"]

        image = ""
        if "media" in hotel_json["hotel"]:
            media = hotel_json["hotel"]["media"]
            for m in media:
                image = m["uri"]

        hotel.name = name
        hotel.image = image
        print(hotel.name + " " + hotel.image)
        session.commit()
    id += 1

session.commit()
session.close()

print("done")

# for city in cities:
#     city_count += 1
#     latitude = city.latitude
#     longitude = city.longitude

#     hotels_list = get_hotels(latitude, longitude)
#     count = 0

#     hotels_list = hotels_list["data"]
#     # print("hotels_list: " + str(len(hotels_list)))

#     for hotel in hotels_list:
#         if count > 50:
#             break
#         hotel_info = hotel["hotel"]
#         # print(hotel_info)

#         try:
#             city_id = city.id
#             hotel_id = hotel_info["hotelId"]
#             name = hotel_info["name"]
#             #get description of hotel
#             description = hotel_info["description"]["text"]
#             # description = ""
#             # if 'description' in hotel_info:
#             #     description = hotel_info["description"]["text"]

#             latitude = hotel_info["latitude"]
#             longitude = hotel_info["longitude"]
#             distance = hotel_info["hotelDistance"]["distance"]
#             distance_unit = hotel_info["hotelDistance"]["distanceUnit"]
#             amenities = json.dumps(hotel_info["amenities"])
#             # amenities_string = json.dumps(amenities)
#             # amenities_list = json.loads(amenities_string)
#             #get image url
#             image = ""
#             media = hotel_info["media"]
#             for m in media:
#                 image = m["uri"]
#             # if 'media' in hotel_info:
#             #     media = hotel_info['media']
#             #     for m in media:
#             #         image = m["uri"]

#             rating = hotel_info["rating"]

#             #get contact info
#             contact = hotel_info["contact"]
#             phone = contact["phone"]
#             email = contact["email"]
#             # phone = ""
#             # email = ""
#             # if 'phone' in contact:
#             #     phone  = contact["phone"]
#             # if 'email' in contact:
#             #     email = contact["email"]

#             hotel_to_insert = Hotel(
#                 id = hotel_id,
#                 name = name,
#                 description = description,
#                 latitude = latitude,
#                 longitude = longitude,
#                 distance = distance,
#                 distance_unit = distance_unit,
#                 amenities = amenities,
#                 image = image,
#                 rating = rating,
#                 # phone = phone,
#                 # email = email,
#                 city_id = city_id
#             )
#             session.add(hotel_to_insert)
#             session.commit()
#         except KeyError:
#             continue;
#         else:
#             count += 1
#             hotel_count += 1

# print("\n\n\n\n\n %% city count :: {} \t hotel count :: {}\n\n\n\n\n".format(city_count,hotel_count))
session.commit()
session.close()

print("done")
