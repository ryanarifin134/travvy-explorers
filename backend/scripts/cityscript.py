import requests
from bs4 import BeautifulSoup
from app import engine, City
from sqlalchemy.orm import Session
import time

headers = {
    "Host": "www.innovation-cities.com",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate, br",
    "Connection": "keep-alive",
    "Upgrade-Insecure-Requests": "1",
    "Cache-Control": "max-age=0",
}

# get top 500 cities from website and use beautifulsoup to scrape contents
r = requests.get(
    "https://www.innovation-cities.com/index-2019-global-city-rankings/",
    headers=headers,
)
soup = BeautifulSoup(r.content)

print("got 500 cities")

# select table rows
rows = soup.find_all("tr")

# pop first row as it is the table header
rows.pop(0)


def get_particular_city_from_prefix_response(data, country, region):

    # if we have multiple cities with the same name prefix, we filter based on exact city name and region
    filtered_cities = [
        city
        for city in data
        if country in city["country"]
        and (
            len(region) == 0
            or city["region"].lower() == region.lower()
            or city["regionCode"] == region
        )
    ]

    # filtered_cities SHOULD have only one element, so return the first element
    if len(filtered_cities) == 0:
        return None
    return filtered_cities[0]


# create a session for the database to insert rows
session = Session(engine)

print("started session...")

city_count = 0

# iterate through all rows and insert rows in one sweep
for row in rows:

    city_count += 1

    # get city and region from column class name
    city = row.select(".column-3")[0].text
    country = row.select(".column-4")[0].text
    region = row.select(".column-5")[0].text

    print("attempting to get info for city: " + city)

    # unfortunately, the api is only capabale of filtering by name prefix to find a particular city.
    city_req = requests.get(
        "http://geodb-free-service.wirefreethought.com/v1/geo/cities?limit=10&offset=0&namePrefix="
        + city
    )
    print("got info! now sleeping for a sec...")

    # we are limited to one request per second, so just wait for a sec
    time.sleep(1)

    # no worries, we can filter manually in case we get multiple cities with the same prefix
    particular_city = get_particular_city_from_prefix_response(
        city_req.json()["data"], country, region
    )

    # if we got no results for the city, move on (may happen due to inconsistencies between sources)
    if particular_city == None:
        continue

    # get the id associated with the city for the geodb api
    id = particular_city["id"]

    # use the id to get extra information for this city
    city_details = requests.get(
        "http://geodb-free-service.wirefreethought.com/v1/geo/cities/" + str(id)
    ).json()["data"]
    print("got city details! sleeping again...")

    # again, one request per second...
    time.sleep(1)

    city_name = particular_city["city"]
    country = particular_city["country"]
    country_code = particular_city["countryCode"]
    region = particular_city["region"]
    region_code = particular_city["regionCode"]
    population = city_details["population"]
    elevation = city_details["elevationMeters"]
    latitude = city_details["latitude"]
    longitude = city_details["longitude"]
    timezone = city_details["timezone"]

    # create a sqlalchemy ORM city object with these attributes
    city_to_insert = City(
        city=city_name,
        country=country,
        country_code=country_code,
        region=region,
        region_code=region_code,
        population=population,
        elevation=elevation,
        latitude=latitude,
        longitude=longitude,
        timezone=timezone,
    )

    # add the city ORM object to the session
    session.add(city_to_insert)
    session.commit()
    # the session will decide to flush changes to the database
    # once it reaches a certain capacity.
    print("planning on inserting row for city: " + city_name)
    print(str(city_count) + "/500 done")

# commit the final changes to the db
session.commit()
session.close()
