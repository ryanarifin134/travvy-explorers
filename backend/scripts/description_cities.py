from app import engine, City
import requests
from sqlalchemy.orm import Session

session = Session(engine)

cities = session.query(City).all()

count = 0

for city in cities:
    count += 1
    find_name = requests.get(
        "https://en.wikipedia.org/w/api.php?action=opensearch&search="
        + city.city
        + " "
        + city.country
        + "&limit=1&namespace=0&format=json"
    ).json()
    if len(find_name[1]) == 0:
        find_name = requests.get(
            "https://en.wikipedia.org/w/api.php?action=opensearch&search="
            + city.city
            + "&limit=1&namespace=0&format=json"
        ).json()
    find_name = find_name[1][0]
    while True:
        find_page = requests.get(
            "https://en.wikipedia.org/w/api.php?action=parse&page="
            + find_name
            + "&prop=wikitext&formatversion=2&format=json"
        ).json()
        text = find_page["parse"]["wikitext"]
        if "#REDIRECT" in text:
            find_name = text[text.index("[[") + 2 : text.index("]]")]
        else:
            break
    sentences = requests.get(
        "https://en.wikipedia.org/w/api.php?action=query&prop=extracts&exsentences=2&exlimit=1&titles="
        + find_name
        + "&explaintext=1&formatversion=2&format=json"
    ).json()
    sentences = sentences["query"]["pages"][0]["extract"]
    city.description = sentences
    session.commit()
    print(sentences)
    print(str(count) + "/396")

session.commit()
session.close()
