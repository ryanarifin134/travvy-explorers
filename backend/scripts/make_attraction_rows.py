import requests
from app import db, City, Attraction
from sqlalchemy.orm import Session
import time


complete_list = []
outdoor_categories = [
    "baths_and_saunas",
    "natural",
    "red_telephone_boxes",
    "sundials",
    "view_points",
    "diving",
    "climbing" "kitesurfing",
    "pools",
    "surfing",
    "winter_sports",
    "picnic_site",
    "outdoor",
]
session = Session(db.engine)
cities = session.query(City).all()

# make sure text is in english
def isEnglish(s):
    try:
        s.encode(encoding="utf-8").decode("ascii")
    except UnicodeDecodeError:
        return False
    else:
        return True


city_count = 0
attraction_count = 0
# iterate through all cities in the database and generate their respective attractions
for cur_city in cities:
    # get list of attractions for a city with the API
    city_count += 1
    lat = cur_city.latitude
    lon = cur_city.longitude
    kinds = "amusements,religion,natural,historic,cultural,architecture,sport"
    url = "http://api.opentripmap.com/0.1/en/places/radius?radius=80467&lon={}&lat={}&kinds={}&apikey=5ae2e3f221c38a28845f05b6e8c8f9c08cb3c2149d9231573b708f40&format=geojson".format(
        lon, lat, kinds
    )
    raw_attraction_list = requests.get(url)
    attraction_list = raw_attraction_list.json()
    count = 0
    if "features" in attraction_list:
        for cur_attraction in attraction_list["features"]:
            name = cur_attraction["properties"]["name"]
            if count > 40:
                break
            if name != "" and isEnglish(name) and name not in complete_list:

                complete_list.append(name)
                # now need to get additional information about this attraction
                # make an additional API call
                xid = cur_attraction["properties"]["xid"]
                city_distance = cur_attraction["properties"]["dist"]
                # time.sleep(1)
                url_2 = "http://api.opentripmap.com/0.1/en/places/xid/{}?apikey=5ae2e3f221c38a28845f05b6e8c8f9c08cb3c2149d9231573b708f40".format(
                    xid
                )
                raw_attraction_info = requests.get(url_2)
                attraction_info = raw_attraction_info.json()
                # fill out fields
                try:
                    id = xid
                    city_id = cur_city.id
                    city_distance = cur_attraction["properties"]["dist"]
                    lon = attraction_info["point"]["lon"]
                    lat = attraction_info["point"]["lat"]
                    rating = attraction_info["rate"]
                    category = attraction_info["kinds"]
                    categories = category.split(",")
                    # check to see if it's an indoor/outdoor venue
                    venue_type = "indoor venue"
                    for current in categories:
                        if current in outdoor_categories:
                            venue_type = "outdoor venue"
                            break
                    description = attraction_info["wikipedia_extracts"]["text"]
                    city_name = attraction_info["address"]["city"]
                    address = (
                        attraction_info["address"]["house_number"]
                        + " "
                        + attraction_info["address"]["road"]
                    )
                    image_src = attraction_info["preview"]["source"]
                    # print("this is the image :: {}\n\n".format(image_src))
                    # break
                    wiki_link = attraction_info["wikipedia"]

                    # now create an attraction object
                    attraction_to_insert = Attraction(
                        id=id,
                        name=name,
                        city_id=city_id,
                        city_name=city_name,
                        city_distance=city_distance,
                        venue_type=venue_type,
                        rating=rating,
                        lon=lon,
                        lat=lat,
                        category=category,
                        description=description,
                        address=address,
                        image_src=image_src,
                        wiki_link=wiki_link,
                    )

                except KeyError:
                    # attraction didn't have enough data, so disregard it
                    continue
                else:
                    # insert attraction object into db
                    session.add(attraction_to_insert)
                    session.commit()

                    count += 1
                    attraction_count += 1
print(
    "\n\n\n\n\n %% city count :: {} \t attraction count :: {} %%\n\n\n\n\n".format(
        city_count, attraction_count
    )
)
session.commit()
session.close()
