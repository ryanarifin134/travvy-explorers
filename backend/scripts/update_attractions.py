from sqlalchemy import select
from sqlalchemy.orm import Session
from app import engine, Attraction
import requests

session = Session(engine)
attractions = (
    session.execute(select(Attraction)).scalars().all()
)  # gets all hotel objects from db
count = 0
for attraction in attractions:
    xid = attraction.id
    url = "http://api.opentripmap.com/0.1/en/places/xid/{}?apikey=5ae2e3f221c38a28845f05b6e8c8f9c08cb3c2149d9231573b708f40".format(
        xid
    )
    raw_attraction_info = requests.get(url)
    attraction_info = raw_attraction_info.json()
    # print(attraction_info)
    # break;
    if "preview" in attraction_info:
        print("got in here\n\n")
        attraction.image_src = attraction_info["preview"]["source"]

    else:
        print("got in bad one\n\n")
        attraction.image = "NULL"
    # hotel.something = 'value to assign to new column'
    # at this point, the session detects that the hotel is "dirty" automatically
    session.commit()  # bam, changes updated and the db is updated with the hotel's new column value
    count += 1
    if count % 50 == 0:
        print(count)
session.commit()
session.close()
