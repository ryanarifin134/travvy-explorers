import json
import urllib
from flask import request, jsonify, Response
from app import app
from flask_marshmallow import Marshmallow
from marshmallow import fields
from app.models.city import City
from app.models.attraction import Attraction
from app.models.hotel import Hotel
from functools import reduce
import sys
import json
from urllib.parse import unquote
import operator
import pdb
from algoliasearch.search_client import SearchClient

# initialize algolia
application_id = "6LIXUH6EAE"
search_api_key = "3b40bb6bb34657c93349f892b038497e"
client = SearchClient.create(application_id, search_api_key)
hotels_index = client.init_index("travvyexplorers_hotels")
cities_index = client.init_index("travvyexplorers_cities")
attractions_index = client.init_index("travvyexplorers_attractions")

ma = Marshmallow(app)

CITIES_PER_PAGE = 10
ATTRACTIONS_PER_PAGE = 9
HOTELS_PER_PAGE = 10


def format_facilities(amenities_list):
    res = []
    for amenity in amenities_list:
        final_amenity_string = ""
        for char in amenity:
            if char.isalpha():
                final_amenity_string += char
        res.append(final_amenity_string)
    return res


def error_on_invalid_id(id):
    response = Response(
        json.dumps({"error": str(id) + " not a valid id"}), mimetype="application/json"
    )
    response.status_code = 404
    return response


def reformat_hotels(search_results_json, offset, instances_per_page, sort_by, asc):
    for instance in search_results_json["hits"]:
        del instance["objectID"]

    num_hits = len(search_results_json["hits"])
    result = search_results_json["hits"]

    reverse = asc == "false"

    if sort_by == "name":
        result.sort(key=lambda x: x["name"], reverse=reverse)
    elif sort_by == "distance":
        result.sort(key=lambda x: x["distance"], reverse=reverse)
    elif sort_by == "price":
        result.sort(key=lambda x: x["price"], reverse=reverse)
    elif sort_by == "rating":
        result.sort(key=lambda x: x["rating"], reverse=reverse)

    return result[offset : offset + instances_per_page], num_hits


def reformat_cities(search_results_json, offset, instances_per_page, sort_by, order):
    for instance in search_results_json["hits"]:
        del instance["objectID"]

    num_hits = len(search_results_json["hits"])
    result = search_results_json["hits"]

    reverse = order == "descending"

    if sort_by == "city":
        result.sort(key=lambda x: x["name"], reverse=reverse)
    elif sort_by == "country":
        result.sort(key=lambda x: x["country"], reverse=reverse)
    elif sort_by == "region":
        result.sort(key=lambda x: x["region"], reverse=reverse)
    elif sort_by == "population":
        result.sort(key=lambda x: x["population"], reverse=reverse)
    elif sort_by == "elevation":
        result.sort(key=lambda x: x["elevation"], reverse=reverse)
    elif sort_by == "timezone":
        result.sort(key=lambda x: x["timezone"], reverse=reverse)
    elif sort_by == "covid_cases":
        result.sort(key=lambda x: x["covidCases"], reverse=reverse)

    return result[offset : offset + instances_per_page], num_hits


def reformat_attractions(search_results_json, offset, instances_per_page, sort_by):
    for instance in search_results_json["hits"]:
        del instance["objectID"]

    num_hits = len(search_results_json["hits"])
    result = search_results_json["hits"]

    if sort_by == "AttractionNameAsc":
        result.sort(key=lambda x: x["name"])
    elif sort_by == "AttractionNameDesc":
        result.sort(key=lambda x: x["name"], reverse=True)
    elif sort_by == "AttractionCityAsc":
        result.sort(key=lambda x: x["city"]["name"])
    elif sort_by == "AttractionCityDesc":
        result.sort(key=lambda x: x["city"]["name"], reverse=True)
    elif sort_by == "CityDistance":
        result.sort(key=lambda x: x["city_distance"])
    elif sort_by == "AttractionRating":
        result.sort(key=lambda x: x["rating"], reverse=True)

    return result[offset : offset + instances_per_page], num_hits


#########################################
############### SCHEMAS #################
#########################################
class CitySchema(ma.Schema):
    class Meta:
        strict = True
        ordered = True

    id = fields.Integer(attribute="id")
    name = fields.String(attribute="city")
    country = fields.String(attribute="country")
    region = fields.String(attribute="region")
    latitude = fields.Float(attribute="latitude")
    longitude = fields.Float(attribute="longitude")
    elevation = fields.Integer(attribute="elevation")
    population = fields.Integer(attribute="population")
    timezone = fields.String(attribute="timezone")
    covidCases = fields.Integer(attribute="covid_cases")
    imageUrl = fields.String(attribute="img_url")
    description = fields.String(attribute="description")
    num_hotels = ma.Function(lambda obj: len(obj.hotels))
    num_attractions = ma.Function(lambda obj: len(obj.attractions))
    attractions = fields.Nested("AttractionSchema", many=True, exclude=("city",))
    hotels = fields.Nested(
        "HotelSchema", many=True, only=("image", "description", "id", "name")
    )

    def get_avg_hotel_rating(obj):
        if len(obj.hotels):
            return sum([hotel.rating for hotel in obj.hotels]) / len(obj.hotels)

    avg_hotel_rating = ma.Function(get_avg_hotel_rating)


class HotelSchema(ma.Schema):
    class Meta:
        strict = True
        ordered = True

    id = fields.Integer(attribute="id")
    name = fields.String(attribute="name")
    city = ma.Function(lambda obj: obj.city and obj.city.city or "unknown")
    description = fields.String(attribute="description")
    latitude = fields.Float(attribute="latitude")
    longitude = fields.Float(attribute="longitude")
    distance = fields.Float(attribute="distance")
    distance_unit = fields.String(attribute="distance_unit")

    def parse_amenities(obj):
        amenities = obj.amenities
        # get rid of brackets
        amenities = amenities[1:-1]
        amenities = amenities.split(", ")
        # get rid of quotes per amenity
        amenities = [a[1:-1] for a in amenities]
        return amenities

    amenities = ma.Function(parse_amenities)
    image = fields.String(attribute="image")
    rating = fields.Float(attribute="rating")
    phone = fields.String(attribute="phone")
    email = fields.String(attribute="email")
    address = fields.String(attribute="address")
    # TODO: reupload as float to db
    price = ma.Function(lambda obj: 0 if obj.price == "" else float(obj.price))
    hotelId = fields.String(attribute="hotel_id")
    cityId = fields.Float(attribute="city_id")
    city = fields.String(attribute="city")
    attractions = fields.Nested(
        "AttractionSchema", many=True, exclude=("city", "hotel")
    )


class AttractionSchema(ma.Schema):
    class Meta:
        strict = True
        ordered = True
        use_fk = True

    id = fields.Integer(attribute="id")
    name = fields.String(attribute="name")
    city_name = fields.String(attribute="city_name")
    city = fields.Nested(CitySchema)
    city_distance = fields.Integer(attribute="city_distance")
    venue_type = fields.String(attribute="venue_type")
    rating = fields.Integer(attribute="rating")
    lat = fields.Float(attribute="lat")
    lon = fields.Float(attribute="lon")
    category = ma.Function(lambda obj: obj.category.split(","))
    description = fields.String(attribute="description")
    address = fields.String(attribute="address")
    imageUrl = fields.String(attribute="image_src")
    wikiUrl = fields.String(attribute="wiki_link")
    hotels_in_city = ma.Function(lambda obj: obj.city and len(obj.city.hotels) or 0)
    hotel = fields.Nested("HotelSchema", exclude=("attractions", "city"))


#########################################
####### INITIALIZE SCHEMA OBJECTS #######
#########################################
city_schema = CitySchema()
cities_schema = CitySchema(many=True, exclude=("hotels", "attractions"))

hotel_schema = HotelSchema()
hotels_schema = HotelSchema(many=True, exclude=("attractions",))

attraction_schema = AttractionSchema()
attractions_schema = AttractionSchema(many=True)

#########################################
############### ROUTES ##################
#########################################
# NOTE: This route is needed for the default EB health check route
@app.route("/")
def home():
    return "ok"


@app.route("/api/cities")
def get_cities():
    sortable = [
        "city",
        "country",
        "region",
        "population",
        "elevation",
        "timezone",
        "covid_cases",
    ]
    filterable = ["country", "region", "timezone"]
    filterable_min_max = ["population", "elevation", "covid_cases"]

    args = request.args
    query = City.query
    filters = []

    for attribute in filterable:
        if attribute in args:
            query = query.filter_by(**{attribute: args.get(attribute)})
            filters.append(attribute + ":" + '"' + args.get(attribute) + '"')

    for attribute in filterable_min_max:
        if "min_" + attribute in args:
            min_num = args.get("min_" + attribute)
            query = query.filter(getattr(City, attribute) >= min_num)
            filters.append("min_" + attribute + " >= " + str(min_num))
        if "max_" + attribute in args:
            max_num = args.get("max_" + attribute)
            query = query.filter(getattr(City, attribute) <= max_num)
            filters.append("max_" + attribute + " <= " + str(max_num))

    if "sort_by" in args and args.get("sort_by") in sortable:
        attribute = args.get("sort_by")
        if "order" in args and args.get("order") == "descending":
            query = query.order_by(getattr(City, attribute).desc())
        else:
            query = query.order_by(getattr(City, attribute).asc())

    page = int(args.get("page") or 1)
    offset = (page - 1) * CITIES_PER_PAGE

    if "query" in args:
        sort_by = args["sort_by"] if "sort_by" in args else ""
        order = args["order"] if "order" in args else ""
        search_query = args.get("query")
        search_results_json = cities_index.search(
            search_query, {"filters": " AND ".join(filters)}
        )
        result, total_count = reformat_cities(
            search_results_json, offset, CITIES_PER_PAGE, sort_by, order
        )
    else:
        page_cities = query.limit(CITIES_PER_PAGE).offset(offset).all()
        result = cities_schema.dump(page_cities)
        total_count = query.count()

    response = {"cities": result, "totalCount": total_count}

    return jsonify(response)


@app.route("/api/cities/<id>")
def city_detail(id):
    city = City.query.get(id)
    if not city:
        return error_on_invalid_id(id)
    return city_schema.dump(city)


def binary_search(elem, list, ascending=True):
    lo = 0
    hi = len(list) - 1
    while lo <= hi:
        mid = (lo + hi) >> 1
        if list[mid] == elem:
            return mid
        if elem < list[mid]:
            if ascending:
                hi = mid - 1
            else:
                lo = mid + 1
        if elem > list[mid]:
            if ascending:
                lo = mid + 1
            else:
                hi = mid - 1
    return -1


def underscore_to_camel_case(underscore):
    parts = underscore.split("_")
    parts = iter(parts)
    result = str.lower(next(parts))
    for part in parts:
        result += str.capitalize(part)
    return result


@app.route("/api/cities/compare/<path:varargs>")
def compare_cities(varargs=None):
    varargs = varargs.split("/")
    # There must be at least 2 cities and no more than 4
    num_cities = len(varargs)
    if num_cities < 2 or num_cities > 4:
        return jsonify({
            "error": "Invalid number of cities. At least 2 and no more than 4 cities allowed."
        }), 400

    rankable_attributes = ["population", "elevation", "covid_cases"]
    ascending_rankable_attributes = {"covid_cases"}
    cities = []
    for city_id in varargs:
        id = 0
        try:
            # Attempt parse
            id = int(city_id)
        except ValueError:
            return error_on_invalid_id(city_id)
        city = City.query.get(id)
        if not city:
            return error_on_invalid_id(id)
        cities += [city]

    attribute_values = {
        attribute: [getattr(city, attribute) for city in cities]
        for attribute in rankable_attributes
    }

    for attribute in attribute_values:
        reverse = attribute not in ascending_rankable_attributes
        attribute_values[attribute].sort(reverse=reverse)
    result = cities_schema.dump(cities)
    for i in range(len(cities)):
        # cities array is parallel to result array from schema dump so use shared index
        city = cities[i]
        result[i]["rankings"] = {}
        for attribute, values in attribute_values.items():
            value = getattr(city, attribute)
            camel_cased_attribute = underscore_to_camel_case(attribute)
            ascending = attribute in ascending_rankable_attributes
            result[i]["rankings"][camel_cased_attribute] = (
                binary_search(value, values, ascending=ascending) + 1
            )
    return jsonify({"cities": result})


@app.route("/api/attractions")
def get_attractions():
    args = request.args
    query = Attraction.query
    filters = []

    # filterable attributes
    if "AttractionRating" in args:
        query = query.filter_by(rating=args["AttractionRating"])
        filters.append("rating:" + args["AttractionRating"])
    if "AttractionCity" in args:
        query = query.filter(
            Attraction.city.has(City.city.like(args["AttractionCity"]))
        )
        filters.append('city.name:"' + args["AttractionCity"] + '"')
    if "indoor" in args:
        if args["indoor"] == "true":  # if you want indoor
            query = query.filter_by(venue_type="indoor venue")
            filters.append('venue_type:"indoor venue"')
        elif args["indoor"] == "false":  # if you want outdoor
            query = query.filter_by(venue_type="outdoor venue")
            filters.append('venue_type:"outdoor venue"')
    if "AttractionCategory" in args:
        query = query.filter(Attraction.category.contains(args["AttractionCategory"]))
        filters.append('category:"' + args["AttractionCategory"] + '"')
    if "AttractionDistance" in args:
        dist = int(args["AttractionDistance"])
        query = query.filter(Attraction.city_distance <= dist)
        filters.append("city_distance <= " + str(dist))

    # sortable attributes
    if "sortby" in args:
        if args["sortby"] == "AttractionNameAsc":
            query = query.order_by(Attraction.name.asc())
        elif args["sortby"] == "AttractionNameDesc":
            query = query.order_by(Attraction.name.desc())
        elif args["sortby"] == "AttractionCityAsc":
            query = query.order_by(Attraction.city_name.asc())
        elif args["sortby"] == "CityDistance":
            query = query.order_by(Attraction.city_distance.asc())
        elif args["sortby"] == "Rating":
            query = query.order_by(Attraction.rating.desc())

    page = int(args.get("page") or 1)
    offset = (page - 1) * ATTRACTIONS_PER_PAGE

    if "query" in args:
        sort_by = args["sortby"] if "sortby" in args else ""
        search_query = args.get("query")
        search_results_json = attractions_index.search(
            search_query, {"filters": " AND ".join(filters)}
        )
        result, total_count = reformat_attractions(
            search_results_json, offset, ATTRACTIONS_PER_PAGE, sort_by
        )
    else:
        page_attractions = query.limit(ATTRACTIONS_PER_PAGE).offset(offset).all()
        result = attractions_schema.dump(page_attractions)
        total_count = query.count()

    response = {"attractions": result, "totalCount": total_count}
    return jsonify(response)


@app.route("/api/attractions/<id>", methods=["GET"])
def attraction_detail(id):
    attraction = Attraction.query.get(id)
    if not attraction:
        return error_on_invalid_id(id)
    return attraction_schema.dump(attraction)


@app.route("/api/attractions/compare/<path:varargs>")
def compare_attractions(varargs=None):
    varargs = varargs.split("/")
    # There must be at least 2 attractions and no more than 4
    num_attractions = len(varargs)
    if num_attractions < 2 or num_attractions > 4:
        return jsonify({
            "error": "Invalid number of attractions. At least 2 and no more than 4 attractions allowed."
        }), 400

    rankable_attributes = ["city_distance", "rating"]
    ascending_rankable_attributes = {"city_distance"}
    attractions = []
    for attraction_id in varargs:
        id = 0
        try:
            # Attempt parse
            id = int(attraction_id)
        except ValueError:
            return error_on_invalid_id(attraction_id)
        attraction = Attraction.query.get(id)
        if not attraction:
            return error_on_invalid_id(id)
        attractions += [attraction]

    attribute_values = {
        attribute: [getattr(attraction, attribute) for attraction in attractions]
        for attribute in rankable_attributes
    }

    for attribute in attribute_values:
        reverse = attribute not in ascending_rankable_attributes
        attribute_values[attribute].sort(reverse=reverse)
    result = attractions_schema.dump(attractions)
    for i in range(len(attractions)):
        # attractions array is parallel to result array from schema dump so use shared index
        attraction = attractions[i]
        result[i]["rankings"] = {}
        for attribute, values in attribute_values.items():
            value = getattr(attraction, attribute)
            ascending = attribute in ascending_rankable_attributes
            result[i]["rankings"][attribute] = (
                binary_search(value, values, ascending=ascending) + 1
            )
    return jsonify({"attractions": result})


# get all hotels
@app.route("/api/hotels")
def get_hotels():
    args = request.args
    query = Hotel.query
    filters = []

    # these are the filterable parameters
    if "rating" in args:
        query = query.filter_by(rating=args["rating"])
        filters.append("rating:" + args["rating"])
    if "city" in args:
        query = query.filter_by(city=urllib.parse.unquote(args["city"]))
        filters.append('city:"' + args["city"] + '"')
    if "name" in args:
        query = query.filter_by(name="%" + urllib.parse.unquote(args["name"]) + "%")
        filters.append('name:"' + args["name"] + '"')
    if "price_min" in args:
        query = query.filter(Hotel.price >= int(args["price_min"]))
        filters.append("price >= " + args["price_min"])
    if "price_max" in args:
        query = query.filter(Hotel.price <= int(args["price_max"]))
        filters.append("price <= " + args["price_max"])
    if "distance" in args:
        query = query.filter(Hotel.distance <= int(args["distance"]))
        filters.append("distance <= " + args["distance"])
    if "amenities" in args:
        query = query.filter(Hotel.amenities.contains(args["amenities"]))
        filters.append('amenities:"' + args["amenities"] + '"')

    if "sortby" in args:
        ascending = True
        if "ascending" in args:
            if args["ascending"] == "false":
                ascending = False
        if args["sortby"] == "name":
            query = (
                query.order_by(Hotel.name.asc())
                if ascending
                else query.order_by(Hotel.name.desc())
            )
        elif args["sortby"] == "distance":
            query = (
                query.order_by(Hotel.distance.asc())
                if ascending
                else query.order_by(Hotel.distance.desc())
            )
        elif args["sortby"] == "price":
            query = (
                query.order_by(Hotel.price.asc())
                if ascending
                else query.order_by(Hotel.price.desc())
            )
        elif args["sortby"] == "rating":
            query = (
                query.order_by(Hotel.rating.asc())
                if ascending
                else query.order_by(Hotel.rating.desc())
            )

    page = int(args.get("page") or 1)
    offset = (page - 1) * HOTELS_PER_PAGE

    if "query" in args:
        sort_by = args["sortby"] if "sortby" in args else ""
        asc = args["ascending"] if "ascending" in args else ""
        search_query = args.get("query")
        search_results_json = hotels_index.search(
            search_query, {"filters": " AND ".join(filters)}
        )
        result, total_count = reformat_hotels(
            search_results_json, offset, HOTELS_PER_PAGE, sort_by, asc
        )
    else:
        page_hotels = query.limit(HOTELS_PER_PAGE).offset(offset).all()
        result = hotels_schema.dump(page_hotels)
        total_count = query.count()

    response = {"hotels": result, "totalCount": total_count}

    return jsonify(response)


# get all hotels for a certain city
@app.route("/api/hotels/city/<cityId>")
def get_hotels_by_city(cityId):
    hotels = Hotel.query.filter_by(city_id=cityId).all()
    if not hotels:
        return error_on_invalid_id(cityId)
    result = hotels_schema.dump(hotels)
    return jsonify({"hotels": result})


# get specific hotel by hotelId
@app.route("/api/hotels/<id>")
def hotel_detail(id):
    hotel = Hotel.query.get(id)
    if not hotel:
        return error_on_invalid_id(id)
    result = hotel_schema.dump(hotel)
    return jsonify({"hotel": result})


@app.route("/api/hotels/names")
def hotel_names():
    hotels = Hotel.query.all()
    hotel_names = []
    for hotel in hotels:
        hotel_names.append({"label": hotel.name, "value": hotel.name})
    return jsonify({"hotels": hotel_names})


@app.route("/api/hotels/compare/<path:varargs>")
def compare_hotels(varargs=None):
    varargs = varargs.split("/")
    # There must be at least 2 hotels and no more than 4
    num_hotels = len(varargs)
    if num_hotels < 2 or num_hotels > 4:
        return jsonify({
            "error": "Invalid number of hotels. At least 2 and no more than 4 hotels allowed."
        }), 400

    rankable_attributes = ["distance", "rating", "price"]
    ascending_rankable_attributes = {"distance", "price"}
    hotels = []
    for hotel_id in varargs:
        id = 0
        try:
            # Attempt parse
            id = int(hotel_id)
        except ValueError:
            return error_on_invalid_id(hotel_id)
        hotel = Hotel.query.get(id)
        if not hotel:
            return error_on_invalid_id(id)
        hotels += [hotel]

    attribute_values = {
        attribute: [getattr(hotel, attribute) for hotel in hotels]
        for attribute in rankable_attributes
    }

    for attribute in attribute_values:
        reverse = attribute not in ascending_rankable_attributes
        attribute_values[attribute].sort(reverse=reverse)
    result = hotels_schema.dump(hotels)
    for i in range(len(hotels)):
        # hotels array is parallel to result array from schema dump so use shared index
        hotel = hotels[i]
        result[i]["rankings"] = {}
        for attribute, values in attribute_values.items():
            value = getattr(hotel, attribute)
            ascending = attribute in ascending_rankable_attributes
            result[i]["rankings"][attribute] = (
                binary_search(value, values, ascending=ascending) + 1
            )
    return jsonify({"hotels": result})
