import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

if "TESTING" in os.environ:
    app.config["TESTING"] = True
    app.testing = True

    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"
else:
    DATABASE_URI = f'mysql+pymysql://{os.getenv("AWS_ACCESS_KEY_ID")}:{os.getenv("AWS_SECRET_ACCESS_KEY")}@{os.getenv("AWS_DB_HOST")}:{os.getenv("AWS_DB_PORT")}/{os.getenv("AWS_DB")}'
    app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URI


db = SQLAlchemy(app)
CORS(app)

from app.models.city import City
from app.models.attraction import Attraction
from app.models.hotel import Hotel

# Create the tables for each model
db.create_all()

if "CITIES" in os.environ:
    cities_to_create = int(os.getenv("CITIES"))
    db.session.add_all(City.create(cities_to_create))
    db.session.commit()

if "ATTRACTIONS" in os.environ:
    attractions_to_create = int(os.getenv("ATTRACTIONS"))
    db.session.add_all(Attraction.create(attractions_to_create))
    db.session.commit()

if "HOTELS" in os.environ:
    hotels_to_create = int(os.getenv("HOTELS"))
    db.session.add_all(Hotel.create(hotels_to_create))
    db.session.commit()


from app import routes
