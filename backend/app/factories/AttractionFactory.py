import random

from faker import Faker
from faker.providers import geo

faker = Faker()


def definition():
    categories = random.sample(
        [
            "historical",
            "cultural",
            "theatres_and_entertainments",
            "interesting_places",
            "other_theatres",
            "architecture",
        ],
        random.randint(1, 6),
    )
    categories = iter(categories)
    stringified_categories = next(categories)
    for category in categories:
        stringified_categories += "," + category
    return {
        "name": faker.text(max_nb_chars=30),
        "city_distance": faker.random_int(min=1, max=20),
        "venue_type": random.choice(["indoor venue", "outdoor venue"]),
        "rating": faker.random_int(min=1, max=5),
        "lat": float(faker.latitude()),
        "lon": float(faker.longitude()),
        "category": stringified_categories,
        "description": faker.text(max_nb_chars=600).replace("\n", " "),
        "address": faker.street_address(),
        "image_src": "https://placeimg.com/1024/768/any",
        "wiki_link": "https://en.wikipedia.org/wiki/Special:Random",
    }
