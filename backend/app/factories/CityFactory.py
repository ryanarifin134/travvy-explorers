from faker import Faker
from faker.providers import geo

faker = Faker()


def definition():
    return {
        "city": faker.city(),
        "country": faker.country(),
        "country_code": faker.country_code(),
        "region": faker.state(),
        "region_code": faker.state_abbr(),
        "population": faker.random_int(min=1000, max=10000000),
        "elevation": faker.random_int(min=0, max=1000),
        "latitude": float(faker.latitude()),
        "longitude": float(faker.longitude()),
        "timezone": faker.timezone(),
        "covid_cases": faker.random_int(min=1, max=100000000),
        "img_url": "https://placeimg.com/1024/768/any",
        "description": faker.text(max_nb_chars=350).replace("\n", " "),
    }
