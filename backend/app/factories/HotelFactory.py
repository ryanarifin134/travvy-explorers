import random

from faker import Faker
from faker.providers import geo

faker = Faker()


def definition():
    amenities = [
        "WHEELCHAIR_ACCESSIBLE_ELEVATORS",
        "DISABLED_ACCESSIBLE_TOILETS",
        "ACCESSIBLE_PARKING",
        "SERVICE_DOGS_ALLOWED",
        "SPECIAL_NEEDS_MENU",
        "DISABLED_TRAINED_STAFF",
        "TV_SUBTITLES/CAPTION",
        "WIDE_CORRIDORS",
        "WIDE_ENTRANCE",
        "WIDE_RESTAURANT_ENTRANCE",
        "EMERGENCY_LIGHTING",
        "FIRE_DETECTORS",
        "FEMA_FIRE_SAFETY_COMPLIANT",
        "SPRINKLERS",
        "FIRST_AID_STAF",
        "VIDEO_SURVEILANCE",
        "KIDS_WELCOME",
        "COFFEE_SHOP",
        "GIFT_SHOP",
        "SAFE_DEPOSIT_BOX",
        "ELEVATOR",
        "RESTAURANT",
        "LOUNGE",
        "MASSAGE",
        "TOUR_DESK",
        "MEETING_FACILITIES",
        "AUDIO-VISUAL_EQUIPMENT",
        "ROOM_SERVICE",
        "INTERNET_SERVICES",
        "FREE_INTERNET",
        "INTERNET_HOTSPOTS",
        "WIFI",
        "WIRELESS_CONNECTIVITY",
    ]
    amenities = random.sample(amenities, random.randint(1, len(amenities)))
    amenities = iter(amenities)
    strinified_amenities = next(amenities)
    for amenity in amenities:
        strinified_amenities += ", " + amenity
    strinified_amenities = "[" + strinified_amenities + "]"

    latitude, longitude, _, _, _ = faker.location_on_land()

    return {
        "name": faker.text(max_nb_chars=30),
        "description": faker.text(max_nb_chars=500).replace("\n", " "),
        "latitude": float(latitude),
        "longitude": float(longitude),
        "distance": random.uniform(1.0, 30.0),
        "distance_unit": "KM",
        "amenities": strinified_amenities,
        "image": "https://placeimg.com/1024/768/any",
        "rating": faker.random_int(min=2, max=5),
        "phone": faker.phone_number(),
        "email": faker.email(),
        "address": faker.street_address(),
        "price": float(
            "%d.%d"
            % (faker.random_int(min=90, max=300), faker.random_int(min=0, max=99))
        ),
    }
