from app import db
from app.factories import CityFactory


class City(db.Model):
    __tablename__ = "cities"

    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(30))
    country = db.Column(db.String(30))
    country_code = db.Column(db.String(4))
    region = db.Column(db.String(30))
    region_code = db.Column(db.String(4))
    population = db.Column(db.Integer)
    elevation = db.Column(db.Integer)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    timezone = db.Column(db.String(20))
    covid_cases = db.Column(db.Integer)
    img_url = db.Column(db.String(300))
    description = db.Column(db.String(350))

    attractions = db.relationship("Attraction", back_populates="city")
    hotels = db.relationship("Hotel", back_populates="city_obj")

    def __repr__(self):
        return (
            f"City(id={self.id!r}, city={self.city!r}, population={self.population!r})"
        )

    @classmethod
    def create(cls, n=1):
        cities = [City(**CityFactory.definition()) for _ in range(n)]

        if len(cities) == 1:
            return cities[0]

        return cities
