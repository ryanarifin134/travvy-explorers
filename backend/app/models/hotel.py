from app import db
from app.factories import HotelFactory


class Hotel(db.Model):
    __tablename__ = "hotels"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    description = db.Column(db.String(500))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    distance = db.Column(db.Float)
    distance_unit = db.Column(db.String(30))
    amenities = db.Column(db.String(1000))
    image = db.Column(db.String(30))
    rating = db.Column(db.Float)
    phone = db.Column(db.String(30))
    email = db.Column(db.String(50))
    address = db.Column(db.String(100))
    price = db.Column(db.String(30))
    hotel_id = db.Column(db.String(30))
    city_id = db.Column(db.Integer, db.ForeignKey("cities.id"))
    city = db.Column(db.String(100))

    # TODO: check this
    city_obj = db.relationship("City", back_populates="hotels")
    attractions = db.relationship("Attraction", back_populates="hotel")

    def __repr__(self):
        return f"City(id={self.id!r}, city={self.city!r})"

    @classmethod
    def create(cls, n=1):
        hotels = [Hotel(**HotelFactory.definition()) for _ in range(n)]

        if len(hotels) == 1:
            return hotels[0]

        return hotels
