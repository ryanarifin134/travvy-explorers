from app import db
from app.factories import AttractionFactory


class Attraction(db.Model):
    __tablename__ = "attractions"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    city_id = db.Column(db.Integer, db.ForeignKey("cities.id"))
    city_name = db.Column(db.String(100))
    city_distance = db.Column(db.Integer)
    venue_type = db.Column(db.String(100))
    rating = db.Column(db.Integer)
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)
    category = db.Column(db.String(100))
    description = db.Column(db.String(600))
    address = db.Column(db.String(100))
    image_src = db.Column(db.String(500))
    wiki_link = db.Column(db.String(500))
    hotel_id = db.Column(db.Integer, db.ForeignKey("hotels.id"))

    city = db.relationship("City", back_populates="attractions")
    hotel = db.relationship("Hotel", back_populates="attractions")

    def __repr__(self):
        return f"Attraction(id={self.id!r}, city={self.name!r})"

    @classmethod
    def create(cls, n=1):
        attractions = [Attraction(**AttractionFactory.definition()) for _ in range(n)]

        if len(attractions) == 1:
            return attractions[0]

        return attractions
