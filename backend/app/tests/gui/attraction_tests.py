import unittest

from utils import create_browser_driver, click_element, TRAVVY_EXPLORERS_URL


class AttractionPageTests(unittest.TestCase):
    def setUp(self):
        self.browser = create_browser_driver()
        self.browser.visit(TRAVVY_EXPLORERS_URL)
        self.browser.click_link_by_text("Attractions")

    def test_attractions(self):
        self.assertTrue(
            self.browser.is_text_present("Jewish Childrens Museum", wait_time=10)
        )
        self.assertTrue(
            self.browser.is_text_present(
                "Jewish Childrens Museum is an indoor venue and has a rating of 3. It is 0.2 kilometers from New York City."
            )
        )
        self.assertTrue(
            self.browser.is_text_present(
                "If you are interested in museums, children museums, cultural, and interesting places, this is the place to be!"
            )
        )
        self.assertTrue(self.browser.is_text_present("New York City"))

        self.assertTrue(self.browser.is_text_present("Weeksville Heritage Center"))
        self.assertTrue(
            self.browser.is_text_present(
                "Weeksville Heritage Center is an indoor venue and has a rating of 3. It is 1.3 kilometers from New York City."
            )
        )
        self.assertTrue(
            self.browser.is_text_present(
                "If you are interested in historic, historical places, museums, cultural, interesting places, historic districts, and history museums, this is the place to be!"
            )
        )

    def test_attraction_grid_pagination(self):
        click_element(self.browser, "5")

        self.assertTrue(self.browser.is_text_present("Central Baptist Church"))
        self.assertTrue(
            self.browser.is_text_present(
                "Central Baptist Church is an indoor venue and has a rating of 3. It is 2.7 kilometers from New York City."
            )
        )
        self.assertTrue(
            self.browser.is_text_present(
                "If you are interested in religion, churches, interesting places, and other churches, this is the place to be!"
            )
        )
        self.assertTrue(self.browser.is_text_present("New York City"))

        self.assertTrue(self.browser.is_text_present("Mode Gakuen Cocoon Tower"))
        self.assertTrue(
            self.browser.is_text_present(
                "Mode Gakuen Cocoon Tower is an indoor venue and has a rating of 3. It is 0.5 kilometers from Tokyo."
            )
        )
        self.assertTrue(
            self.browser.is_text_present(
                "If you are interested in skyscrapers, architecture, and interesting places, this is the place to be!"
            )
        )
        self.assertTrue(self.browser.is_text_present("Tokyo"))

    def test_attraction_instance_page_link_from_attraction_grid(self):
        click_element(self.browser, "Fulton Park")

        self.assertTrue(self.browser.is_text_present("Fulton Park", wait_time=5))
        self.assertTrue(self.browser.is_text_present("3"))
        self.assertTrue(self.browser.is_text_present("1700 Fulton Street"))
        self.assertTrue(
            self.browser.is_text_present(
                "Fulton Park is a park in Bedford-Stuyvesant, Brooklyn, New York City, named after Robert Fulton, who is best known for launching the first commercially successful steamboat. The site on Chauncey Street was acquired by the City in 1904 for just over $300,000."
            )
        )

    def tearDown(self):
        self.browser.quit()


if __name__ == "__main__":
    unittest.main()
