import os

from selenium import webdriver
from splinter import Browser

TRAVVY_EXPLORERS_URL = (
    os.getenv("TRAVVY_EXPLORERS_URL")
    if "TRAVVY_EXPLORERS_URL" in os.environ
    else "http://localhost:3000"
)


def create_browser_driver():
    if "SELENIUM_URL" in os.environ:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")

        return Browser(
            driver_name="remote",
            browser="chrome",
            command_executor=os.getenv("SELENIUM_URL"),
            options=chrome_options,
        )

    return Browser("chrome")


def click_element(browser, text):
    browser.find_by_text(text, wait_time=5).last.click()
