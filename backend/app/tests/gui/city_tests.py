import unittest

from utils import create_browser_driver, click_element, TRAVVY_EXPLORERS_URL


class CityPageTests(unittest.TestCase):
    def setUp(self):
        self.browser = create_browser_driver()
        self.browser.visit(TRAVVY_EXPLORERS_URL)
        self.browser.click_link_by_text("Cities")

    def test_cities(self):
        self.assertTrue(self.browser.is_text_present("City", wait_time=10))
        self.assertTrue(self.browser.is_text_present("Country"))
        self.assertTrue(self.browser.is_text_present("Region"))
        self.assertTrue(self.browser.is_text_present("Elevation"))
        self.assertTrue(self.browser.is_text_present("Latitude"))
        self.assertTrue(self.browser.is_text_present("Longitude"))
        self.assertTrue(self.browser.is_text_present("Population"))
        self.assertTrue(self.browser.is_text_present("Timezone"))

        self.assertTrue(self.browser.is_text_present("New York City"))
        self.assertTrue(self.browser.is_text_present("United States of America"))
        self.assertTrue(self.browser.is_text_present("New York"))
        self.assertTrue(self.browser.is_text_present("10"))
        self.assertTrue(self.browser.is_text_present("40.67"))
        self.assertTrue(self.browser.is_text_present("-73.94"))
        self.assertTrue(self.browser.is_text_present("8,398,748"))
        self.assertTrue(self.browser.is_text_present("America__New_York"))

        self.assertTrue(self.browser.is_text_present("Melbourne"))
        self.assertTrue(self.browser.is_text_present("Australia"))
        self.assertTrue(self.browser.is_text_present("Victoria"))
        self.assertTrue(self.browser.is_text_present("31"))
        self.assertTrue(self.browser.is_text_present("-37.8206"))
        self.assertTrue(self.browser.is_text_present("144.961"))
        self.assertTrue(self.browser.is_text_present("4,529,500"))
        self.assertTrue(self.browser.is_text_present("Australia__Melbourne"))

    def test_city_pagination(self):
        click_element(self.browser, "5")

        self.assertTrue(self.browser.is_text_present("Copenhagen", wait_time=10))
        self.assertTrue(self.browser.is_text_present("Denmark"))
        self.assertTrue(self.browser.is_text_present("Capital Region of Denmark"))
        self.assertTrue(self.browser.is_text_present("16"))
        self.assertTrue(self.browser.is_text_present("55.6761"))
        self.assertTrue(self.browser.is_text_present("12.5689"))
        self.assertTrue(self.browser.is_text_present("602,481"))
        self.assertTrue(self.browser.is_text_present("Europe__Copenhagen"))

        self.assertTrue(self.browser.is_text_present("Manchester"))
        self.assertTrue(self.browser.is_text_present("United Kingdom"))
        self.assertTrue(self.browser.is_text_present("England"))
        self.assertTrue(self.browser.is_text_present("38"))
        self.assertTrue(self.browser.is_text_present("53.4667"))
        self.assertTrue(self.browser.is_text_present("-2.23333"))
        self.assertTrue(self.browser.is_text_present("547,627"))
        self.assertTrue(self.browser.is_text_present("Europe__London"))

    def test_city_instance_page(self):
        self.browser.find_by_text("Tokyo", wait_time=5).first.click()

        self.assertTrue(self.browser.is_text_present("Tokyo"))
        self.assertTrue(
            self.browser.is_text_present(
                "is the 20th constructed and 18th operating temple of The Church of Jesus Christ of Latter-day Saints (LDS Church). Located in Minato, Tokyo, Japan, it was the first temple built in Asia."
            )
        )
        self.assertTrue(self.browser.is_text_present("13,942,856"))
        self.assertTrue(self.browser.is_text_present("6 meters"))
        self.assertTrue(self.browser.is_text_present("35.69"))
        self.assertTrue(self.browser.is_text_present("139.69"))
        self.assertTrue(self.browser.is_text_present("Asia__Tokyo"))
        self.assertTrue(self.browser.is_text_present("51"))
        self.assertTrue(self.browser.is_text_present("20"))
        self.assertTrue(self.browser.is_text_present("3.78"))
        self.assertTrue(self.browser.is_text_present("457,104"))

    def tearDown(self):
        self.browser.quit()


if __name__ == "__main__":
    unittest.main()
