import unittest

from about_page import AboutPageTests
from attraction_tests import AttractionPageTests
from hotels_tests import HotelPageTests
from city_tests import CityPageTests

# Add Selenium GUI test files here
about_page_tests = unittest.TestLoader().loadTestsFromTestCase(AboutPageTests)
attraction_page_tests = unittest.TestLoader().loadTestsFromTestCase(AttractionPageTests)
hotel_page_tests = unittest.TestLoader().loadTestsFromTestCase(HotelPageTests)
city_page_tests = unittest.TestLoader().loadTestsFromTestCase(CityPageTests)

# Create a test suite combining all Selenium GUI tests
test_suite = unittest.TestSuite(
    [about_page_tests, attraction_page_tests, hotel_page_tests, city_page_tests]
)

result = unittest.TextTestRunner(verbosity=2).run(test_suite)

if result.wasSuccessful():
    exit(0)
exit(1)
