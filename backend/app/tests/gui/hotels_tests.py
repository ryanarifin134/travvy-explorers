import unittest

from utils import create_browser_driver, click_element, TRAVVY_EXPLORERS_URL


class HotelPageTests(unittest.TestCase):
    def setUp(self):
        self.browser = create_browser_driver()
        self.browser.visit(TRAVVY_EXPLORERS_URL)
        self.browser.click_link_by_text("Hotels")

    def test_hotels(self):
        self.assertTrue(self.browser.is_text_present("Name", wait_time=5))
        self.assertTrue(self.browser.is_text_present("City"))
        self.assertTrue(self.browser.is_text_present("Distance"))
        self.assertTrue(self.browser.is_text_present("Price per Night"))
        self.assertTrue(self.browser.is_text_present("Rating"))
        self.assertTrue(self.browser.is_text_present("Amenities"))

        self.assertTrue(self.browser.is_text_present("Best Western Plus Arena Hotel"))
        self.assertTrue(self.browser.is_text_present("New York City"))
        self.assertTrue(self.browser.is_text_present("1.2"))
        self.assertTrue(self.browser.is_text_present("$131.55"))
        self.assertTrue(
            self.browser.is_text_present(
                "Gym, High speed internet, Safe deposit box, 24 hour front desk..."
            )
        )

        self.assertTrue(self.browser.is_text_present("Condor Hotel"))
        self.assertTrue(self.browser.is_text_present("3.4"))
        self.assertTrue(self.browser.is_text_present("$229.67"))
        self.assertTrue(self.browser.is_text_present("4"))
        self.assertTrue(
            self.browser.is_text_present(
                "Wheelchair accessible elevators, Handrails bathroom, Tv subtitles/caption, Wide corridors..."
            )
        )

    def test_hotel_pagination(self):
        click_element(self.browser, "5")

        self.assertTrue(self.browser.is_text_present("Fairfield Inn", wait_time=5))
        self.assertTrue(self.browser.is_text_present("New York City"))
        self.assertTrue(self.browser.is_text_present("6.8"))
        self.assertTrue(self.browser.is_text_present("$112.51"))
        self.assertTrue(
            self.browser.is_text_present(
                "Spa, Safe deposit box, Laundry service, Vending machines..."
            )
        )

        self.assertTrue(self.browser.is_text_present("Hotel Indigo Lower East Side"))
        self.assertTrue(self.browser.is_text_present("7"))
        self.assertTrue(self.browser.is_text_present("$145.79"))
        self.assertTrue(self.browser.is_text_present("4"))
        self.assertTrue(
            self.browser.is_text_present(
                "24 hour front desk, Coffee shop, Concierge, Conference facilities..."
            )
        )

    def test_if_instance_pages_work(self):
        click_element(self.browser, "Gowanus Inn & Yard")

        self.assertTrue(self.browser.is_text_present("Gowanus Inn & Yard"))
        self.assertTrue(self.browser.is_text_present("New York City"))
        self.assertTrue(
            self.browser.is_text_present(
                "Gowanus Inn & Yard welcomes you with loft-inspired guest rooms, friendly service, and all that you need for a comfortable stay in New York City."
            )
        )
        self.assertTrue(self.browser.is_text_present("Phone"))
        self.assertTrue(self.browser.is_text_present("+1 718 8556503"))
        self.assertTrue(self.browser.is_text_present("Email"))
        self.assertTrue(self.browser.is_text_present("Melissa.esposito@gowanusinnand"))
        self.assertTrue(self.browser.is_text_present("Distance from center of city"))
        self.assertTrue(self.browser.is_text_present("3.3 KM"))

    def tearDown(self):
        self.browser.quit()


if __name__ == "__main__":
    unittest.main()
