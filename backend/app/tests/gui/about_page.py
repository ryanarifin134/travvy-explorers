import unittest

from utils import create_browser_driver, TRAVVY_EXPLORERS_URL


class AboutPageTests(unittest.TestCase):
    def setUp(self):
        self.browser = create_browser_driver()
        self.browser.visit(TRAVVY_EXPLORERS_URL)
        self.browser.click_link_by_text("About")

    def test_team_members_exist(self):
        self.assertTrue(self.browser.is_text_present("Ryan Arifin"))
        self.assertTrue(self.browser.is_text_present("Tristan Blake"))
        self.assertTrue(self.browser.is_text_present("Pooja Chivukula"))
        self.assertTrue(self.browser.is_text_present("Jaden Hyde"))
        self.assertTrue(self.browser.is_text_present("Erika Tan"))

    def test_repository_statistics_exist(self):
        self.assertTrue(self.browser.is_text_present("Repository Statistics"))
        self.assertTrue(self.browser.is_text_present("Total Commits:"))
        self.assertTrue(self.browser.is_text_present("Total Issues:"))
        self.assertTrue(self.browser.is_text_present("Total Tests:"))

    def test_tools_used_exist(self):
        self.assertTrue(self.browser.is_text_present("GeoDB"))
        self.assertTrue(self.browser.is_text_present("Amadeus"))
        self.assertTrue(self.browser.is_text_present("React"))
        self.assertTrue(self.browser.is_text_present("Flask"))
        self.assertTrue(self.browser.is_text_present("Postman"))
        self.assertTrue(self.browser.is_text_present("AWS"))
        self.assertTrue(self.browser.is_text_present("Docker"))
        self.assertTrue(self.browser.is_text_present("GitLab"))
        self.assertTrue(self.browser.is_text_present("Slack"))
        self.assertTrue(self.browser.is_text_present("Travvy Explorers Postman"))
        self.assertTrue(self.browser.is_text_present("Travvy Explorers GitLab"))

    def tearDown(self):
        self.browser.quit()


if __name__ == "__main__":
    unittest.main()
