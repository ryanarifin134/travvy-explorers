import os
import tempfile

import pytest

from app import app, db


@pytest.fixture
def client():

    app.config["TESTING"] = True
    app.testing = True

    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with app.test_client() as client:
        # Create fresh database before each test
        db.create_all()
        yield client
        # Clear the database after test
        db.drop_all()
