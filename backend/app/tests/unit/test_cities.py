from app import City, db


def test_get_cities_has_cities_field(client):
    r = client.get("/api/cities")
    assert "cities" in r.json


def test_empty_cities(client):
    r = client.get("/api/cities")
    assert r.json["totalCount"] == 0
    assert len(r.json["cities"]) == 0


def test_get_cities_single(client):
    # Create a city and insert into the db
    city = City.create()

    db.session.add(city)
    db.session.commit()

    r = client.get("/api/cities")
    assert len(r.json["cities"]) == 1

    assert r.json["cities"][0]["name"] == city.city
    assert r.json["cities"][0]["country"] == city.country
    assert r.json["cities"][0]["region"] == city.region


def test_get_cities_multiple(client):
    cities_created = 4
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities")
    assert len(r.json["cities"]) == cities_created

    assert r.json["cities"][0]["name"] == cities[0].city
    assert r.json["cities"][0]["country"] == cities[0].country
    assert r.json["cities"][0]["region"] == cities[0].region

    assert r.json["cities"][1]["name"] == cities[1].city
    assert r.json["cities"][1]["country"] == cities[1].country
    assert r.json["cities"][1]["region"] == cities[1].region


def test_get_cities_has_total_count_field(client):
    r = client.get("/api/cities")
    assert "totalCount" in r.json


def test_total_city_count_empty(client):
    r = client.get("/api/cities")
    assert r.json["totalCount"] == 0


def test_total_city_count_multiple(client):
    cities_created = 283
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities")
    assert r.json["totalCount"] == cities_created


def test_city_pagination_same_fields_as_default(client):
    cities_created = 153
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?page=1")
    assert "cities" in r.json
    assert "totalCount" in r.json


def test_empty_cities_page(client):
    r = client.get("/api/cities?page=1")
    assert r.json["totalCount"] == 0
    assert len(r.json["cities"]) == 0


def test_city_pagination_cities_on_page_count(client):
    cities_created = 40
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()
    r = client.get("/api/cities?page=3")
    assert len(r.json["cities"]) == 10


def test_city_pagination_total_city_count_on_page(client):
    cities_created = 147
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()
    r = client.get("/api/cities?page=7")
    assert r.json["totalCount"] == cities_created


def test_edge_page_is_flush_when_amount_divisible_by_cities_on_page_constant(client):
    cities_created = 100
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()
    # Go to last page to check if right amount are there
    r = client.get("/api/cities?page=10")
    assert len(r.json["cities"]) == 10


def test_edge_page_is_not_flush_when_amount_not_divisible_by_cities_on_page_constant(
    client,
):
    cities_created = 103
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()
    # Go to last page to check if right amount are there
    r = client.get("/api/cities?page=11")
    assert len(r.json["cities"]) == 3


def test_city_detailed_view(client):
    city = City.create()
    db.session.add(city)
    db.session.commit()
    # Once city is commited, an id is created and automatically updated!
    id = city.id
    r = client.get(f"/api/cities/{id}")
    assert r.json["name"] == city.city


def test_city_invalid_id_has_error(client):
    r = client.get("/api/cities/1")
    # ID of 1 should not exist as database is empty
    assert "error" in r.json


def test_city_invalid_id_error_message(client):
    r = client.get("/api/cities/1")
    assert r.json["error"] == "1 not a valid id"
