import random

from app import City, Hotel, db


def test_get_hotels_has_hotels_field(client):
    r = client.get("/api/hotels")
    assert "hotels" in r.json


def test_empty_hotels(client):
    r = client.get("/api/hotels")
    assert r.json["totalCount"] == 0
    assert len(r.json["hotels"]) == 0


def test_get_hotels_single(client):
    # Create a hotel and insert into the db
    hotel = Hotel.create()

    db.session.add(hotel)
    db.session.commit()

    r = client.get("/api/hotels")
    assert len(r.json["hotels"]) == 1

    assert r.json["hotels"][0]["name"] == hotel.name
    assert r.json["hotels"][0]["description"] == hotel.description
    assert r.json["hotels"][0]["rating"] == hotel.rating


def test_get_hotels_multiple(client):
    hotels_created = 4
    hotels = Hotel.create(hotels_created)
    db.session.add_all(hotels)
    db.session.commit()

    r = client.get("/api/hotels")
    assert len(r.json["hotels"]) == hotels_created

    assert r.json["hotels"][0]["name"] == hotels[0].name
    assert r.json["hotels"][0]["description"] == hotels[0].description
    assert r.json["hotels"][0]["rating"] == hotels[0].rating

    assert r.json["hotels"][1]["name"] == hotels[1].name
    assert r.json["hotels"][1]["description"] == hotels[1].description
    assert r.json["hotels"][1]["rating"] == hotels[1].rating


def test_get_hotels_has_total_count_field(client):
    r = client.get("/api/hotels")
    assert "totalCount" in r.json


def test_total_hotel_count_empty(client):
    r = client.get("/api/hotels")
    assert r.json["totalCount"] == 0


def test_total_hotel_count_multiple(client):
    hotels_created = 283
    hotels = Hotel.create(hotels_created)
    db.session.add_all(hotels)
    db.session.commit()

    r = client.get("/api/hotels")
    assert r.json["totalCount"] == hotels_created


def test_hotel_pagination_same_fields_as_default(client):
    hotels_created = 153
    hotels = Hotel.create(hotels_created)
    db.session.add_all(hotels)
    db.session.commit()

    r = client.get("/api/hotels?page=1")
    assert "hotels" in r.json
    assert "totalCount" in r.json


def test_empty_hotels_page(client):
    r = client.get("/api/hotels?page=1")
    assert r.json["totalCount"] == 0
    assert len(r.json["hotels"]) == 0


def test_hotel_pagination_hotels_on_page_count(client):
    hotels_created = 40
    hotels = Hotel.create(hotels_created)
    db.session.add_all(hotels)
    db.session.commit()
    r = client.get("/api/hotels?page=3")
    assert len(r.json["hotels"]) == 10


def test_hotel_pagination_total_hotel_count_on_page(client):
    hotels_created = 147
    hotels = Hotel.create(hotels_created)
    db.session.add_all(hotels)
    db.session.commit()
    r = client.get("/api/hotels?page=7")
    assert r.json["totalCount"] == hotels_created


def test_edge_page_is_flush_when_amount_divisible_by_hotels_on_page_constant(client):
    hotels_created = 100
    hotels = Hotel.create(hotels_created)
    db.session.add_all(hotels)
    db.session.commit()
    # Go to last page to check if right amount are there
    r = client.get("/api/hotels?page=10")
    assert len(r.json["hotels"]) == 10


def test_edge_page_is_not_flush_when_amount_not_divisible_by_hotels_on_page_constant(
    client,
):
    hotels_created = 103
    hotels = Hotel.create(hotels_created)
    db.session.add_all(hotels)
    db.session.commit()
    # Go to last page to check if right amount are there
    r = client.get("/api/hotels?page=11")
    assert len(r.json["hotels"]) == 3


def test_hotel_detailed_view(client):
    hotel = Hotel.create()
    db.session.add(hotel)
    db.session.commit()
    # Once hotel is commited, an id is created and automatically updated!
    id = hotel.id
    r = client.get(f"/api/hotels/{id}")
    assert r.json["hotel"]["name"] == hotel.name


def test_get_hotels_from_city(client):
    city = City.create()
    db.session.add(city)
    db.session.commit()

    # City created, now create hotels and link them together!
    hotels_created = 35
    hotels = Hotel.create(hotels_created)

    hotels_linked_to_city = 10
    linked_hotels = random.sample(hotels, hotels_linked_to_city)
    for hotel in linked_hotels:
        hotel.city_id = city.id

    db.session.add_all(hotels)
    db.session.commit()

    r = client.get("/api/hotels")
    assert r.json["totalCount"] == 35

    r = client.get(f"/api/hotels/city/{city.id}")
    count = 0
    for hotel in r.json["hotels"]:
        count += 1
        assert hotel["cityId"] == city.id

    assert count == hotels_linked_to_city


def test_hotel_invalid_id_has_error(client):
    r = client.get("/api/hotels/1")
    # ID of 1 should not exist as database is empty
    assert "error" in r.json


def test_hotel_invalid_id_error_message(client):
    r = client.get("/api/hotels/1")
    assert r.json["error"] == "1 not a valid id"
