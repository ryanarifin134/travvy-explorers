from app import City, db


def test_city_sort_by_city(client):
    cities_created = 462
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=city")
    # Iterate through results and ensure ascending values for cities by default
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["name"] >= prev_city["name"]
        prev_city = city


def test_city_sort_by_city_descending(client):
    cities_created = 289
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=city&order=descending")
    # Iterate through results and ensure descending values for cities
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["name"] <= prev_city["name"]
        prev_city = city


def test_city_sort_by_country(client):
    cities_created = 194
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=country")
    # Iterate through results and ensure ascending values for countries
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["country"] >= prev_city["country"]
        prev_city = city


def test_city_sort_by_country_descending(client):
    cities_created = 324
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=country&order=descending")
    # Iterate through results and ensure descending values for countries
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["country"] <= prev_city["country"]
        prev_city = city


def test_city_sort_by_region(client):
    cities_created = 351
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=region")
    # Iterate through results and ensure ascending values for regions
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["region"] >= prev_city["region"]
        prev_city = city


def test_city_sort_by_region_descending(client):
    cities_created = 73
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=region&order=descending")
    # Iterate through results and ensure descending values for regions
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["region"] <= prev_city["region"]
        prev_city = city


def test_city_sort_by_population(client):
    cities_created = 217
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=population")
    # Iterate through results and ensure ascending values for populations
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["population"] >= prev_city["population"]
        prev_city = city


def test_city_sort_by_population_descending(client):
    cities_created = 743
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=population&order=descending")
    # Iterate through results and ensure descending values for populations
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["population"] <= prev_city["population"]
        prev_city = city


def test_city_sort_by_elevation(client):
    cities_created = 104
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=elevation")
    # Iterate through results and ensure ascending values for elevations
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["elevation"] >= prev_city["elevation"]
        prev_city = city


def test_city_sort_by_elevation_descending(client):
    cities_created = 483
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=elevation&order=descending")
    # Iterate through results and ensure descending values for elevations
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["elevation"] <= prev_city["elevation"]
        prev_city = city


def test_city_sort_by_timezone(client):
    cities_created = 293
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=timezone")
    # Iterate through results and ensure ascending values for timezones
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["timezone"] >= prev_city["timezone"]
        prev_city = city


def test_city_sort_by_timezone_descending(client):
    cities_created = 321
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=timezone&order=descending")
    # Iterate through results and ensure descending values for timezones
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["timezone"] <= prev_city["timezone"]
        prev_city = city


def test_city_sort_by_covid_cases(client):
    cities_created = 592
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=covid_cases")
    # Iterate through results and ensure ascending values for covid cases
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["covidCases"] >= prev_city["covidCases"]
        prev_city = city


def test_city_sort_by_covid_cases_descending(client):
    cities_created = 257
    cities = City.create(cities_created)
    db.session.add_all(cities)
    db.session.commit()

    r = client.get("/api/cities?sort_by=covid_cases&order=descending")
    # Iterate through results and ensure descending values for covid cases
    cities = iter(r.json["cities"])
    prev_city = next(cities)
    for city in cities:
        assert city["covidCases"] <= prev_city["covidCases"]
        prev_city = city
