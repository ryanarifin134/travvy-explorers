from app import City, db


def test_compare_cities_exists_empty(client):
    r = client.get("/api/cities/compare/1")
    # Should return 400 code indicating a syntax error.
    # It should NOT return 404, as the route is valid, just client error
    assert r.status_code == 400


def test_compare_cities_exists_populated(client):
    cities = City.create(234)
    db.session.add_all(cities)
    db.session.commit()
    r = client.get("/api/cities/compare/1")
    assert r.status_code == 400


def test_compare_cities_error_on_one_city(client):
    r = client.get("/api/cities/compare/1")
    assert r.status_code == 400
    assert "error" in r.json


def test_compare_cities_error_on_five_cities(client):
    r = client.get("/api/cities/compare/1/2/3/4/5")
    assert r.status_code == 400
    assert "error" in r.json


def test_compare_cities_valid_on_four_cities(client):
    cities = City.create(123)
    db.session.add_all(cities)
    db.session.commit()
    r = client.get("/api/cities/compare/1/2/3/4")
    assert r.status_code == 200


def test_compare_cities_error_on_invalid_id(client):
    cities = City.create(123)
    db.session.add_all(cities)
    db.session.commit()
    # id of 300 should not exist in a database with 123 cities
    r = client.get("/api/cities/compare/1/2/3/300")
    assert r.status_code == 404
    assert "error" in r.json


def test_compare_cities_error_on_invalid_id_non_integer(client):
    cities = City.create(123)
    db.session.add_all(cities)
    db.session.commit()
    r = client.get("/api/cities/compare/1/2/3/something")
    assert r.status_code == 404
    assert "error" in r.json


def test_compare_cities_valid_on_three_cities(client):
    cities = City.create(123)
    db.session.add_all(cities)
    db.session.commit()
    r = client.get("/api/cities/compare/1/2/3")
    assert r.status_code == 200


def test_compare_cities_valid_on_two_cities(client):
    cities = City.create(123)
    db.session.add_all(cities)
    db.session.commit()
    r = client.get("/api/cities/compare/1/2")
    assert r.status_code == 200


def test_compare_cities_displays_city_info(client):
    cities = City.create(2)
    city1, city2 = cities
    db.session.add_all(cities)
    db.session.commit()

    r = client.get(f"/api/cities/compare/{city1.id}/{city2.id}")

    assert "cities" in r.json
    assert r.json["cities"][0]["name"] == city1.city
    assert r.json["cities"][1]["name"] == city2.city
    assert r.json["cities"][0]["covidCases"] == city1.covid_cases
    assert r.json["cities"][1]["covidCases"] == city2.covid_cases


def test_compare_cities_displays_ranking_info(client):
    cities = City.create(2)
    city1, city2 = cities
    city1.covid_cases = 100
    city2.covid_cases = 200
    city1.population = 1000
    city2.population = 2000
    db.session.add_all(cities)
    db.session.commit()

    r = client.get(f"/api/cities/compare/{city1.id}/{city2.id}")

    assert "cities" in r.json
    assert "rankings" in r.json["cities"][0]
    assert r.json["cities"][0]["rankings"]["covidCases"] == 1
    assert r.json["cities"][1]["rankings"]["covidCases"] == 2
    assert r.json["cities"][0]["rankings"]["population"] == 2
    assert r.json["cities"][1]["rankings"]["population"] == 1
