from app import Attraction, db


def create_attractions_with_attribute_value(num_attractions, attribute, attribute_val):
    attractions = Attraction.create(num_attractions)
    for attraction in attractions:
        setattr(attraction, attribute, attribute_val)
    db.session.add_all(attractions)
    db.session.commit()


def test_attraction_indoor_filter(client):
    attractions_indoor = 423
    create_attractions_with_attribute_value(
        attractions_indoor, "venue_type", "indoor venue"
    )

    attractions_outdoor = 24
    create_attractions_with_attribute_value(
        attractions_outdoor, "venue_type", "outdoor venue"
    )

    total_attractions = attractions_indoor + attractions_outdoor

    r = client.get("/api/attractions")
    # Check that all attractions have been created in db
    assert r.json["totalCount"] == total_attractions

    # Apply filter to Country_A
    r = client.get("/api/attractions?indoor=true")
    assert r.json["totalCount"] == attractions_indoor
    # Apply filter to Country_B
    r = client.get("/api/attractions?indoor=false")
    assert r.json["totalCount"] == attractions_outdoor


def test_attraction_rating_filter(client):
    ratingOne = 23
    create_attractions_with_attribute_value(ratingOne, "rating", "1")

    ratingTwo = 69
    create_attractions_with_attribute_value(ratingTwo, "rating", "2")

    ratingThree = 200

    create_attractions_with_attribute_value(ratingThree, "rating", "3")

    r = client.get("/api/attractions")
    total_attractions = ratingOne + ratingTwo + ratingThree
    # Check that all attractions have been created in db
    assert r.json["totalCount"] == total_attractions

    # Apply filter to rating 1
    r = client.get("/api/attractions?AttractionRating=1")
    assert r.json["totalCount"] == ratingOne
    # Apply filter to rating 2
    r = client.get("/api/attractions?AttractionRating=2")
    assert r.json["totalCount"] == ratingTwo
    # Apply filter to rating 3
    r = client.get("/api/attractions?AttractionRating=3")
    assert r.json["totalCount"] == ratingThree


def test_attraction_category_filter(client):

    category1 = 10
    category2 = 11
    category3 = 12
    category4 = 13
    category5 = 14
    category6 = 15
    category7 = 16
    category8 = 17
    category9 = 18
    category10 = 19
    category11 = 20

    create_attractions_with_attribute_value(category1, "category", "interesting_places")
    create_attractions_with_attribute_value(category2, "category", "architecture")
    create_attractions_with_attribute_value(category3, "category", "cultural")
    create_attractions_with_attribute_value(category4, "category", "historic")
    create_attractions_with_attribute_value(
        category5, "category", "industrial_facilities"
    )
    create_attractions_with_attribute_value(category6, "category", "natural")
    create_attractions_with_attribute_value(category7, "category", "religion")
    create_attractions_with_attribute_value(category8, "category", "sport")
    create_attractions_with_attribute_value(category9, "category", "shops")
    create_attractions_with_attribute_value(category10, "category", "food")
    create_attractions_with_attribute_value(category11, "category", "amusements")

    r = client.get("/api/attractions")
    total_attractions = (
        category1
        + category2
        + category3
        + category4
        + category5
        + category6
        + category7
        + category8
        + category9
        + category10
        + category11
    )
    # Check that all attractions have been created in db
    assert r.json["totalCount"] == total_attractions

    # Apply filter to amusement category
    r = client.get("/api/attractions?AttractionCategory=amusements")
    assert r.json["totalCount"] == category11
    # Apply filter to interesting_places category
    r = client.get("/api/attractions?AttractionCategory=interesting_places")
    assert r.json["totalCount"] == category1


def test_attraction_distance_filter(client):
    distanceOne = 5
    create_attractions_with_attribute_value(distanceOne, "city_distance", "8")

    distanceTwo = 35
    create_attractions_with_attribute_value(distanceTwo, "city_distance", "35")

    r = client.get("/api/attractions")
    total_attractions = distanceOne + distanceTwo
    # Check that all attractions have been created in db
    assert r.json["totalCount"] == total_attractions

    # Apply filter to city1
    r = client.get("/api/attractions?AttractionDistance=8")
    assert r.json["totalCount"] == distanceOne
    # Apply filter to city2
    r = client.get("/api/attractions?AttractionDistance=35")
    assert r.json["totalCount"] == distanceTwo + distanceOne


def test_attraction_sort_by_distance(client):
    attractions_created = 462
    attractions = Attraction.create(attractions_created)
    db.session.add_all(attractions)
    db.session.commit()

    r = client.get("/api/attractions?sortby=CityDistance")
    # Iterate through results and ensure ascending values for cities by default
    attractions = iter(r.json["attractions"])
    prev_attraction = next(attractions)
    for attraction in attractions:
        assert attraction["city_distance"] >= prev_attraction["city_distance"]
        prev_attraction = attraction
