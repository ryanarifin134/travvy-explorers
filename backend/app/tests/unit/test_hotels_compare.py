from app import Hotel, db


def test_compare_hotels_exists_empty(client):
    r = client.get("/api/hotels/compare/1")
    # Should return 400 code indicating a syntax error.
    # It should NOT return 404, as the route is valid, just client error
    assert r.status_code == 400


def test_compare_hotels_exists_populated(client):
    hotels = Hotel.create(234)
    db.session.add_all(hotels)
    db.session.commit()
    r = client.get("/api/hotels/compare/1")
    assert r.status_code == 400


def test_compare_hotels_error_on_one_hotel(client):
    r = client.get("/api/hotels/compare/1")
    assert r.status_code == 400
    assert "error" in r.json


def test_compare_hotels_error_on_five_hotels(client):
    r = client.get("/api/hotels/compare/1/2/3/4/5")
    assert r.status_code == 400
    assert "error" in r.json


def test_compare_hotels_valid_on_four_hotels(client):
    hotels = Hotel.create(123)
    db.session.add_all(hotels)
    db.session.commit()
    r = client.get("/api/hotels/compare/1/2/3/4")
    assert r.status_code == 200


def test_compare_hotels_error_on_invalid_id(client):
    hotels = Hotel.create(123)
    db.session.add_all(hotels)
    db.session.commit()
    # id of 300 should not exist in a database with 123 hotels
    r = client.get("/api/hotels/compare/1/2/3/300")
    assert r.status_code == 404
    assert "error" in r.json


def test_compare_hotels_error_on_invalid_id_non_integer(client):
    hotels = Hotel.create(123)
    db.session.add_all(hotels)
    db.session.commit()
    r = client.get("/api/hotels/compare/1/2/3/something")
    assert r.status_code == 404
    assert "error" in r.json


def test_compare_hotels_valid_on_three_hotels(client):
    hotels = Hotel.create(123)
    db.session.add_all(hotels)
    db.session.commit()
    r = client.get("/api/hotels/compare/1/2/3")
    assert r.status_code == 200


def test_compare_hotels_valid_on_two_hotels(client):
    hotels = Hotel.create(123)
    db.session.add_all(hotels)
    db.session.commit()
    r = client.get("/api/hotels/compare/1/2")
    assert r.status_code == 200


def test_compare_hotels_displays_hotel_info(client):
    hotels = Hotel.create(2)
    hotel1, hotel2 = hotels
    db.session.add_all(hotels)
    db.session.commit()

    r = client.get(f"/api/hotels/compare/{hotel1.id}/{hotel2.id}")

    assert "hotels" in r.json
    assert r.json["hotels"][0]["distance"] == hotel1.distance
    assert r.json["hotels"][1]["distance"] == hotel2.distance
    assert r.json["hotels"][0]["rating"] == hotel1.rating
    assert r.json["hotels"][1]["rating"] == hotel2.rating


def test_compare_hotels_displays_ranking_info(client):
    hotels = Hotel.create(2)
    hotel1, hotel2 = hotels
    hotel1.distance = 10
    hotel2.distance = 20
    hotel1.rating = 1
    hotel2.rating = 3
    db.session.add_all(hotels)
    db.session.commit()

    r = client.get(f"/api/hotels/compare/{hotel1.id}/{hotel2.id}")

    assert "hotels" in r.json
    assert "rankings" in r.json["hotels"][0]
    assert r.json["hotels"][0]["rankings"]["distance"] == 1
    assert r.json["hotels"][1]["rankings"]["distance"] == 2
    assert r.json["hotels"][0]["rankings"]["rating"] == 2
    assert r.json["hotels"][1]["rankings"]["rating"] == 1
