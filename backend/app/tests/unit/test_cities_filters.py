from app import City, db


def create_cities_with_attribute_value(num_cities, attribute, attribute_val):
    cities = City.create(num_cities)
    for city in cities:
        setattr(city, attribute, attribute_val)
    db.session.add_all(cities)
    db.session.commit()


def test_city_country_filter(client):
    cities_in_country_a = 423
    create_cities_with_attribute_value(cities_in_country_a, "country", "Country_A")

    cities_in_country_b = 261
    create_cities_with_attribute_value(cities_in_country_b, "country", "Country_B")

    total_cities = cities_in_country_a + cities_in_country_b

    r = client.get("/api/cities")
    # Check that all cities have been created in db
    assert r.json["totalCount"] == total_cities

    # Apply filter to Country_A
    r = client.get("/api/cities?country=Country_A")
    assert r.json["totalCount"] == cities_in_country_a

    # Apply filter to Country_B
    r = client.get("/api/cities?country=Country_B")
    assert r.json["totalCount"] == cities_in_country_b


def test_city_region_filter(client):
    cities_in_region_a = 371
    create_cities_with_attribute_value(cities_in_region_a, "region", "Region_A")

    cities_in_region_b = 207
    create_cities_with_attribute_value(cities_in_region_b, "region", "Region_B")

    total_cities = cities_in_region_a + cities_in_region_b

    r = client.get("/api/cities")
    # Check that all cities have been created in db
    assert r.json["totalCount"] == total_cities

    # Apply filter to Region_A
    r = client.get("/api/cities?region=Region_A")
    assert r.json["totalCount"] == cities_in_region_a

    # Apply filter to Region_B
    r = client.get("/api/cities?region=Region_B")
    assert r.json["totalCount"] == cities_in_region_b


def test_city_timezone_filter(client):
    cities_in_timezone_a = 287
    create_cities_with_attribute_value(cities_in_timezone_a, "timezone", "Timezone_A")

    cities_in_timezone_b = 432
    create_cities_with_attribute_value(cities_in_timezone_b, "timezone", "Timezone_B")

    total_cities = cities_in_timezone_a + cities_in_timezone_b

    r = client.get("/api/cities")
    # Check that all cities have been created in db
    assert r.json["totalCount"] == total_cities

    # Apply filter to Timezone_A
    r = client.get("/api/cities?timezone=Timezone_A")
    assert r.json["totalCount"] == cities_in_timezone_a

    # Apply filter to Timezone_B
    r = client.get("/api/cities?timezone=Timezone_B")
    assert r.json["totalCount"] == cities_in_timezone_b


def test_city_min_population_filter(client):
    cities_to_create = 100
    cities = City.create(cities_to_create)
    for city_index_and_population_value in range(len(cities)):
        # Create cities with populations 0...99
        city = cities[city_index_and_population_value]
        population = city_index_and_population_value
        city.population = population

    db.session.add_all(cities)
    db.session.commit()

    r = client.get("api/cities?min_population=25")
    # 25...99 accounts for 75 populations, so 75 cities should be returned
    assert r.json["totalCount"] == 75


def test_city_max_population_filter(client):
    cities_to_create = 100
    cities = City.create(cities_to_create)
    for city_index_and_population_value in range(len(cities)):
        # Create cities with populations 0...99
        city = cities[city_index_and_population_value]
        population = city_index_and_population_value
        city.population = population

    db.session.add_all(cities)
    db.session.commit()

    r = client.get("api/cities?max_population=25")
    # 0...25 accounts for 26 populations, so 26 cities should be returned
    assert r.json["totalCount"] == 26


def test_city_min_elevation_filter(client):
    cities_to_create = 100
    cities = City.create(cities_to_create)
    for city_index_and_elevation_value in range(len(cities)):
        # Create cities with elevations 0...99
        city = cities[city_index_and_elevation_value]
        elevation = city_index_and_elevation_value
        city.elevation = elevation

    db.session.add_all(cities)
    db.session.commit()

    r = client.get("api/cities?min_elevation=25")
    # 25...99 accounts for 75 elevations, so 75 cities should be returned
    assert r.json["totalCount"] == 75


def test_city_max_elevation_filter(client):
    cities_to_create = 100
    cities = City.create(cities_to_create)
    for city_index_and_elevation_value in range(len(cities)):
        # Create cities with elevations 0...99
        city = cities[city_index_and_elevation_value]
        elevation = city_index_and_elevation_value
        city.elevation = elevation

    db.session.add_all(cities)
    db.session.commit()

    r = client.get("api/cities?max_elevation=25")
    # 0...25 accounts for 26 elevations, so 26 cities should be returned
    assert r.json["totalCount"] == 26


def test_city_min_covid_cases_filter(client):
    cities_to_create = 100
    cities = City.create(cities_to_create)
    for city_index_and_covid_cases_value in range(len(cities)):
        # Create cities with covid_cases 0...99
        city = cities[city_index_and_covid_cases_value]
        covid_cases = city_index_and_covid_cases_value
        city.covid_cases = covid_cases

    db.session.add_all(cities)
    db.session.commit()

    r = client.get("api/cities?min_covid_cases=25")
    # 25...99 accounts for 75 covid_cases, so 75 cities should be returned
    assert r.json["totalCount"] == 75


def test_city_max_covid_cases_filter(client):
    cities_to_create = 100
    cities = City.create(cities_to_create)
    for city_index_and_covid_cases_value in range(len(cities)):
        # Create cities with covid_cases 0...99
        city = cities[city_index_and_covid_cases_value]
        covid_cases = city_index_and_covid_cases_value
        city.covid_cases = covid_cases

    db.session.add_all(cities)
    db.session.commit()

    r = client.get("api/cities?max_covid_cases=25")
    # 0...25 accounts for 26 covid_cases, so 26 cities should be returned
    assert r.json["totalCount"] == 26
