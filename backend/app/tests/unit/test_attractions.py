from app import Attraction, db


def test_get_attractions_has_attractions_field(client):
    r = client.get("/api/attractions")
    assert "attractions" in r.json


def test_empty_attractions(client):
    r = client.get("/api/attractions")
    assert r.json["totalCount"] == 0
    assert len(r.json["attractions"]) == 0


def test_get_attractions_single(client):
    # Create a attraction and insert into the db
    attraction = Attraction.create()

    db.session.add(attraction)
    db.session.commit()

    r = client.get("/api/attractions")
    assert len(r.json["attractions"]) == 1

    assert r.json["attractions"][0]["name"] == attraction.name
    assert r.json["attractions"][0]["lat"] == attraction.lat
    assert r.json["attractions"][0]["address"] == attraction.address


def test_get_attractions_multiple(client):
    attractions_created = 4
    attractions = Attraction.create(attractions_created)
    db.session.add_all(attractions)
    db.session.commit()

    r = client.get("/api/attractions")
    assert len(r.json["attractions"]) == attractions_created

    assert r.json["attractions"][0]["name"] == attractions[0].name
    assert r.json["attractions"][0]["lat"] == attractions[0].lat
    assert r.json["attractions"][0]["address"] == attractions[0].address

    assert r.json["attractions"][1]["name"] == attractions[1].name
    assert r.json["attractions"][1]["lat"] == attractions[1].lat
    assert r.json["attractions"][1]["address"] == attractions[1].address


def test_get_attractions_has_total_count_field(client):
    r = client.get("/api/attractions")
    assert "totalCount" in r.json


def test_total_attraction_count_empty(client):
    r = client.get("/api/attractions")
    assert r.json["totalCount"] == 0


def test_total_attraction_count_multiple(client):
    attractions_created = 283
    attractions = Attraction.create(attractions_created)
    db.session.add_all(attractions)
    db.session.commit()

    r = client.get("/api/attractions")
    assert r.json["totalCount"] == attractions_created


def test_attraction_pagination_same_fields_as_default(client):
    attractions_created = 153
    attractions = Attraction.create(attractions_created)
    db.session.add_all(attractions)
    db.session.commit()

    r = client.get("/api/attractions?page=1")
    assert "attractions" in r.json
    assert "totalCount" in r.json


def test_empty_attractions_page(client):
    r = client.get("/api/attractions?page=1")
    assert r.json["totalCount"] == 0
    assert len(r.json["attractions"]) == 0


def test_attraction_pagination_attractions_on_page_count(client):
    attractions_created = 40
    attractions = Attraction.create(attractions_created)
    db.session.add_all(attractions)
    db.session.commit()
    r = client.get("/api/attractions?page=3")
    assert len(r.json["attractions"]) == 9


def test_attraction_pagination_total_attraction_count_on_page(client):
    attractions_created = 147
    attractions = Attraction.create(attractions_created)
    db.session.add_all(attractions)
    db.session.commit()
    r = client.get("/api/attractions?page=7")
    assert r.json["totalCount"] == attractions_created


def test_edge_page_is_flush_when_amount_divisible_by_attractions_on_page_constant(
    client,
):
    attractions_created = 90
    attractions = Attraction.create(attractions_created)
    db.session.add_all(attractions)
    db.session.commit()
    # Go to last page to check if right amount are there
    r = client.get("/api/attractions?page=10")
    assert len(r.json["attractions"]) == 9


def test_edge_page_is_not_flush_when_amount_not_divisible_by_attractions_on_page_constant(
    client,
):
    attractions_created = 93
    attractions = Attraction.create(attractions_created)
    db.session.add_all(attractions)
    db.session.commit()
    # Go to last page to check if right amount are there
    r = client.get("/api/attractions?page=11")
    assert len(r.json["attractions"]) == 3


def test_attraction_detailed_view(client):
    attraction = Attraction.create()
    db.session.add(attraction)
    db.session.commit()
    # Once attraction is commited, an id is created and automatically updated!
    id = attraction.id
    r = client.get(f"/api/attractions/{id}")
    assert r.json["name"] == attraction.name


def test_attraction_invalid_id_has_error(client):
    r = client.get("/api/attractions/1")
    # ID of 1 should not exist as database is empty
    assert "error" in r.json


def test_attraction_invalid_id_error_message(client):
    r = client.get("/api/attractions/1")
    assert r.json["error"] == "1 not a valid id"
