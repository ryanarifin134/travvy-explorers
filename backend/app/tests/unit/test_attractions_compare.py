from app import Attraction, db


def test_compare_attractions_exists_empty(client):
    r = client.get("/api/attractions/compare/1")
    # Should return 400 code indicating a syntax error.
    # It should NOT return 404, as the route is valid, just client error
    assert r.status_code == 400


def test_compare_attractions_exists_populated(client):
    attractions = Attraction.create(234)
    db.session.add_all(attractions)
    db.session.commit()
    r = client.get("/api/attractions/compare/1")
    assert r.status_code == 400


def test_compare_attractions_error_on_one_attraction(client):
    r = client.get("/api/attractions/compare/1")
    assert r.status_code == 400
    assert "error" in r.json


def test_compare_attractions_error_on_five_attractions(client):
    r = client.get("/api/attractions/compare/1/2/3/4/5")
    assert r.status_code == 400
    assert "error" in r.json


def test_compare_attractions_valid_on_four_attractions(client):
    attractions = Attraction.create(123)
    db.session.add_all(attractions)
    db.session.commit()
    r = client.get("/api/attractions/compare/1/2/3/4")
    assert r.status_code == 200


def test_compare_attractions_error_on_invalid_id(client):
    attractions = Attraction.create(123)
    db.session.add_all(attractions)
    db.session.commit()
    # id of 300 should not exist in a database with 123 attractions
    r = client.get("/api/attractions/compare/1/2/3/300")
    assert r.status_code == 404
    assert "error" in r.json


def test_compare_attractions_error_on_invalid_id_non_integer(client):
    attractions = Attraction.create(123)
    db.session.add_all(attractions)
    db.session.commit()
    r = client.get("/api/attractions/compare/1/2/3/something")
    assert r.status_code == 404
    assert "error" in r.json


def test_compare_attractions_valid_on_three_attractions(client):
    attractions = Attraction.create(123)
    db.session.add_all(attractions)
    db.session.commit()
    r = client.get("/api/attractions/compare/1/2/3")
    assert r.status_code == 200


def test_compare_attractions_valid_on_two_attractions(client):
    attractions = Attraction.create(123)
    db.session.add_all(attractions)
    db.session.commit()
    r = client.get("/api/attractions/compare/1/2")
    assert r.status_code == 200


def test_compare_attractions_displays_attraction_info(client):
    attractions = Attraction.create(2)
    attraction1, attraction2 = attractions
    db.session.add_all(attractions)
    db.session.commit()

    r = client.get(f"/api/attractions/compare/{attraction1.id}/{attraction2.id}")

    assert "attractions" in r.json
    assert r.json["attractions"][0]["city_distance"] == attraction1.city_distance
    assert r.json["attractions"][1]["city_distance"] == attraction2.city_distance
    assert r.json["attractions"][0]["rating"] == attraction1.rating
    assert r.json["attractions"][1]["rating"] == attraction2.rating


def test_compare_attractions_displays_ranking_info(client):
    attractions = Attraction.create(2)
    attraction1, attraction2 = attractions
    attraction1.city_distance = 10
    attraction2.city_distance = 20
    attraction1.rating = 1
    attraction2.rating = 3
    db.session.add_all(attractions)
    db.session.commit()

    r = client.get(f"/api/attractions/compare/{attraction1.id}/{attraction2.id}")

    assert "attractions" in r.json
    assert "rankings" in r.json["attractions"][0]
    assert r.json["attractions"][0]["rankings"]["city_distance"] == 1
    assert r.json["attractions"][1]["rankings"]["city_distance"] == 2
    assert r.json["attractions"][0]["rankings"]["rating"] == 2
    assert r.json["attractions"][1]["rankings"]["rating"] == 1
