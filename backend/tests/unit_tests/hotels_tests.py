import unittest
import requests
from constants import HOST_NAME

NUM_HOTELS = 12630


class HotelApiTests(unittest.TestCase):

    # test getting all hotel data
    def test_get_hotels(self):
        r = requests.get(f"http://{HOST_NAME}/api/hotels")
        assert r.status_code == 200
        d = r.json()
        assert len(d["hotels"]) == 10

    # test getting all hotels for a certain city
    def test_get_hotels_by_city(self):
        r = requests.get(f"http://{HOST_NAME}/api/hotels/city/8")
        assert r.status_code == 200
        d = r.json()
        for hotel in d["hotels"]:
            assert hotel["cityId"] == 8

    def test_hotel_detail(self):
        r = requests.get(f"http://{HOST_NAME}/api/hotels/hotel/1")
        assert r.status_code == 200
        d = r.json()
        assert d == {
            "hotel": {
                "id": 1,
                "name": "Best Western Plus Arena Hotel",
                "city": "New York City",
                "description": "Discover the best of New York City at this Brooklyn hotel- 100% non-smoking, close to the Brooklyn Childrens Museum and the Brooklyn Botanic Gardens. The Best Western Plus Arena Hotel, a newly-constructed hotel, is committed to providing guests with friendly customer service and comfortable accommodations at an unbeatable value. We feature spaciously-appointed guest rooms, each complete with free wireless high-speed Internet access, 42-inch LG televisions, microwave and refrigerators. Start your",
                "latitude": 40.6782,
                "longitude": -73.9488,
                "distance": 1.2,
                "distance_unit": "KM",
                "amenities": '["GYM", "HIGH_SPEED_INTERNET", "SAFE_DEPOSIT_BOX", "24_HOUR_FRONT_DESK", "ICE_MACHINES", "PARKING", "ELEVATOR", "SPRINKLERS", "SMOKE_DETECTOR", "FIRE_DETECTORS", "FEMA_FIRE_SAFETY_COMPLIANT", "EMERGENCY_LIGHTING", "VIDEO_SURVEILANCE", "BUSINESS_CENTER", "PRINTER", "AIR_CONDITIONING", "CABLE_TELEVISION", "FREE_NEWSPAPER", "REFRIGERATOR", "VOICEMAIL_IN_ROOM", "SAFE", "WAKEUP_SERVICE", "TEA/COFFEE_MAKER", "FREE_LOCAL_CALLS", "HIGH_SPEED_INTERNET_IN_ROOM", "HAIR_DRYER", "MICROWAVE"]',
                "imageUrl": "https://d2573qu6qrjt8c.cloudfront.net/807489E054424121BA679161CD823072/807489E054424121BA679161CD823072.JPEG",
                "email": "33142@hotel.bestwestern.com",
                "phone": "+1 718 6047300",
                "rating": 3,
                "price": 131.55,
                "hotelId": "BWJFK142",
                "cityId": 8,
            }
        }

    def test_hotels_error(self):
        r = requests.get(f"http://{HOST_NAME}/api/hotels/hotel/-1")
        assert r.status_code == 404
        d = r.json()
        assert d == {"error": "-1 not a valid id"}
