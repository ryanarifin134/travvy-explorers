import unittest
import requests
from constants import HOST_NAME


NUM_CITIES = 396


class CityApiTests(unittest.TestCase):

    # test getting first page
    def test_get_cities(self):
        r = requests.get(f"http://{HOST_NAME}/api/cities")
        # implicit first page
        assert r.status_code == 200
        d = r.json()
        assert len(d["cities"]) == 10

    def test_total_city_count(self):
        r = requests.get(f"http://{HOST_NAME}/api/cities")
        assert r.status_code == 200
        d = r.json()
        assert d["totalCount"] == NUM_CITIES

    # test getting page from query
    def test_get_cities(self):
        r = requests.get(f"http://{HOST_NAME}/api/cities?page=5")
        assert r.status_code == 200
        d = r.json()
        assert d["cities"][0]["id"] == 48

    def test_city_detail(self):
        r = requests.get(f"http://{HOST_NAME}/api/cities/9")
        assert r.status_code == 200
        d = r.json()
        assert d == {
            "id": 9,
            "name": "Tokyo",
            "country": "Japan",
            "region": "Tokyo",
            "latitude": 35.6897,
            "longitude": 139.692,
            "elevation": 6,
            "population": 13942856,
            "timezone": "Asia__Tokyo",
            "covidCases": 457104,
            "imageUrl": "https://images.unsplash.com/photo-1540959733332-eab4deabeeaf",
            "description": "The Tokyo Japan Temple (formerly the Tokyo Temple) (東京神殿, Tōkyō Shinden) is the 20th constructed and 18th operating temple of The Church of Jesus Christ of Latter-day Saints (LDS Church). Located in Minato, Tokyo, Japan, it was the first temple built in Asia.",
            "num_hotels": 51,
            "num_attractions": 4,
            "avg_hotel_rating": 3.784313725490196,
        }

    def test_cities_error(selff):
        r = requests.get(f"http://{HOST_NAME}/api/cities/-1")
        assert r.status_code == 404
        d = r.json()
        assert d == {"error": "-1 not a valid id"}
