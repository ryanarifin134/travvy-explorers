import os

HOST_NAME = (
    os.getenv("REACT_APP_API_HOST")
    if "REACT_APP_API_HOST" in os.environ
    else "localhost:8080"
)
