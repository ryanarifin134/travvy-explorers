import unittest
import requests
from constants import HOST_NAME

NUM_ATTRACTIONS = 1355


class AttractionApiTests(unittest.TestCase):
    def test_get_attractions(self):
        r = requests.get(f"http://{HOST_NAME}/api/attractions")
        assert r.status_code == 200
        d = r.json()
        assert len(d["attractions"]) == 9
        assert d["totalCount"] == NUM_ATTRACTIONS

    def test_attraction_detail(self):
        r = requests.get(f"http://{HOST_NAME}/api/attractions/1210")
        assert r.status_code == 200
        d = r.json()
        assert d == {
            "id": 1210,
            "name": "Moroccan Jewish Museum",
            "city": {
                "id": 347,
                "name": "Casablanca",
                "country": "Morocco",
                "region": "Casablanca-Settat",
                "latitude": 33.5992,
                "longitude": -7.62,
                "elevation": 115,
                "population": 3499000,
                "timezone": "Africa__Casablanca",
                "covidCases": 491834,
                "imageUrl": "https://images.unsplash.com/photo-1451422450617-99d28523649e",
                "description": "Casablanca (Arabic: الدار البيضاء‎, romanized: ad-dār al-bayḍāʾ; Berber languages: ⴰⵏⴼⴰ, romanized: anfa) is the largest city of Morocco. Located in the central-western part of Morocco bordering the Atlantic Ocean, it is the second largest city in the Maghreb region and the eighth-largest in the Arab world.",
                "num_hotels": 35,
                "num_attractions": 1,
                "avg_hotel_rating": 3.7142857142857144,
            },
            "city_distance": 5502,
            "venue_type": "indoor venue",
            "rating": 3,
            "lat": 33.5522,
            "lon": -7.63897,
            "category": "museums,cultural,interesting_places,other_museums",
            "description": "The Museum of Moroccan Judaism, or El Mellah Museum, is a Jewish museum in Casablanca, Morocco. Established in 1997, it is the only museum devoted to Judaism in the Arab world. The museum, whose building originated in 1948 as a Jewish orphanage that housed up to 160 Jewish youth, was renovated in 2013.The museum was founded by Simon Levy, a former professor at the University of Rabat and founder of the Foundation for the Preservation of Moroccan Jewish Culture. Prior to his role in preserving Moroccan Jewish Culture, Levy (1934 – 2011) was known as an independence and human rights activist fro",
            "address": "81 Rue du Chasseur Jules Gros",
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Jewish_Museum_of_Casablanca_Morocco.jpg/400px-Jewish_Museum_of_Casablanca_Morocco.jpg",
            "wikiUrl": "https://en.wikipedia.org/wiki/Moroccan%20Jewish%20Museum",
            "hotels_in_city": 35,
        }

    def test_attractions_error(self):
        r = requests.get(f"http://{HOST_NAME}/api/attractions/-1")
        assert r.status_code == 404
        d = r.json()
        assert d == {"error": "-1 not a valid id"}


if __name__ == "__main__":
    unittest.main()
