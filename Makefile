be-build-image:
	cd backend ; \
	docker build -t travvy-explorers-backend .

be-start:
	docker run -it --rm \
	--name travvy-explorers-backend \
	--env-file ./backend/development/config.list \
	-v "${PWD}/backend:/backend" \
	-p 8080:8080 \
	travvy-explorers-backend

be-sandbox:
	docker run -it --rm \
	--name travvy-explorers-backend \
	-v "${PWD}/backend:/backend" \
	-p 8080:8080 \
	-e TESTING=true \
	-e CITIES=$(cities) \
	-e ATTRACTIONS=$(attractions) \
	-e HOTELS=$(hotels) \
	travvy-explorers-backend

be-stop:
	docker rm --force travvy-explorers-backend

fe-build-image:
	cd frontend ; \
	docker build -t travvy-explorers-frontend .

fe-start:
	docker run -it --rm \
	--name travvy-explorers-frontend \
	-v "${PWD}/frontend:/frontend" \
	-v /frontend/node_modules \
	-p 3000:3000 \
	-e CHOKIDAR_USEPOLLING=true \
	-e REACT_APP_API_URL='http://localhost:8080'  \
	travvy-explorers-frontend

fe-stop:
	docker rm --force travvy-explorers-frontend

check:
	cd frontend ; \
	yarn eslint . --ext .js,.jsx,.ts,.tsx

run-prettier:
	prettier --write "{*.tsx,!(node*)**/*.tsx}"

run-black:
	black .

python-unit-tests:
	export TESTING=true ;\
	pytest

jest-unit-tests:
	cd frontend ; \
	yarn test

# When running locally, make sure to have backend and frontend running
gui-tests:
	python3 ./backend/app/tests/gui/selenium_tests.py

postman-tests:
	newman run ./backend/app/tests/travvyexplorers_test_suite.postman_collection.json

test:
	make python-unit-tests ; \
	make jest-unit-tests ; \
	make gui-tests ; \
	make postman-tests
