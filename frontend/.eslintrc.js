const ERROR = 2

module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: [
      '@typescript-eslint',
      'jest'
    ],
    rules: {
      'eqeqeq': [ERROR, 'always'],
      "no-alert": ERROR,
      "no-useless-concat": ERROR,
      "jsx-quotes": [ ERROR, "prefer-double" ],
    },
    extends: [
      'eslint:recommended',
      'plugin:@typescript-eslint/recommended',
      'plugin:jest/recommended'
    ],
    env: {
      node: true,
      browser: true,
    }
  };