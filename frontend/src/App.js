/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { useState } from 'react';
import {
  Switch,
  Route,
  Link
} from "react-router-dom";
import './App.scss';
import Home from './pages/Home'
import Search from './pages/Search'
import Visualizations from './pages/Visualizations'
import About from './pages/About'
import Cities from './pages/Cities'
import Attractions from './pages/Attractions'
import Hotels from './pages/Hotels'
import HotelInstance from './pages/Hotels/HotelInstance'
import AttractionInstance from './pages/Attractions/AttractionInstance'
import AttractionCompare from "./pages/Attractions/AttractionCompare"
import CityInstance from "./pages/Cities/CityInstance";
import CityCompare from "./pages/Cities/CityCompare"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons'
import HotelCompare from "./pages/Hotels/HotelCompare";

function App() {
  const [query, setQuery] = useState("");

  const handleSearchInput = event => {
    setQuery(event.target.value)
  };

  return (
    <>
        <Navbar sticky="top" bg="light">
          <Navbar.Brand href="/">
            <FontAwesomeIcon icon={faPaperPlane} size={'1x'} style={{marginRight:'7px'}}/>
            Travvy Explorers
          </Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link><Link to="/about" className="custom-nav-link">About</Link></Nav.Link>
            <Nav.Link><Link to="/cities" className="custom-nav-link">Cities</Link></Nav.Link>
            <Nav.Link><Link to="/attractions" className="custom-nav-link">Attractions</Link></Nav.Link>
            <Nav.Link><Link to="/hotels" className="custom-nav-link">Hotels</Link></Nav.Link>
            <Nav.Link><Link to="/visualizations" className="custom-nav-link">Visualizations</Link></Nav.Link>
          </Nav>
          <Form onSubmit={(e) => {e.preventDefault()}} inline>
            <FormControl onChange={handleSearchInput} type="text" placeholder="Search site" className="mr-sm-2" />
            <Link to={`/search/${query}`} className="search-link"><Button variant="outline-info">Search</Button></Link>
          </Form>
        </Navbar>

      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route exact path="/cities">
          <Cities />
        </Route>
        <Route path="/cities/compare/:cityid1?/:cityid2?/:cityid3?/:cityid4?" component={CityCompare} />
        <Route path="/cities/:cityid" component={CityInstance} />
        <Route exact path="/attractions">
          <Attractions/>
        </Route>
        <Route path="/attractions/compare/:attractionid1?/:attractionid2?/:attractionid3?/:attractionid4?" component={AttractionCompare} />
        <Route path="/attractions/:attractionid" component={AttractionInstance} >
        </Route>
        <Route exact path={"/hotels"}>
          <Hotels />
        </Route>
        <Route path="/hotels/compare/:hotelid1?/:hotelid2?/:hotelid3?/:hotelid4?" component={HotelCompare} />
        <Route
          path={"/hotels/:id"}
          render={(props) => <HotelInstance {...props} />}
        />
        <Route
          path={"/hotels/:filterCity"}
          render={(props) => <Hotels {...props} />}
        />
        <Route path="/visualizations">
          <Visualizations />
        </Route>
        <Route path="/search/:query" component={Search} />
      </Switch>
      </>
  );
}

export default App;
