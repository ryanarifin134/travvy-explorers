import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import About from "../pages/About"
import TeamMemberCard from "../components/TeamMemberCard"
import ToolCard from "../components/ToolCard"

configure({ adapter: new Adapter() })

describe("Test page views", () => {
	test("About", () => {
		const aboutTest = shallow(<About />)
		expect(aboutTest).toMatchSnapshot()
	})
})

describe("Test components", () => {
	test("TeamMemberCard", () => {
		const teamMemberCard = shallow(
			<TeamMemberCard
				name="Erika Tan"
				description="test description"
				role="test role"
				commits={0}
				issues={0}
				tests={0}
			/>
		)
		expect(teamMemberCard).toMatchSnapshot()
	})

    test("ToolCard", () => {
        const toolCard = shallow(
            <ToolCard
                link="https://flask.palletsprojects.com/en/1.1.x/"
                title="test title"
                image="https://d2573qu6qrjt8c.cloudfront.net/54CF795D30354373A6991E2CEC2A8E0D/54CF795D30354373A6991E2CEC2A8E0D.JPEG"
            />
        )
        expect(toolCard).toMatchSnapshot()
    })
})