import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import Home from "../pages/Home"

configure({ adapter: new Adapter() })

describe("Test page views", () => {
	test("Home", () => {
		const homeTest = shallow(<Home />)
		expect(homeTest).toMatchSnapshot()
	})
})