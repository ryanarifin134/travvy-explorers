import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import Hotels from "../pages/Hotels"
import HotelCard from "../components/HotelCard"

configure({ adapter: new Adapter() })

describe("Test page views", () => {
	test("Hotel", () => {
		const hotelsTest = shallow(<Hotels />)
		expect(hotelsTest).toMatchSnapshot()
	})
})

describe("Test components", () => {
    test("HotelCard", () => {
        const hotelCard = shallow(
            <HotelCard
                name="test hotel"
                description="test description"
                location="Austin"
                image="https://d2573qu6qrjt8c.cloudfront.net/54CF795D30354373A6991E2CEC2A8E0D/54CF795D30354373A6991E2CEC2A8E0D.JPEG"
            />
        )
        expect(hotelCard).toMatchSnapshot()
    })
})