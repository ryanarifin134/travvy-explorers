import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import Attractions from "../pages/Attractions"

configure({ adapter: new Adapter() })

describe("Test page views", () => {
	test("Attractions", () => {
		const attractionsTest = shallow(<Attractions />)
		expect(attractionsTest).toMatchSnapshot()
	})
})