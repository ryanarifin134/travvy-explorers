import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import Cities from "../pages/Cities"

configure({ adapter: new Adapter() })

describe("Test page views", () => {
	test("Cities", () => {
		const citiesTest = shallow(<Cities />)
		expect(citiesTest).toMatchSnapshot()
	})
})