import { Link } from "react-router-dom";
import { FC } from "react";
import "./SearchItem.scss";
import { City } from "../../pages/Cities/City";
import Highlighter from "react-highlight-words";
// for hover underline animation
import "../AttributeCard.scss";

interface CitySearchItemProps {
  city: City;
  query: string;
}

const CitySearchItem: FC<CitySearchItemProps> = (
  props: CitySearchItemProps
): JSX.Element => {
  return (
    <div className="search-item hover-underline-animation">
      <Link to={`/cities/${props.city.id}`}>
        <Highlighter
          searchWords={[props.query]}
          highlightClassName="highlighted"
          className="header"
          textToHighlight={props.city.name}
        />
      </Link>
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={"Country: " + props.city.country}
      />
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={"Region: " + props.city.region}
      />
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={"Number of hotels: " + props.city.num_hotels}
      />
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={"Number of attractions: " + props.city.num_attractions}
      />
    </div>
  );
};

export default CitySearchItem;
