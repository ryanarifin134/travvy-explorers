import { Link } from "react-router-dom";
import { FC } from "react";
import "./SearchItem.scss";
import { Attraction } from "../../pages/Attractions/Attraction";
import Highlighter from "react-highlight-words";
// for hover underline animation
import "../AttributeCard.scss";

interface AttractionSearchItemProps {
  attraction: Attraction;
  query: string;
}

const AttractionSearchItem: FC<AttractionSearchItemProps> = (
  props: AttractionSearchItemProps
): JSX.Element => {
  return (
    <div className="search-item hover-underline-animation">
      <Link to={`/attractions/${props.attraction.id}`}>
        <Highlighter
          searchWords={[props.query]}
          highlightClassName="highlighted"
          className="header"
          textToHighlight={props.attraction.name}
        />
      </Link>
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={"City: " + props.attraction.city.name}
      />
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={"Venue type: " + props.attraction.venue_type}
      />
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={"Rating: " + props.attraction.rating}
      />
    </div>
  );
};

export default AttractionSearchItem;
