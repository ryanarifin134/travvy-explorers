import { Link } from "react-router-dom";
import { FC } from "react";
import "./SearchItem.scss";
import { Hotel } from "../../pages/Hotels/Hotel";
import Highlighter from "react-highlight-words";
// for hover underline animation
import "../AttributeCard.scss";

interface HotelSearchItemProps {
  hotel: Hotel;
  query: string;
}

const HotelSearchItem: FC<HotelSearchItemProps> = (
  props: HotelSearchItemProps
): JSX.Element => {
  return (
    <div className="search-item hover-underline-animation">
      <Link to={`/hotels/${props.hotel.id}`}>
        <Highlighter
          searchWords={[props.query]}
          highlightClassName="highlighted"
          className="header"
          textToHighlight={props.hotel.name}
        />
      </Link>
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={"City: " + props.hotel.city}
      />
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={"Rating: " + props.hotel.rating}
      />
      <Highlighter
        searchWords={[props.query]}
        highlightClassName="highlighted"
        className="search-item-attribute"
        textToHighlight={`Average Price Per Night: ${props.hotel.price}`}
      />
    </div>
  );
};

export default HotelSearchItem;
