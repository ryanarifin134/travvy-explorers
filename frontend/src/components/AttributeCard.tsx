import { Card } from "react-bootstrap";
import "./AttributeCard.scss";

interface AttributeCardProps {
  title: string;
  text: string | number;
}

function AttributeCard(props: AttributeCardProps): JSX.Element {
  return (
    <Card
      className="hover-underline-animation"
      style={{ minWidth: "100%", height: "100%" }}
    >
      <Card.Body>
        <Card.Title>{props.title}</Card.Title>
        <Card.Text>{props.text}</Card.Text>
      </Card.Body>
    </Card>
  );
}

export default AttributeCard;
