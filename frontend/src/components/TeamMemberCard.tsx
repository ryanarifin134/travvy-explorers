import "./TeamMemberCard.scss";
import { Card } from "react-bootstrap";
import pooja from "../images/pooja.jpg";
import erika from "../images/erika.jpg";
import tristan from "../images/tristan.jpg";
import ryan from "../images/ryan.jpg";
import jaden from "../images/jaden.jpeg";
// for hover underline animation
import "./AttributeCard.scss";

interface TeamMemberCardProps {
  name: string;
  role: string;
  description: string;
  commits: number;
  issues: number;
  tests: number;
}

function TeamMemberCard(props: TeamMemberCardProps): JSX.Element {
  const getImage = (name: string): string => {
    switch (name) {
      case "Erika Tan":
        return erika;
      case "Pooja Chivukula":
        return pooja;
      case "Ryan Arifin":
        return ryan;
      case "Tristan Blake":
        return tristan;
      case "Jaden Hyde":
        return jaden;
      default:
        return "";
    }
  };

  return (
    <Card
      className="text-center hover-underline-animation"
      style={{ height: "100%" }}
    >
      <Card.Body>
        <Card.Title>{props.name}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">{props.role}</Card.Subtitle>
        <Card.Img className="mb-3" src={getImage(props.name)}></Card.Img>
        <Card.Text>{props.description}</Card.Text>
        <div className="stats">
          <div>
            Commits: {props.commits === 0 ? "loading..." : props.commits}
          </div>
          <div>Issues: {props.issues === 0 ? "loading..." : props.issues}</div>
          <div>Tests: {props.tests}</div>
        </div>
      </Card.Body>
    </Card>
  );
}

export default TeamMemberCard;
