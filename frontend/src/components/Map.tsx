import React from "react";
import { MapContainer, TileLayer, Marker } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { myIcon } from "../icons/icon.js";

interface mapProps {
  lat: number;
  lon: number;
}

const Map: React.FC<mapProps> = (props: mapProps): JSX.Element => {
  return (
    <div>
      <MapContainer
        center={[props.lat, props.lon]}
        zoom={10}
        style={{
          alignItems: "center",
          width: "100%",
          height: "400px",
        }}
      >
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        <Marker position={[props.lat, props.lon]} icon={myIcon} />
      </MapContainer>
    </div>
  );
};

export default Map;
