import * as React from "react";
import { Card } from "react-bootstrap";
import "./HotelCard.scss";
import Map from "./Map";
import { parseAmenities } from "../pages/Hotels/helper";

interface HotelCardProps {
  name: string;
  description: string;
  location: string;
  image: string;
  latitude: number;
  longitude: number;
  cityId: number;
  amenities: string[];
  rating: string;
  phone: string;
  email: string;
  distance: number;
}

interface HotelCardState {
  name: string;
  description: string;
  location: string;
  image: string;
  latitude: number;
  longitude: number;
  cityId: number;
  amenities: string[];
  rating: string;
  phone: string;
  email: string;
  distance: number;
}

export default class HotelCard extends React.Component<
  HotelCardProps,
  HotelCardState
> {
  constructor(props: HotelCardProps) {
    super(props);

    const {
      name,
      description,
      location,
      image,
      latitude,
      longitude,
      cityId,
      amenities,
      rating,
      phone,
      email,
      distance,
    } = this.props;

    this.state = {
      name: name,
      description: description,
      location: location,
      image: image,
      latitude: latitude,
      longitude: longitude,
      cityId: cityId,
      amenities: amenities,
      rating: rating,
      phone: phone,
      email: email,
      distance: distance,
    };
  }

  render(): JSX.Element {
    const {
      name,
      description,
      location,
      image,
      latitude,
      longitude,
      cityId,
      amenities,
      rating,
      phone,
      email,
      distance,
    } = this.state;
    const city_url = "/cities/" + cityId;
    const attraction_url = "/attractions";
    return (
      <div className="grid">
        <Card className="box">
          <Card.Img
            variant="top"
            src={
              image === ""
                ? "https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg"
                : image
            }
          />
        </Card>
        <Card className="box">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Link href={city_url}>{location}</Card.Link>
            <Card.Text>{description}</Card.Text>
            <Card.Text>Rating: {rating}</Card.Text>
            <Card.Text>Phone: {phone}</Card.Text>
            <Card.Text>Email: {email}</Card.Text>
            <Card.Text>Distance from Center of City: {distance} KM</Card.Text>
            <Card.Link href={attraction_url}>View Attractions</Card.Link>
          </Card.Body>
        </Card>
        <Card className="box">
          <Card.Body>
            <Card.Title>Amenities</Card.Title>
            <Card.Text>{parseAmenities(amenities)}</Card.Text>
          </Card.Body>
        </Card>
        <Card>
          <Map lat={latitude} lon={longitude} />
        </Card>
      </div>
    );
  }
}
