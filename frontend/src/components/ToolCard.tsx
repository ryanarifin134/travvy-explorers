import "./ToolCard.scss";
// for hover underline animation
import "./AttributeCard.scss";

interface ToolCardProps {
  title: string;
  img: string;
  link: string;
  description: string;
}

function ToolCard(props: ToolCardProps): JSX.Element {
  return (
    <a href={props.link} target="_blank">
      <div className="tool-card hover-underline-animation">
        <img alt={props.title} src={props.img} />
        <p>{props.title}</p>
        <span className="desc">
          <p>{props.description}</p>
        </span>
      </div>
    </a>
  );
}

export default ToolCard;
