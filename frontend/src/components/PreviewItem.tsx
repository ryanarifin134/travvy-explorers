import { Link } from "react-router-dom";
import { FC } from "react";
import "./PreviewItem.scss";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

interface PreviewItemProps {
  link: string;
  name: string;
  description: string;
}

const PreviewItem: FC<PreviewItemProps> = (
  props: PreviewItemProps
): JSX.Element => {
  return (
    <OverlayTrigger
      placement="top"
      overlay={
        <Tooltip id="tooltip">
          {props.description.substring(0, 120) + "..."}
        </Tooltip>
      }
    >
      <div className="preview-item">
        <Link to={props.link}>{props.name}</Link>
      </div>
    </OverlayTrigger>
  );
};

export default PreviewItem;
