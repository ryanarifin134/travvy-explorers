import map from "lodash/map";
import capitalize from "lodash/capitalize";

// TODO: Parsing amenities should be done server side!
export const parseAmenities = (amenitiesInput: string[]): string => {
  const amenities = map(amenitiesInput, (rawAmenity) => {
    rawAmenity = capitalize(rawAmenity);
    const amenity = rawAmenity.replaceAll("_", " ")

    return amenity
  });

  return amenities.join(", ")
}
