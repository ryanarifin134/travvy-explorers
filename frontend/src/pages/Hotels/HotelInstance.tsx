import { Link, RouteComponentProps } from "react-router-dom";
import { Hotel } from "./Hotel";
import { getRequest } from "../../utils/requestHelper";
import LoadingSpinner from "../Shared/LoadingSpinner";
import { FC, useEffect, useState } from "react";
import { Card, Container, Row, Image, Col } from "react-bootstrap";
import Map from "../../components/Map";
import { parseAmenities } from "./helper";
import AttributeCard from "../../components/AttributeCard";
import PreviewItem from "../../components/PreviewItem";
// for hover underline animation
import "../Attractions/Attractions.scss";

interface GetHotelResponse {
  hotel: Hotel;
}

interface RouterProps {
  id: string;
}

const IMAGE_NOT_FOUND_URL =
  "https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg";

const HotelInstance: FC<RouteComponentProps<RouterProps>> = (props) => {
  const [hotel, setHotel] = useState<Hotel>();

  useEffect(() => {
    getHotel();
  }, []);

  const getHotel = async (): Promise<void> => {
    const { id } = props.match.params;

    const response = await getRequest<GetHotelResponse>(`/api/hotels/${id}`);
    setHotel(response.hotel);
  };

  if (!hotel) {
    return <LoadingSpinner />;
  }

  return (
    <>
      <Container>
        <Row className="mb-3">
          <h1>{hotel.name}</h1>
        </Row>
        <Row className="mb-3">
          <p>{hotel.description}</p>
        </Row>
        <Row className="mb-3">
          <Image
            fluid
            src={hotel.image ? hotel.image : IMAGE_NOT_FOUND_URL}
            style={{
              height: "25vw",
              display: "block",
              marginLeft: "auto",
              marginRight: "auto",
            }}
          />
        </Row>
        <Row className="mb-3">
          <Col>
            <AttributeCard title="Rating" text={hotel.rating} />
          </Col>
          <Col>
            <Card
              className="hover-underline-animation"
              style={{ minWidth: "100%", height: "100%" }}
            >
              <Card.Body>
                <Card.Title>Hotel city</Card.Title>
                <Card.Text>
                  <Link
                    to={"../../Cities/" + ((hotel.city && hotel.cityId) || "8")}
                  >
                    Learn more about {hotel.city}
                  </Link>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <AttributeCard
              title="Distance from center of city"
              text={hotel.distance + " KM"}
            />
          </Col>
        </Row>
        <Row className="mb-3">
          <Col>
            <AttributeCard
              title="Amenities"
              text={parseAmenities(hotel.amenities)}
            />
          </Col>
        </Row>
        <Row className="mb-4">
          <Col>
            <AttributeCard title="Phone" text={hotel.phone} />
          </Col>
          <Col>
            <AttributeCard title="Email" text={hotel.email} />
          </Col>
        </Row>
        <Row className="mb-3 text-center">
          <Col>
            <h3>Attractions near {hotel.name}</h3>
          </Col>
        </Row>
        <Row className="mb-3 text-center">
          <Col>
            {hotel.attractions.map((attraction) => (
              <PreviewItem
                link={`../attractions/${attraction.id}`}
                name={attraction.name}
                description={attraction.description}
              />
            ))}
          </Col>
        </Row>
      </Container>
      <Map lat={hotel.latitude} lon={hotel.longitude} />
    </>
  );
};
export default HotelInstance;
