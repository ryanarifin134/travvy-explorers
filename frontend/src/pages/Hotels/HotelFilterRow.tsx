/* eslint-disable  @typescript-eslint/no-explicit-any */

import React from "react";
import Select from "react-select";
import { Container, Col, Row } from "react-bootstrap";
import ModelSearchBar from "../../components/ModelSearchBar";

interface filterProps {
  onSearchChange: (event: any) => void;
  onSearchSubmit: () => void;
  filterRating: (chosen: string | undefined) => void;
  filterPrice: (chosen: string | undefined) => void;
  filterName: (chosen: string | undefined) => void;
  filterCity: (chosen: string | undefined) => void;
  filterDistance: (chosen: string | undefined) => void;
  filterAmenities: (chosen: string | undefined) => void;
  sortBy: (chosen: string | undefined) => void;
  ratingOptions: any;
  priceOptions: any;
  cityOptions: any;
  distanceOptions: any;
  amenitiesOptions: any;
  sortByOptions: any;
}

const HotelFilterRow: React.FC<filterProps> = (props: filterProps) => {
  return (
    <Container fluid>
      <Row className="mb-2 mt-2 justify-content-md-center">
        <Col md={2}>
          <ModelSearchBar
            model="hotels"
            onSearchChange={props.onSearchChange}
            onSearchSubmit={props.onSearchSubmit}
          />
        </Col>
        <Col className="pl-0" md={1}>
          <Select
            onChange={(x: any) => props.filterCity(x)}
            placeholder="City"
            options={props.cityOptions}
            isClearable
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.filterRating(x)}
            placeholder="Rating"
            options={props.ratingOptions}
            isClearable
          />
        </Col>
        <Col className="pl-0" md={2}>
          <Select
            onChange={(x: any) => props.filterPrice(x)}
            placeholder="Price"
            options={props.priceOptions}
            isClearable
          />
        </Col>
        <Col className="pl-0" md={2}>
          <Select
            onChange={(x: any) => props.filterDistance(x)}
            placeholder="Distance"
            options={props.distanceOptions}
            isClearable
          />
        </Col>
        <Col className="pl-0" md={2}>
          <Select
            onChange={(x: any) => props.filterAmenities(x)}
            placeholder="Amenities"
            options={props.amenitiesOptions}
            isClearable
          />
        </Col>
        <Col className="pl-0" md={1}>
          <Select
            onChange={(x: any) => props.sortBy(x)}
            placeholder="Sort"
            options={props.sortByOptions}
            isClearable
          />
        </Col>
      </Row>
    </Container>
  );
};

export default HotelFilterRow;
