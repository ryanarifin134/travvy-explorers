import {Attraction} from '../Attractions/Attraction';

export interface Hotel {
    id: number,
    name: string,
    city: string,
    description: string,
    latitude: number,
    longitude: number,
    distance: number,
    distance_unit: string,
    amenities: string[],
    image: string,
    rating: string,
    phone: string,
    email: string,
    price: number,
    hotelId: string,
    cityId: number,
    attractions: Attraction[]
}