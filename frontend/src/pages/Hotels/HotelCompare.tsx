// import { Container, Image, Row, Col } from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
// import Map from "../../components/Map";
import "leaflet/dist/leaflet.css";
import { RouteComponentProps } from "react-router-dom";
import { FC, useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { HotelComparison } from "./HotelComparison";
import { getRequest } from "../../utils/requestHelper";
// import AttributeCard from "../../components/AttributeCard";
// import PreviewItem from "../../components/PreviewItem";
// import {formatNumber} from "../../utils/formatHelper";
import LoadingSpinner from "../Shared/LoadingSpinner";
import "./HotelComparison.scss";

interface RouterProps {
  hotelid1: string;
  hotelid2: string;
  hotelid3: string;
  hotelid4: string;
}

const HotelCompare: FC<RouteComponentProps<RouterProps>> = (props) => {
  const [hotelComparison, setHotelComparison] = useState<HotelComparison>();

  let path = "";
  if (props.match.params.hotelid1) {
    path += "/" + props.match.params.hotelid1;
  }
  if (props.match.params.hotelid2) {
    path += "/" + props.match.params.hotelid2;
  }
  if (props.match.params.hotelid3) {
    path += "/" + props.match.params.hotelid3;
  }
  if (props.match.params.hotelid4) {
    path += "/" + props.match.params.hotelid4;
  }

  useEffect(() => {
    getComparison();
  }, []);

  const getComparison = async (): Promise<void> => {
    const hotelComparison = await getRequest<HotelComparison>(
      `/api/hotels/compare${path}`
    );
    setHotelComparison(hotelComparison);
  };

  if (!hotelComparison) {
    return <LoadingSpinner />;
  }

  return (
    <Container className="city-model" fluid>
      <Row className="justify-content-center">
        <Col>
          <h1>Hotel Comparison</h1>
        </Col>
      </Row>
      <div className="hotel-comparison">
        {hotelComparison.hotels.map((hotel) => (
          <ListGroup>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Name:</span>
                <span>{hotel.name}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>City:</span>
                <span>{hotel.city}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>City Distance:</span>
                <span>
                  (Rank: {hotel.rankings.distance}) {hotel.distance}{" "}
                  {hotel.distance_unit}
                </span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Rating:</span>
                <span>
                  (Rank: {hotel.rankings.rating}) {hotel.rating}
                </span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Price:</span>
                <span>
                  (Rank: {hotel.rankings.price}) {hotel.price}
                </span>
              </div>
            </ListGroup.Item>
          </ListGroup>
        ))}
      </div>
    </Container>
  );
};

export default HotelCompare;
