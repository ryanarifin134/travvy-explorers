/* eslint-disable  @typescript-eslint/no-explicit-any */

import { FC, useEffect, useState } from "react";
import { Hotel } from "./Hotel";
import { Link, useHistory } from "react-router-dom";
import {
  Col,
  Container,
  Row,
  Table,
  Button,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import { getRequest } from "../../utils/requestHelper";
import Paginator, { INITIAL_PAGE_NUM } from "../Shared/Paginator";
import HotelFilterRow from "./HotelFilterRow";
import * as values from "../../data/hotelOptions";
import "./Hotel.scss";
import LoadingSpinner from "../Shared/LoadingSpinner";
import { parseAmenities } from "./helper";
import SearchableText from "../Shared/SearchableText";

export interface GetHotelsResponse {
  hotels: Hotel[];
  totalCount: number;
}

const columns = [
  "Name",
  "City",
  "Distance",
  "Price per Night",
  "Rating",
  "Amenities",
];

const NUM_AMMENITIES_TO_SHOW = 4;

const Hotels: FC = (): JSX.Element => {
  const [hotels, setHotels] = useState<Hotel[]>();
  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    getHotels(INITIAL_PAGE_NUM);
  }, []);

  const [currentPage, setCurrentPage] = useState(INITIAL_PAGE_NUM);

  const [filterRatingState, setFilterRatingState] = useState("");
  const [filterPriceState, setFilterPriceState] = useState("");
  const [filterNameState, setFilterNameState] = useState("");
  const [filterDistanceState, setFilterDistanceState] = useState("");
  const [filterCityState, setFilterCityState] = useState("");
  const [sortState, setSortState] = useState("");
  const [ascending, setAscending] = useState("");
  const [filterAmenitiesState, setFilterAmenitiesState] = useState("");
  const [currentQuery, setCurrentQuery] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [selectMode, setSelectMode] = useState(false);
  const [selectButtonDisabled, setSelectButtonDisabled] = useState(false);
  const [selectedHotels, setSelectedHotels] = useState([]);

  const history = useHistory();

  useEffect(() => {
    setCurrentPage(INITIAL_PAGE_NUM); // TODO: Better way to do this?
    getHotels(INITIAL_PAGE_NUM);
  }, [
    filterRatingState,
    filterPriceState,
    filterNameState,
    filterDistanceState,
    filterCityState,
    filterAmenitiesState,
    sortState,
    ascending,
  ]);

  const getHotels = async (page: number): Promise<void> => {
    setHotels(undefined);

    const queryParams = queryParam();
    const output =
      queryParams === "?" ? "?page=" + page : queryParams + "&page=" + page;

    const response = await getRequest<GetHotelsResponse>(
      "/api/hotels" + output,
      page
    );

    setHotels(response.hotels);
    setTotalCount(response.totalCount);
  };

  const queryParam = (): string => {
    let output = "?";
    if (filterAmenitiesState !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "amenities=" + filterAmenitiesState;
    }
    if (filterRatingState !== "") {
      console.log("in rating if: " + filterRatingState);
      if (output !== "?") {
        output += "&";
      }
      output += "rating=" + filterRatingState;
    }
    if (filterDistanceState !== "") {
      console.log("in distance if: " + filterDistanceState);
      if (output !== "?") {
        output += "&";
      }
      output += "distance=" + filterDistanceState;
    }
    if (filterCityState !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "city=" + encodeURIComponent(filterCityState);
    }
    if (filterNameState !== "") {
      console.log("in name if: " + encodeURIComponent(filterNameState));
      if (output !== "?") {
        output += "&";
      }
      output += "name=" + encodeURIComponent(filterNameState);
    }
    if (filterPriceState !== "") {
      console.log("in price if: " + filterPriceState);
      if (output !== "?") {
        output += "&";
      }
      if (filterPriceState === "50") {
        output += "price_max=" + filterPriceState;
      } else if (filterPriceState === "100") {
        output += "price_min=50&price_max=" + filterPriceState;
      } else if (filterPriceState === "150") {
        output += "price_min=100&price_max=" + filterPriceState;
      } else if (filterPriceState === "200") {
        output += "price_min=150&price_max=" + filterPriceState;
      } else {
        output += "price_min=" + filterPriceState;
      }
    }
    if (sortState !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "sortby=" + sortState + "&ascending=" + ascending;
    }

    if (currentQuery !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "query=" + currentQuery;
    }

    return output;
  };

  const filterAmenities = (amenities: any): void => {
    const value = amenities === null ? "" : amenities["value"];
    setFilterAmenitiesState(value);
  };

  const filterRating = (rating: any): void => {
    const value = rating === null ? "" : rating["value"];
    setFilterRatingState(value);
    // getHotels(INITIAL_PAGE_NUM)
  };

  const filterName = (name: any): void => {
    const value = name === null ? "" : name["value"];
    setFilterNameState(value);
    // getHotels(INITIAL_PAGE_NUM)
  };

  const filterCity = (city: any): void => {
    const value = city === null ? "" : city["value"];
    setFilterCityState(value);
    // getHotels(INITIAL_PAGE_NUM)
  };

  const filterDistance = (distance: any): void => {
    const value = distance === null ? "" : distance["value"];
    setFilterDistanceState(value);
    // getHotels(INITIAL_PAGE_NUM)
  };

  const filterPrice = (price: any): void => {
    const value = price === null ? "" : price["value"];
    setFilterPriceState(value);
    // getHotels(INITIAL_PAGE_NUM)
  };

  const sortBy = (val: any): void => {
    const sort = val === null ? "" : val["value"];
    let sort_val = sort;
    if (sort.includes("name")) {
      sort_val = "name";
    } else if (sort.includes("price")) {
      sort_val = "price";
    } else if (sort.includes("distance")) {
      sort_val = "distance";
    } else if (sort.includes("rating")) {
      sort_val = "rating";
    }
    let ascending = "true";
    if (sort.includes("descending")) {
      ascending = "false";
    }
    setSortState(sort_val);
    setAscending(ascending);
    // getHotels(INITIAL_PAGE_NUM)
  };

  const onSearchChange = (event) => {
    setCurrentQuery(event.target.value);
  };

  const onSearchSubmit = (): void => {
    setCurrentPage(INITIAL_PAGE_NUM);
    getHotels(INITIAL_PAGE_NUM);
    setSearchQuery(currentQuery);
  };

  const onPageChange = (page: number): void => {
    setCurrentPage(page);
    getHotels(page);
  };

  const onSelectButtonClick = (): void => {
    if (!selectMode) {
      setSelectButtonDisabled(!selectButtonDisabled);
      setSelectMode(true);
    } else {
      let path = "hotels/compare";
      selectedHotels.forEach((hotelId) => {
        path += "/" + hotelId;
      });
      history.push(path);
    }
  };

  const popover = (
    <Popover id="popover-basic">
      <Popover.Title as="h3">Select Hotels</Popover.Title>
      <Popover.Content>
        Select up to <strong>4</strong> hotels, then compare!
      </Popover.Content>
    </Popover>
  );

  const SelectButton = () => (
    <OverlayTrigger trigger="click" placement="left" overlay={popover}>
      <Button
        disabled={selectButtonDisabled}
        onClick={onSelectButtonClick}
        className="select-button"
        variant="primary"
      >
        {selectMode ? "Compare!" : "Select"}
      </Button>
    </OverlayTrigger>
  );

  return (
    <div className="hotel-model">
      <Container fluid>
        <Row>
          <Col>
            <h1>Hotels</h1>
            <SelectButton />
          </Col>
        </Row>
        <HotelFilterRow
          onSearchChange={onSearchChange}
          onSearchSubmit={onSearchSubmit}
          filterRating={filterRating}
          filterPrice={filterPrice}
          filterName={filterName}
          filterCity={filterCity}
          filterDistance={filterDistance}
          filterAmenities={filterAmenities}
          sortBy={sortBy}
          ratingOptions={values.rating_options}
          priceOptions={values.price_options}
          cityOptions={values.city_options}
          distanceOptions={values.distance_options}
          amenitiesOptions={values.amenities_options}
          sortByOptions={values.sorting_options}
        />
        <Table className="mt-4 mb-1" hover>
          <thead key="table head">
            <tr>
              {columns.map((column) => (
                <th key={"col_" + column}>{column}</th>
              ))}
            </tr>
          </thead>
          {renderTableBody(
            hotels,
            searchQuery,
            selectedHotels,
            setSelectedHotels,
            selectMode,
            setSelectButtonDisabled
          )}
        </Table>
        {hotels ? (
          <Paginator
            totalCount={totalCount}
            onPageChange={onPageChange}
            currentPage={currentPage}
          />
        ) : (
          <LoadingSpinner />
        )}
      </Container>
    </div>
  );
};

const renderTableBody = (
  hotels: Hotel[] | undefined,
  searchQuery: string,
  selectedHotels: any,
  setSelectedHotels: any,
  selectMode: any,
  setSelectButtonDisabled: any
) => {
  const onRowClick = (e) => {
    if (!selectMode) {
      return;
    }

    if (e.target.parentElement.classList.contains("hovered")) {
      const url = e.target.parentElement.children[0].children[0].href;
      const lastIndex = url.lastIndexOf("/");
      const id = url.substring(lastIndex + 1);
      const index = selectedHotels.indexOf(id);
      if (index > -1) {
        selectedHotels.splice(index, 1);
        setSelectedHotels(selectedHotels);
      }
      e.target.parentElement.classList.remove("hovered");
      if (selectedHotels.length === 1) {
        setSelectButtonDisabled(true);
      }
      return;
    }

    if (selectedHotels.length === 4) {
      return;
    }

    e.target.parentElement.classList.add("hovered");
    const url = e.target.parentElement.children[0].children[0].href;
    const lastIndex = url.lastIndexOf("/");
    const id = url.substring(lastIndex + 1);
    console.log("adding " + id);
    selectedHotels.push(id);
    console.log(selectedHotels);
    setSelectedHotels(selectedHotels);

    if (selectedHotels.length === 2) {
      setSelectButtonDisabled(false);
    }

    console.log("here");
  };

  if (!hotels) {
    return;
  }

  return (
    <tbody>
      {hotels.map((hotel) => (
        <tr
          onClick={onRowClick}
          className={selectedHotels.includes("" + hotel.id) ? "hovered" : ""}
        >
          <td>
            <Link to={`/hotels/${hotel.id}`}>
              <SearchableText
                searchQuery={searchQuery}
                textToSearch={hotel.name}
              />
            </Link>
          </td>
          <td>
            <SearchableText
              searchQuery={searchQuery}
              textToSearch={hotel.city}
            />
          </td>
          <td>
            <SearchableText
              searchQuery={searchQuery}
              textToSearch={hotel.distance ? `${hotel.distance}` : "Unknown"}
            />
          </td>
          <td>
            <SearchableText
              searchQuery={searchQuery}
              textToSearch={hotel.price ? `$${hotel.price}` : "Unknown"}
            />
          </td>
          <td>
            <SearchableText
              searchQuery={searchQuery}
              textToSearch={hotel.rating ? `${hotel.rating}` : "Unknown"}
            />
          </td>
          <td>
            <SearchableText
              searchQuery={searchQuery}
              textToSearch={
                parseAmenities(
                  hotel.amenities.slice(0, NUM_AMMENITIES_TO_SHOW)
                ) + "..."
              }
            />
          </td>
        </tr>
      ))}
    </tbody>
  );
};

export default Hotels;
