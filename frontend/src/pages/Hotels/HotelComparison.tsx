export interface HotelComparison {
  hotels: Hotel[];
}

interface Hotel {
  id: number;
  name: string;
  city: string;
  description: string;
  latitude: number;
  longitude: number;
  distance: number;
  distance_unit: string;
  amenities: string[];
  image: string;
  rating: string;
  phone: string;
  email: string;
  price: number;
  hotelId: string;
  cityId: number;
  rankings: Ranking;
}

interface Ranking {
  distance: number;
  rating: number;
  price: number;
}
