/* eslint-disable  @typescript-eslint/no-explicit-any */
import { useHistory } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Button,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import "../Cities/Cities.scss";
import { FC, useEffect, useState } from "react";
import { Attraction } from "./Attraction";
import { getRequest } from "../../utils/requestHelper";
import AttractionFilterRow from "./AttractionFilterRow";
import * as values from "../../data/attractionOptions";
import { INITIAL_PAGE_NUM } from "../Shared/Paginator";
import AttractionGrid, { AttractionGridProps } from "./AttractionGrid";

export interface GetAttractionsResponse {
  attractions: Attraction[];
  totalCount: number;
}

const Attractions: FC = (): JSX.Element => {
  const [attractions, setAttractions] = useState<Attraction[]>();
  const [totalCount, setTotalCount] = useState(0);

  const [currentPage, setCurrentPage] = useState(INITIAL_PAGE_NUM);

  const [filterRatingState, setFilterRatingState] = useState("");
  const [filterCityState, setFilterCityState] = useState("");
  const [filterIndoorState, setFilterIndoorState] = useState("");
  const [filterCategoryState, setFilterCategoryState] = useState("");
  const [filterDistanceState, setFilterDistanceState] = useState("");
  const [sortState, setSortState] = useState("");
  const [currentQuery, setCurrentQuery] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [selectMode, setSelectMode] = useState(false);
  const [selectButtonDisabled, setSelectButtonDisabled] = useState(false);
  const [selectedAttractions, setSelectedAttractions] = useState<string[]>([]);

  const history = useHistory();

  useEffect(() => {
    setCurrentPage(INITIAL_PAGE_NUM); // TODO: Better way to do this
    getAttractions(INITIAL_PAGE_NUM);
  }, [
    filterRatingState,
    filterCityState,
    filterIndoorState,
    filterCategoryState,
    filterDistanceState,
    sortState,
  ]);

  const getAttractions = async (page: number): Promise<void> => {
    setAttractions(undefined);
    //insert helper method to take care of query parameters
    const output = queryParam() + "&page=" + page;
    const response = await getRequest<GetAttractionsResponse>(
      "/api/attractions" + output,
      page
    );

    setTotalCount(response.totalCount);
    setAttractions(response.attractions);
  };

  const queryParam = (): string => {
    let output = "?";
    if (filterIndoorState !== "") {
      output += "indoor=" + filterIndoorState;
    }
    if (filterRatingState !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "AttractionRating=" + filterRatingState;
    }
    if (filterDistanceState !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "AttractionDistance=" + filterDistanceState;
    }
    if (filterCityState !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "AttractionCity=" + filterCityState;
    }
    if (filterCategoryState !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "AttractionCategory=" + filterCategoryState;
    }
    if (sortState !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "sortby=" + sortState;
    }
    if (currentQuery !== "") {
      if (output !== "?") {
        output += "&";
      }
      output += "query=" + currentQuery;
    }

    return output;
  };

  const filterIndoor = (
    chosen: any /* DON"T KNOW WHAT THE TYPE IS */
  ): void => {
    if (chosen === null) {
      setFilterIndoorState("");
    } else if (chosen["label"] === "Indoor") {
      setFilterIndoorState("true");
    } else {
      setFilterIndoorState("false");
    }
    // getAttractions(INITIAL_PAGE_NUM)
  };

  const filterRating = (
    rating: any /* DON"T KNOW WHAT THE TYPE IS */
  ): void => {
    if (rating === null) {
      setFilterRatingState("");
    } else {
      setFilterRatingState(rating["value"]);
    }

    // getAttractions(INITIAL_PAGE_NUM)
  };

  const filterCategory = (
    categories: any /* DON"T KNOW WHAT THE TYPE IS */
  ): void => {
    if (categories === null) {
      setFilterCategoryState("");
    } else {
      setFilterCategoryState(categories["value"]);
    }

    // getAttractions(INITIAL_PAGE_NUM)
  };

  const filterCity = (cities: any /* DON"T KNOW WHAT THE TYPE IS */): void => {
    if (cities === null) {
      setFilterCityState("");
    } else {
      setFilterCityState(cities["value"]);
    }

    // getAttractions(INITIAL_PAGE_NUM)
  };

  const filterDistance = (
    distance: any /* DON"T KNOW WHAT THE TYPE IS */
  ): void => {
    if (distance === null) {
      setFilterDistanceState("");
    } else {
      setFilterDistanceState(distance["value"]);
    }

    // getAttractions(INITIAL_PAGE_NUM)
  };

  const sortby = (val: any /* DON"T KNOW WHAT THE TYPE IS */): void => {
    // if (val === null || val['label'] === '---'){
    if (val === null) {
      setSortState("");
    } else {
      setSortState(val["value"]);
    }
    // getAttractions(INITIAL_PAGE_NUM)
  };

  const onSearchChange = (event): void => {
    setCurrentQuery(event.target.value);
  };

  const onSearchSubmit = (): void => {
    setCurrentPage(INITIAL_PAGE_NUM);
    getAttractions(INITIAL_PAGE_NUM);
    setSearchQuery(currentQuery);
  };

  const onPageChange = (page: number): void => {
    setCurrentPage(page);
    getAttractions(page);
  };

  const onSelectButtonClick = (): void => {
    if (!selectMode) {
      setSelectButtonDisabled(!selectButtonDisabled);
      setSelectMode(true);
    } else {
      let path = "attractions/compare";
      selectedAttractions.forEach((attractionId) => {
        path += "/" + attractionId;
      });
      history.push(path);
    }
  };

  const SelectButton = () => (
    <OverlayTrigger trigger="click" placement="left" overlay={popover}>
      <Button
        disabled={selectButtonDisabled}
        onClick={onSelectButtonClick}
        className="select-button"
        variant="primary"
      >
        {selectMode ? "Compare!" : "Select"}
      </Button>
    </OverlayTrigger>
  );

  const popover = (
    <Popover id="popover-basic">
      <Popover.Title as="h3">Select Attractions</Popover.Title>
      <Popover.Content>
        Select up to <strong>4</strong> attractions, then compare!
      </Popover.Content>
    </Popover>
  );

  const attractionGridProps: AttractionGridProps = {
    attractions,
    totalCount,
    currentPage,
    searchQuery,
    onPageChange,
    selectedAttractions,
    setSelectedAttractions,
    selectMode,
    setSelectButtonDisabled,
  };

  return (
    <div className="city-model">
      <Container fluid>
        <Row className="justify-content-center">
          <Col>
            <h1>Attractions</h1>
            <SelectButton />
          </Col>
        </Row>
        <AttractionFilterRow
          onSearchChange={onSearchChange}
          onSearchSubmit={onSearchSubmit}
          filterIndoor={filterIndoor}
          filterRating={filterRating}
          filterCategory={filterCategory}
          filterCity={filterCity}
          filterDistance={filterDistance}
          sortby={sortby}
          indoorOptions={values.indoorFilter}
          ratingOptions={values.ratingFilter}
          categoryOptions={values.categoryFilter}
          cityOptions={values.cityFilter}
          distanceOptions={values.distanceFilter}
          sortbyOptions={values.sortbyOptions}
        ></AttractionFilterRow>
        <AttractionGrid {...attractionGridProps} />
      </Container>
    </div>
  );
};

export default Attractions;
