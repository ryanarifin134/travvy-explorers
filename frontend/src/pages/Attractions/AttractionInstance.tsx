import "./Attractions.scss";
import { Link, RouteComponentProps } from "react-router-dom";
import Map from "../../components/Map";
import { FC, useEffect, useState } from "react";
import { Attraction } from "./Attraction";
import { getRequest } from "../../utils/requestHelper";
import { Card, Container, Row, Col, Image } from "react-bootstrap";
import { parseAmenities } from "../Hotels/helper";
import AttributeCard from "../../components/AttributeCard";
import LoadingSpinner from "../Shared/LoadingSpinner";

interface RouterProps {
  attractionid: string;
}

const AttractionInstance: FC<RouteComponentProps<RouterProps>> = (
  props
): JSX.Element => {
  const [attraction, setAttraction] = useState<Attraction>();

  useEffect(() => {
    getAttraction();
  }, []);

  const getAttraction = async () => {
    const attraction = await getRequest<Attraction>(
      `/api/attractions/${props.match.params.attractionid}`
    );
    setAttraction(attraction);
  };

  if (!attraction) {
    return <LoadingSpinner />;
  }

  return (
    <>
      <Container>
        <Row className="mb-3">
          <h1>{attraction.name}</h1>
        </Row>
        <Row className="mb-3">
          <p>{attraction.description}</p>
        </Row>
        <Row className="mb-3">
          <Image
            fluid
            src={attraction.imageUrl}
            style={{
              height: "25vw",
              display: "block",
              marginLeft: "auto",
              marginRight: "auto",
            }}
          />
        </Row>

        <Row className="mb-3">
          <Col>
            <AttributeCard title="Rating" text={attraction.rating} />
          </Col>
          <Col>
            <AttributeCard title="Address" text={attraction.address} />
          </Col>
          <Col>
            <AttributeCard
              title="Number of hotels nearby"
              text={attraction.hotels_in_city}
            />
          </Col>
        </Row>
        <Row className="mb-3">
          <Col>
            <AttributeCard
              title="Attraction categories"
              text={parseAmenities(attraction.category)}
            />
          </Col>
        </Row>
        <Row className="mb-3">
          <Col>
            <Card
              className="hover-underline-animation"
              style={{ minWidth: "100%", height: "100%" }}
            >
              <Card.Body>
                <Card.Title>Wikipedia article</Card.Title>
                <Card.Text>
                  <a target="_blank" href={attraction.wikiUrl}>
                    Learn more about {attraction.name}{" "}
                  </a>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="hover-underline-animation"
              style={{ minWidth: "100%", height: "100%" }}
            >
              <Card.Body>
                <Card.Title>Attraction city</Card.Title>
                <Card.Text>
                  <Link
                    to={
                      "../../Cities/" +
                      ((attraction.city && attraction.city.id) || "8")
                    }
                  >
                    Learn more about {attraction.city.name}
                  </Link>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="hover-underline-animation"
              style={{ minWidth: "100%", height: "100%" }}
            >
              <Card.Body>
                <Card.Title>Nearest hotel to {attraction.name}</Card.Title>
                <Card.Text>
                  <Link to={`/hotels/${attraction.hotel.id}`}>
                    Learn more about {attraction.hotel.name}
                  </Link>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
      <Map lat={attraction.lat} lon={attraction.lon} />
    </>
  );
};

export default AttractionInstance;
