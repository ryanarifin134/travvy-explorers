/* eslint-disable  @typescript-eslint/no-explicit-any */
import React from "react";
import Select from "react-select";
import { Col, Row, Container } from "react-bootstrap";
import ModelSearchBar from "../../components/ModelSearchBar";

interface AttractionFilterRowProps {
  onSearchChange: (event: any) => void;
  onSearchSubmit: () => void;
  filterIndoor: (chosen: any) => void;
  filterRating: (chosen: any) => void;
  filterCategory: (chosen: any) => void;
  filterCity: (chosen: any) => void;
  filterDistance: (chosen: any) => void;
  sortby: (chosen: any) => void;
  indoorOptions: any;
  ratingOptions: any;
  categoryOptions: any;
  cityOptions: any;
  distanceOptions: any;
  sortbyOptions: any;
}

const AttractionFilterRow: React.FC<AttractionFilterRowProps> = (props) => {
  return (
    <Container fluid>
      <Row className="mb-2 mt-2 justify-content-md-center">
        <Col md={2}>
          <ModelSearchBar
            model="attractions"
            onSearchChange={props.onSearchChange}
            onSearchSubmit={props.onSearchSubmit}
          />
        </Col>
        <Col md={1}>
          <Select
            onChange={(x: any) => props.filterCity(x)}
            placeholder="City"
            options={props.cityOptions}
            isClearable
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.filterCategory(x)}
            placeholder="Category"
            options={props.categoryOptions}
            isClearable
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.filterRating(x)}
            placeholder="Rating"
            options={props.ratingOptions}
            isClearable
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.filterIndoor(x)}
            placeholder="Venue"
            options={props.indoorOptions}
            isClearable
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.filterDistance(x)}
            placeholder="Distance (miles)"
            options={props.distanceOptions}
            isClearable
          />
        </Col>
        <Col md={1}>
          <Select
            onChange={(x: any) => props.sortby(x)}
            placeholder="Sort"
            options={props.sortbyOptions}
            isClearable
          />
        </Col>
      </Row>
    </Container>
  );
};

export default AttractionFilterRow;
