// import { Container, Image, Row, Col } from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
// import Map from "../../components/Map";
import "leaflet/dist/leaflet.css";
import { RouteComponentProps } from "react-router-dom";
import { Col, Container, Row } from "react-bootstrap";
import { FC, useEffect, useState } from "react";
import { AttractionComparison } from "./AttractionComparison";
import { getRequest } from "../../utils/requestHelper";
// import AttributeCard from "../../components/AttributeCard";
// import PreviewItem from "../../components/PreviewItem";
// import {formatNumber} from "../../utils/formatHelper";
import LoadingSpinner from "../Shared/LoadingSpinner";
import "./AttractionComparison.scss";

interface RouterProps {
  attractionid1: string;
  attractionid2: string;
  attractionid3: string;
  attractionid4: string;
}

const AttractionCompare: FC<RouteComponentProps<RouterProps>> = (props) => {
  const [
    attractionComparison,
    setAttractionComparison,
  ] = useState<AttractionComparison>();

  let path = "";
  if (props.match.params.attractionid1) {
    path += "/" + props.match.params.attractionid1;
  }
  if (props.match.params.attractionid2) {
    path += "/" + props.match.params.attractionid2;
  }
  if (props.match.params.attractionid3) {
    path += "/" + props.match.params.attractionid3;
  }
  if (props.match.params.attractionid4) {
    path += "/" + props.match.params.attractionid4;
  }

  useEffect(() => {
    getComparison();
  }, []);

  const getComparison = async (): Promise<void> => {
    const attractionComparison = await getRequest<AttractionComparison>(
      `/api/attractions/compare${path}`
    );
    setAttractionComparison(attractionComparison);
  };

  if (!attractionComparison) {
    return <LoadingSpinner />;
  }

  return (
    <Container className="city-model" fluid>
      <Row className="justify-content-center">
        <Col>
          <h1>Attraction Comparison</h1>
        </Col>
      </Row>
      <div className="attraction-comparison">
        {attractionComparison.attractions.map((attraction) => (
          <ListGroup>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Name:</span>
                <span>{attraction.name}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>City:</span>
                <span>{attraction.city_name}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>City Distance:</span>
                <span>
                  (Rank: {attraction.rankings.city_distance}){" "}
                  {attraction.city_distance}
                </span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Venue Type:</span>
                <span>{attraction.venue_type}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Rating:</span>
                <span>
                  (Rank: {attraction.rankings.rating}) {attraction.rating}
                </span>
              </div>
            </ListGroup.Item>
          </ListGroup>
        ))}
      </div>
    </Container>
  );
};

export default AttractionCompare;
