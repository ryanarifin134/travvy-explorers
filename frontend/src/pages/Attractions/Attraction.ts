import {City} from "../Cities/City";
import {Hotel} from '../Hotels/Hotel';

export interface Attraction {
    id: number;
    name: string;
    city_name: string;
    city: City;
    city_distance: number;
    venue_type: string;
    rating: number;
    lat: number;
    lon: number;
    category: string[];
    description: string;
    address: string;
    imageUrl: string;
    wikiUrl: string;
    hotels_in_city: number;
    hotel: Hotel;
}