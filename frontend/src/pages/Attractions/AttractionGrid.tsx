import { Attraction } from "./Attraction";
import { FC } from "react";
import LoadingSpinner from "../Shared/LoadingSpinner";
import chunk from "lodash/chunk";
import AttractionGridRow from "./AttractionGridRow";
import Paginator from "../Shared/Paginator";

export interface AttractionGridProps {
  attractions: Attraction[] | undefined;
  totalCount: number;
  currentPage: number;
  searchQuery: string;
  onPageChange: (page: number) => void;
  selectedAttractions: string[];
  setSelectedAttractions: (attractions: string[]) => void;
  selectMode: boolean;
  setSelectButtonDisabled: (disabled: boolean) => void;
}

const NUM_ATTRACTIONS_PER_PAGE = 9;
const NUM_ATTRACTIONS_PER_ROW = 3;

const AttractionGrid: FC<AttractionGridProps> = (props) => {
  const {
    attractions,
    totalCount,
    currentPage,
    searchQuery,
    onPageChange,
    selectedAttractions,
    setSelectedAttractions,
    selectMode,
    setSelectButtonDisabled,
  } = props;

  if (!attractions) {
    return <LoadingSpinner />;
  }

  return (
    <>
      {chunk(attractions, NUM_ATTRACTIONS_PER_ROW).map((attractionsInRow) => (
        <AttractionGridRow
          attractions={attractionsInRow}
          searchQuery={searchQuery}
          selectedAttractions={selectedAttractions}
          setSelectedAttractions={setSelectedAttractions}
          selectMode={selectMode}
          setSelectButtonDisabled={setSelectButtonDisabled}
        />
      ))}
      <Paginator
        entitiesPerPage={NUM_ATTRACTIONS_PER_PAGE}
        totalCount={totalCount}
        onPageChange={onPageChange}
        currentPage={currentPage}
      />
    </>
  );
};

export default AttractionGrid;
