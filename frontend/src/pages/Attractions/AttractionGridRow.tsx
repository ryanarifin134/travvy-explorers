import { Attraction } from "./Attraction";
import { FC } from "react";
import { Card, CardDeck, Col, Row } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import replace from "lodash/replace";
import SearchableText from "../Shared/SearchableText";
// for the hover underline animation
import "../../components/AttributeCard.scss";

interface AttractionGridRowProps {
  searchQuery: string;
  attractions: Attraction[];
  selectedAttractions: string[];
  setSelectedAttractions: (attractions: string[]) => void;
  selectMode: boolean;
  setSelectButtonDisabled: (disabled: boolean) => void;
}

const NUM_DECIMALS_IN_DISTANCE = 1;

const AttractionGridRow: FC<AttractionGridRowProps> = (props): JSX.Element => {
  const {
    searchQuery,
    attractions,
    selectedAttractions,
    setSelectedAttractions,
    selectMode,
    setSelectButtonDisabled,
  } = props;

  const onCardClick = (e) => {
    if (!selectMode) {
      return;
    }

    e.preventDefault();

    let currentElement = e.target;
    while (!currentElement.getAttribute("href")) {
      currentElement = currentElement.parentElement;
    }

    if (currentElement.classList.contains("hovered")) {
      const url = currentElement.getAttribute("href");
      const lastIndex = url.lastIndexOf("/");
      const id = url.substring(lastIndex + 1);
      const index = selectedAttractions.indexOf(id);
      if (index > -1) {
        selectedAttractions.splice(index, 1);
        setSelectedAttractions(selectedAttractions);
      }
      currentElement.classList.remove("hovered");
      if (selectedAttractions.length === 1) {
        setSelectButtonDisabled(true);
      }
      return;
    }

    if (selectedAttractions.length === 4) {
      return;
    }

    currentElement.classList.add("hovered");
    const url = currentElement.getAttribute("href");
    const lastIndex = url.lastIndexOf("/");
    const id = url.substring(lastIndex + 1);
    console.log("adding " + id);
    selectedAttractions.push(id);
    console.log(selectedAttractions);
    setSelectedAttractions(selectedAttractions);

    if (selectedAttractions.length === 2) {
      setSelectButtonDisabled(false);
    }

    console.log("here");
  };

  return (
    <Row>
      <Col>
        <CardDeck className="mb-3">
          {attractions.map((attraction) => (
            <LinkContainer to={`/attractions/${attraction.id}`}>
              <Card
                onClick={onCardClick}
                className={
                  "attraction-card hover-underline-animation" +
                  (selectedAttractions.includes("" + attraction.id)
                    ? " hovered"
                    : "")
                }
              >
                <Card.Body className="pb-3">
                  <Card.Title>
                    <SearchableText
                      searchQuery={searchQuery}
                      textToSearch={attraction.name}
                    />
                  </Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">
                    <SearchableText
                      searchQuery={searchQuery}
                      textToSearch={attraction.city.name}
                    />
                  </Card.Subtitle>
                  {renderAttractionCardText(attraction, searchQuery)}
                </Card.Body>
              </Card>
            </LinkContainer>
          ))}
        </CardDeck>
      </Col>
    </Row>
  );
};

const renderAttractionCardText = (
  attraction: Attraction,
  searchQuery: string
) => {
  const distanceInKilometers = attraction.city_distance / 1000;
  const categoryText = parseAttractionCategories(attraction.category);

  const attractionCardText = `${attraction.name} is an ${
    attraction.venue_type
  } and has a rating of ${attraction.rating}. It is 
    ${distanceInKilometers.toFixed(
      NUM_DECIMALS_IN_DISTANCE
    )} kilometers from ${getCityName(attraction)}.
    If you are interested in ${categoryText}, this is the place to be!`;

  return (
    <Card.Text>
      <SearchableText
        searchQuery={searchQuery}
        textToSearch={attractionCardText}
      />
    </Card.Text>
  );
};

const parseAttractionCategories = (categories: string[]): string => {
  const parsedCategories = categories.map((category) =>
    replace(category, "_", " ")
  );
  const lastCategory = `and ${parsedCategories[parsedCategories.length - 1]}`; // Make the sentence gramatically correct
  parsedCategories[parsedCategories.length - 1] = lastCategory;

  return parsedCategories.join(", ");
};

const getCityName = (attraction: Attraction) => {
  return (attraction.city && attraction.city.name) || "Undefined";
};

export default AttractionGridRow;
