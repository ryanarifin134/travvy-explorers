import { RouteComponentProps } from "react-router-dom";
import { Spinner } from "react-bootstrap";
import { FC, useState, useEffect } from "react";
import { getRequest } from "../../utils/requestHelper";
import { City } from "../Cities/City";
import { GetCitiesResponse } from "../Cities";
import { Hotel } from "../Hotels/Hotel";
import { GetHotelsResponse } from "../Hotels";
import { Attraction } from "../Attractions/Attraction";
import { GetAttractionsResponse } from "../Attractions";
import "./Search.scss";
import CitySearchItem from "../../components/SearchItem/CitySearchItem";
import AttractionSearchItem from "../../components/SearchItem/AttractionSearchItem";
import HotelSearchItem from "../../components/SearchItem/HotelSearchItem";

interface RouterProps {
  query: string;
}

const Search: FC<RouteComponentProps<RouterProps>> = (props) => {
  const [hotels, setHotels] = useState<Hotel[]>();
  const [hotelsCount, setHotelsCount] = useState(0);
  const [cities, setCities] = useState<City[]>();
  const [citiesCount, setCitiesCount] = useState(0);
  const [attractions, setAttractions] = useState<Attraction[]>();
  const [attractionsCount, setAttractionsCount] = useState(0);

  useEffect(() => {
    getCities();
    getHotels();
    getAttractions();
  }, [props.match.params.query]);

  const getCities = async (): Promise<void> => {
    let response = await getRequest<GetCitiesResponse>(
      `/api/cities?query=${props.match.params.query}`
    );
    const cities = response.cities;
    let page = 2;
    while (cities.length < response.totalCount) {
      response = await getRequest<GetCitiesResponse>(
        `/api/cities?query=${props.match.params.query}&page=${page}`
      );
      cities.push(...response.cities);
      page++;
    }
    setCities(cities);
    setCitiesCount(response.totalCount);
  };

  const getHotels = async (): Promise<void> => {
    let response = await getRequest<GetHotelsResponse>(
      `/api/hotels?query=${props.match.params.query}`
    );
    const hotels = response.hotels;
    let page = 2;
    while (hotels.length < response.totalCount) {
      response = await getRequest<GetHotelsResponse>(
        `/api/hotels?query=${props.match.params.query}&page=${page}`
      );
      hotels.push(...response.hotels);
      page++;
    }
    setHotels(hotels);
    setHotelsCount(response.totalCount);
  };

  const getAttractions = async (): Promise<void> => {
    let response = await getRequest<GetAttractionsResponse>(
      `/api/attractions?query=${props.match.params.query}`
    );
    const attractions = response.attractions;
    let page = 2;
    while (attractions.length < response.totalCount) {
      response = await getRequest<GetAttractionsResponse>(
        `/api/attractions?query=${props.match.params.query}&page=${page}`
      );
      attractions.push(...response.attractions);
      page++;
    }
    setAttractions(attractions);
    setAttractionsCount(response.totalCount);
  };

  if (!cities || !hotels || !attractions) {
    return <Spinner animation="border" variant="primary" />;
  }

  return (
    <div className="container">
      <div style={{ textAlign: "center" }}>
        <h1>
          Search Results for <i>{props.match.params.query}</i>
        </h1>
        <p>
          The most important attributes of each instance are displayed. To see
          more information, click on the name of the instance.
        </p>
      </div>

      <p className="model">
        Cities <span className="hits">(total hits: {citiesCount})</span>
      </p>
      <div className="model-section">
        {cities && cities.length > 0 ? (
          cities.map((city) => (
            <CitySearchItem city={city} query={props.match.params.query} />
          ))
        ) : (
          <p>No cities matched with query "{props.match.params.query}"</p>
        )}
      </div>

      <p className="model">
        Attractions{" "}
        <span className="hits">(total hits: {attractionsCount})</span>
      </p>
      <div className="model-section">
        {attractions && attractions.length > 0 ? (
          attractions.map((attraction) => (
            <AttractionSearchItem
              attraction={attraction}
              query={props.match.params.query}
            />
          ))
        ) : (
          <p>No attractions matched with query "{props.match.params.query}"</p>
        )}
      </div>

      <p className="model">
        Hotels <span className="hits">(total hits: {hotelsCount})</span>
      </p>
      <div className="model-section">
        {hotels && hotels.length > 0 ? (
          hotels.map((hotel) => (
            <HotelSearchItem hotel={hotel} query={props.match.params.query} />
          ))
        ) : (
          <p>No hotels matched with query "{props.match.params.query}"</p>
        )}
      </div>
    </div>
  );
};

export default Search;
