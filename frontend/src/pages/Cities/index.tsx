/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Link, useHistory } from "react-router-dom";
import {
  Col,
  Container,
  Row,
  Table,
  Button,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import "./Cities.scss";
import { FC, useEffect, useState } from "react";
import { City } from "./City";
import { getRequest } from "../../utils/requestHelper";
import Paginator, { INITIAL_PAGE_NUM } from "../Shared/Paginator";
import LoadingSpinner from "../Shared/LoadingSpinner";
import CityFilters from "./CityFilters";
import * as values from "../../data/cityOptions";
import SearchableText from "../Shared/SearchableText";
import { formatNumber } from "../../utils/formatHelper";

export interface GetCitiesResponse {
  cities: City[];
  totalCount: number;
}

const attributes = [
  "City",
  "Country",
  "Region",
  "Elevation",
  "Latitude",
  "Longitude",
  "Population",
  "Timezone",
];

const Cities: FC = (): JSX.Element => {
  const [cities, setCities] = useState<City[]>();
  const [totalCount, setTotalCount] = useState(0);

  const [currentPage, setCurrentPage] = useState(INITIAL_PAGE_NUM);

  const [filterCountryState, setFilterCountryState] = useState("");
  const [filterRegionState, setFilterRegionState] = useState("");
  const [filterTimezoneState, setFilterTimezoneState] = useState("");
  const [filterMinPopulationState, setFilterMinPopulationState] = useState(
    Number.MIN_SAFE_INTEGER
  );
  const [filterMaxPopulationState, setFilterMaxPopulationState] = useState(
    Number.MAX_SAFE_INTEGER
  );
  const [filterMinElevationState, setFilterMinElevationState] = useState(
    Number.MIN_SAFE_INTEGER
  );
  const [filterMaxElevationState, setFilterMaxElevationState] = useState(
    Number.MAX_SAFE_INTEGER
  );
  const [filterMinCovidCasesState, setFilterMinCovidCasesState] = useState(
    Number.MIN_SAFE_INTEGER
  );
  const [filterMaxCovidCasesState, setFilterMaxCovidCasesState] = useState(
    Number.MAX_SAFE_INTEGER
  );
  const [sortState, setSortState] = useState("");
  const [orderState, setOrderState] = useState("");
  const [currentQuery, setCurrentQuery] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [selectMode, setSelectMode] = useState(false);
  const [selectButtonDisabled, setSelectButtonDisabled] = useState(false);
  const [selectedCities, setSelectedCities] = useState([]);

  const history = useHistory();

  useEffect(() => {
    setCurrentPage(INITIAL_PAGE_NUM);
    getCities(INITIAL_PAGE_NUM);
  }, [
    filterCountryState,
    filterRegionState,
    filterTimezoneState,
    filterMinPopulationState,
    filterMaxPopulationState,
    filterMinElevationState,
    filterMaxElevationState,
    filterMinCovidCasesState,
    filterMaxCovidCasesState,
    sortState,
    orderState,
  ]);

  const getCities = async (page: number): Promise<void> => {
    setCities(undefined);

    const query = queryString();
    const response = await getRequest<GetCitiesResponse>(
      "/api/cities" + query + "&page=" + page
    );

    setCities(response.cities);
    setTotalCount(response.totalCount);
  };

  const queryString = (): string => {
    let output = "?";
    if (filterCountryState !== "") {
      output += "&country=" + filterCountryState;
    }
    if (filterRegionState !== "") {
      output += "&region=" + filterRegionState;
    }
    if (filterTimezoneState !== "") {
      output += "&timezone=" + filterTimezoneState;
    }
    if (filterMinPopulationState !== Number.MIN_SAFE_INTEGER) {
      output += "&min_population=" + filterMinPopulationState;
    }
    if (filterMaxPopulationState !== Number.MAX_SAFE_INTEGER) {
      output += "&max_population=" + filterMaxPopulationState;
    }
    if (filterMinElevationState !== Number.MIN_SAFE_INTEGER) {
      output += "&min_elevation=" + filterMinElevationState;
    }
    if (filterMaxElevationState !== Number.MAX_SAFE_INTEGER) {
      output += "&max_elevation=" + filterMaxElevationState;
    }
    if (filterMinCovidCasesState !== Number.MIN_SAFE_INTEGER) {
      output += "&min_covid_cases=" + filterMinCovidCasesState;
    }
    if (filterMaxCovidCasesState !== Number.MAX_SAFE_INTEGER) {
      output += "&max_covid_cases=" + filterMaxCovidCasesState;
    }
    if (sortState !== "") {
      output += "&sort_by=" + sortState;
    }
    if (orderState !== "") {
      output += "&order=" + orderState;
    }
    if (currentQuery !== "") {
      output += "&query=" + currentQuery;
    }

    return output;
  };

  const filterCountry = (country: any): void => {
    if (country["label"] === "---") {
      setFilterCountryState("");
    } else {
      setFilterCountryState(country["value"]);
    }
  };

  const filterRegion = (region: any): void => {
    if (region["label"] === "---") {
      setFilterRegionState("");
    } else {
      setFilterRegionState(region["value"]);
    }
  };

  const filterTimezone = (timezone: any): void => {
    if (timezone["label"] === "---") {
      setFilterTimezoneState("");
    } else {
      setFilterTimezoneState(timezone["value"]);
    }
  };

  const filterMinPopulation = (event: any): void => {
    setFilterMinPopulationState(event.target.value);
  };

  const filterMaxPopulation = (event: any): void => {
    if (event.target.value !== "") {
      setFilterMaxPopulationState(event.target.value);
    } else {
      setFilterMaxPopulationState(Number.MAX_SAFE_INTEGER);
    }
  };

  const filterMinElevation = (event: any): void => {
    setFilterMinElevationState(event.target.value);
  };

  const filterMaxElevation = (event: any): void => {
    if (event.target.value) {
      setFilterMaxElevationState(event.target.value);
    } else {
      setFilterMaxElevationState(Number.MAX_SAFE_INTEGER);
    }
  };

  const filterMinCovidCases = (event: any): void => {
    setFilterMinCovidCasesState(event.target.value);
  };

  const filterMaxCovidCases = (event: any): void => {
    if (event.target.value) {
      setFilterMaxCovidCasesState(event.target.value);
    } else {
      setFilterMaxCovidCasesState(Number.MAX_SAFE_INTEGER);
    }
  };

  const sortBy = (val: any): void => {
    if (val["label"] === "---") {
      setSortState("");
    } else {
      setSortState(val["value"]);
    }
  };

  const order = (val: any): void => {
    if (val["label"] === "---") {
      setOrderState("");
    } else {
      setOrderState(val["value"]);
    }
  };

  const onSearchChange = (event): void => {
    setCurrentQuery(event.target.value);
  };

  const onSearchSubmit = (): void => {
    setCurrentPage(INITIAL_PAGE_NUM);
    getCities(INITIAL_PAGE_NUM);
    setSearchQuery(currentQuery);
  };

  const onPageChange = (page: number): void => {
    setCurrentPage(page);
    getCities(page);
  };

  const onSelectButtonClick = (): void => {
    if (!selectMode) {
      setSelectButtonDisabled(!selectButtonDisabled);
      setSelectMode(true);
    } else {
      let path = "cities/compare";
      selectedCities.forEach((cityId) => {
        path += "/" + cityId;
      });
      history.push(path);
    }
  };

  const popover = (
    <Popover id="popover-basic">
      <Popover.Title as="h3">Select Cities</Popover.Title>
      <Popover.Content>
        Select up to <strong>4</strong> cities, then compare!
      </Popover.Content>
    </Popover>
  );

  const SelectButton = () => (
    <OverlayTrigger trigger="click" placement="left" overlay={popover}>
      <Button
        disabled={selectButtonDisabled}
        onClick={onSelectButtonClick}
        className="select-button"
        variant="primary"
      >
        {selectMode ? "Compare!" : "Select"}
      </Button>
    </OverlayTrigger>
  );

  return (
    <Container className="city-model" fluid>
      <Row className="justify-content-center">
        <Col>
          <h1>Cities</h1>
          <SelectButton />
        </Col>
      </Row>
      <CityFilters
        onSearchChange={onSearchChange}
        onSearchSubmit={onSearchSubmit}
        filterCountry={filterCountry}
        filterRegion={filterRegion}
        filterTimezone={filterTimezone}
        filterMinPopulation={filterMinPopulation}
        filterMaxPopulation={filterMaxPopulation}
        filterMinElevation={filterMinElevation}
        filterMaxElevation={filterMaxElevation}
        filterMinCovidCases={filterMinCovidCases}
        filterMaxCovidCases={filterMaxCovidCases}
        sortBy={sortBy}
        order={order}
        countryOptions={values.countryFilter}
        regionOptions={values.regionFilter}
        timezoneOptions={values.timezoneFilter}
        sortByOptions={values.sortByOptions}
        orderOptions={values.orderOptions}
      />
      <Table size="md" className="mb-0 mt-4">
        <thead>
          <tr>
            {attributes.map((attribute) => (
              <th>{attribute}</th>
            ))}
          </tr>
        </thead>
        {renderTableData(
          cities,
          searchQuery,
          selectedCities,
          setSelectedCities,
          selectMode,
          setSelectButtonDisabled
        )}
      </Table>
      {cities ? (
        <Paginator
          totalCount={totalCount}
          onPageChange={onPageChange}
          currentPage={currentPage}
        />
      ) : (
        <LoadingSpinner />
      )}
    </Container>
  );
};

const renderTableData = (
  cities: City[] | undefined,
  searchQuery: string,
  selectedCities: any,
  setSelectedCities: any,
  selectMode: any,
  setSelectButtonDisabled: any
) => {
  const onRowClick = (e) => {
    if (!selectMode) {
      return;
    }

    if (e.target.parentElement.classList.contains("hovered")) {
      const url = e.target.parentElement.children[0].children[0].href;
      const lastIndex = url.lastIndexOf("/");
      const id = url.substring(lastIndex + 1);
      const index = selectedCities.indexOf(id);
      if (index > -1) {
        selectedCities.splice(index, 1);
        setSelectedCities(selectedCities);
      }
      e.target.parentElement.classList.remove("hovered");
      if (selectedCities.length === 1) {
        setSelectButtonDisabled(true);
      }
      return;
    }

    if (selectedCities.length === 4) {
      return;
    }

    e.target.parentElement.classList.add("hovered");
    const url = e.target.parentElement.children[0].children[0].href;
    const lastIndex = url.lastIndexOf("/");
    const id = url.substring(lastIndex + 1);
    console.log("adding " + id);
    selectedCities.push(id);
    console.log(selectedCities);
    setSelectedCities(selectedCities);

    if (selectedCities.length === 2) {
      setSelectButtonDisabled(false);
    }

    console.log("here");
  };

  if (!cities) {
    return;
  }

  return (
    <>
      <tbody>
        {cities.map((city) => (
          <tr
            onClick={onRowClick}
            className={selectedCities.includes("" + city.id) ? "hovered" : ""}
          >
            <td>
              <Link to={`/cities/${city.id}`}>
                <SearchableText
                  searchQuery={searchQuery}
                  textToSearch={city.name}
                />
              </Link>
            </td>
            <td>
              <SearchableText
                searchQuery={searchQuery}
                textToSearch={city.country}
              />
            </td>
            <td>
              <SearchableText
                searchQuery={searchQuery}
                textToSearch={city.region}
              />
            </td>
            <td>
              <SearchableText
                searchQuery={searchQuery}
                textToSearch={city.elevation.toString()}
              />
            </td>
            <td>
              <SearchableText
                searchQuery={searchQuery}
                textToSearch={city.latitude.toString()}
              />
            </td>
            <td>
              <SearchableText
                searchQuery={searchQuery}
                textToSearch={city.longitude.toString()}
              />
            </td>
            <td>
              <SearchableText
                searchQuery={searchQuery}
                textToSearch={formatNumber(city.population).toString()}
              />
            </td>
            <td>
              <SearchableText
                searchQuery={searchQuery}
                textToSearch={city.timezone}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </>
  );
};

export default Cities;
