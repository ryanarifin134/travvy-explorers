/* eslint @typescript-eslint/no-empty-interface: "off" */
//import { Link } from "react-router-dom";
import { Container, Image, Row, Col } from "react-bootstrap";
import Map from "../../components/Map";
import "leaflet/dist/leaflet.css";
import { RouteComponentProps } from "react-router-dom";
import { FC, useEffect, useState } from "react";
import { City } from "./City";
import { getRequest } from "../../utils/requestHelper";
import AttributeCard from "../../components/AttributeCard";
import PreviewItem from "../../components/PreviewItem";
import { formatNumber } from "../../utils/formatHelper";
import LoadingSpinner from "../Shared/LoadingSpinner";

interface RouterProps {
  cityid: string;
}

const CityInstance: FC<RouteComponentProps<RouterProps>> = (props) => {
  const [city, setCity] = useState<City>();

  useEffect(() => {
    getCity();
  }, []);

  const getCity = async (): Promise<void> => {
    const city = await getRequest<City>(
      `/api/cities/${props.match.params.cityid}`
    );
    setCity(city);
  };

  if (!city) {
    return <LoadingSpinner />;
  }

  return (
    <div className="city-instance">
      <Container>
        <Row className="mb-3">
          <h1>{city.name}</h1>
        </Row>
        <Row className="mb-3">
          <p>{city.description}</p>
        </Row>
        <Row className="mb-3">
          <Image
            fluid
            src={city.imageUrl}
            style={{
              height: "25vw",
              display: "block",
              marginLeft: "auto",
              marginRight: "auto",
            }}
          />
        </Row>

        <Row className="mb-3">
          <Col>
            <AttributeCard
              title="Population"
              text={formatNumber(city.population)}
            />
          </Col>
          <Col>
            <AttributeCard
              title="Elevation"
              text={city.elevation + " meters"}
            />
          </Col>
          <Col>
            <AttributeCard title="Time zone" text={city.timezone} />
          </Col>
          <Col>
            <AttributeCard
              title="Covid cases"
              text={formatNumber(city.covidCases)}
            />
          </Col>
        </Row>
        <Row className="mb-3">
          <Col>
            <AttributeCard
              title="Latitude"
              text={formatNumber(city.latitude)}
            />
          </Col>
          <Col>
            <AttributeCard
              title="Longitude"
              text={formatNumber(city.longitude)}
            />
          </Col>
        </Row>
        <Row className="mb-3">
          <Col>
            <AttributeCard
              title="Number of attractions"
              text={city.num_attractions}
            />
          </Col>
          <Col>
            <AttributeCard title="Number of hotels" text={city.num_hotels} />
          </Col>
          <Col>
            <AttributeCard
              title="Average hotel rating"
              text={formatNumber(city.avg_hotel_rating)}
            />
          </Col>
        </Row>
      </Container>
      <div className="instance-links">
        <h3>Attractions in {city.name}</h3>
        <Container>
          {city.attractions.map((attraction) => (
            <PreviewItem
              link={`../attractions/${attraction.id}`}
              name={attraction.name}
              description={attraction.description}
            />
          ))}
        </Container>
        <h3>Hotels in {city.name}</h3>
        <Container>
          {city.hotels.map((hotel) => (
            <PreviewItem
              link={`../hotels/${hotel.id}`}
              name={hotel.name}
              description={hotel.description}
            />
          ))}
        </Container>
      </div>
      <Map lat={city.latitude} lon={city.longitude} />
    </div>
  );
};

export default CityInstance;
