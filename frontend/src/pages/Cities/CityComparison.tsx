export interface CityComparison {
  cities: City[];
}

interface City {
  id: number;
  name: string;
  country: string;
  region: string;
  latitude: number;
  longitude: number;
  elevation: number;
  population: number;
  timezone: string;
  covidCases: number;
  imageUrl: string;
  description: string;
  num_hotels: number;
  num_attractions: number;
  avg_hotel_rating: number;
  rankings: Ranking;
}

interface Ranking {
  population: number;
  elevation: number;
  covidCases: number;
}
