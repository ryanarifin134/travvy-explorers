/* eslint-disable  @typescript-eslint/no-explicit-any */
import React from "react";
import Select from "react-select";
import { Row, Col, InputGroup, FormControl } from "react-bootstrap";
import ModelSearchBar from "../../components/ModelSearchBar";

interface filterProps {
  onSearchChange: (event: any) => void;
  onSearchSubmit: () => void;
  filterCountry: (country: any) => void;
  filterRegion: (region: any) => void;
  filterTimezone: (timezone: any) => void;
  filterMinPopulation: (minPopulation: any) => void;
  filterMaxPopulation: (maxPopulation: any) => void;
  filterMinElevation: (minElevation: any) => void;
  filterMaxElevation: (maxElevation: any) => void;
  filterMinCovidCases: (minCovidCases: any) => void;
  filterMaxCovidCases: (maxCovidCases: any) => void;
  sortBy: (val: any) => void;
  order: (val: any) => void;
  countryOptions: any;
  regionOptions: any;
  timezoneOptions: any;
  sortByOptions: any;
  orderOptions: any;
}

const CityFilters: React.FC<filterProps> = (props: filterProps) => {
  return (
    <>
      <Row className="mt-2">
        <Col md={2}>
          <ModelSearchBar
            model="cities"
            onSearchChange={props.onSearchChange}
            onSearchSubmit={props.onSearchSubmit}
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.filterCountry(x)}
            placeholder="Country"
            options={props.countryOptions}
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.filterRegion(x)}
            placeholder="Region"
            options={props.regionOptions}
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.filterTimezone(x)}
            placeholder="Timezone"
            options={props.timezoneOptions}
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.sortBy(x)}
            placeholder="Sort By"
            options={props.sortByOptions}
          />
        </Col>
        <Col md={2}>
          <Select
            onChange={(x: any) => props.order(x)}
            placeholder="Order"
            options={props.orderOptions}
          />
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md={2}>
          <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-sm">
                Min population
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl onChange={(x: any) => props.filterMinPopulation(x)} />
          </InputGroup>
        </Col>
        <Col md={2}>
          <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-sm">
                Max Population
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl onChange={(x: any) => props.filterMaxPopulation(x)} />
          </InputGroup>
        </Col>
        <Col md={2}>
          <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-sm">
                Min Elevation
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl onChange={(x: any) => props.filterMinElevation(x)} />
          </InputGroup>
        </Col>
        <Col md={2}>
          <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-sm">
                {" "}
                Max Elevation{" "}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl onChange={(x: any) => props.filterMaxElevation(x)} />
          </InputGroup>
        </Col>
        <Col md={2}>
          <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-sm">
                {" "}
                Min Covid Cases{" "}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl onChange={(x: any) => props.filterMinCovidCases(x)} />
          </InputGroup>
        </Col>
        <Col md={2}>
          <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-sm">
                {" "}
                Max Covid Case{" "}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl onChange={(x: any) => props.filterMaxCovidCases(x)} />
          </InputGroup>
        </Col>
      </Row>
    </>
  );
};

export default CityFilters;
