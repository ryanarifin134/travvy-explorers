import { Attraction } from "../Attractions/Attraction";
import { Hotel } from "../Hotels/Hotel";

export interface City {
  id: number;
  name: string;
  country: string;
  region: string;
  latitude: number;
  longitude: number;
  elevation: number;
  population: number;
  timezone: string;
  covidCases: number;
  imageUrl: string;
  description: string;
  num_hotels: number;
  num_attractions: number;
  avg_hotel_rating: number;
  attractions: Attraction[];
  hotels: Hotel[];
}
