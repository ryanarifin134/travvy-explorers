// import { Container, Image, Row, Col } from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
// import Map from "../../components/Map";
import "leaflet/dist/leaflet.css";
import { Col, Container, Row } from "react-bootstrap";
import { RouteComponentProps } from "react-router-dom";
import { FC, useEffect, useState } from "react";
import { CityComparison } from "./CityComparison";
import { getRequest } from "../../utils/requestHelper";
// import AttributeCard from "../../components/AttributeCard";
// import PreviewItem from "../../components/PreviewItem";
// import {formatNumber} from "../../utils/formatHelper";
import LoadingSpinner from "../Shared/LoadingSpinner";
import "./CityComparison.scss";

interface RouterProps {
  cityid1: string;
  cityid2: string;
  cityid3: string;
  cityid4: string;
}

const CityCompare: FC<RouteComponentProps<RouterProps>> = (props) => {
  const [cityComparison, setCityComparison] = useState<CityComparison>();

  let path = "";
  if (props.match.params.cityid1) {
    path += "/" + props.match.params.cityid1;
  }
  if (props.match.params.cityid2) {
    path += "/" + props.match.params.cityid2;
  }
  if (props.match.params.cityid3) {
    path += "/" + props.match.params.cityid3;
  }
  if (props.match.params.cityid4) {
    path += "/" + props.match.params.cityid4;
  }

  useEffect(() => {
    getComparison();
  }, []);

  const getComparison = async (): Promise<void> => {
    const cityComparison = await getRequest<CityComparison>(
      `/api/cities/compare${path}`
    );
    setCityComparison(cityComparison);
  };

  if (!cityComparison) {
    return <LoadingSpinner />;
  }

  return (
    <Container className="city-model" fluid>
      <Row className="justify-content-center">
        <Col>
          <h1>City Comparison</h1>
        </Col>
      </Row>
      <div className="city-comparison">
        {cityComparison.cities.map((city) => (
          <ListGroup>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Name:</span>
                <span>{city.name}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Country:</span>
                <span>{city.country}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Region:</span>
                <span>{city.region}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Elevation:</span>
                <span>
                  (Rank: {city.rankings.elevation}) {city.elevation}
                </span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Population:</span>
                <span>
                  (Rank: {city.rankings.population}) {city.population}
                </span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Timezone:</span>
                <span>{city.timezone}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="compare-row">
                <span>Covid Cases:</span>
                <span>
                  (Rank: {city.rankings.covidCases}) {city.covidCases}
                </span>
              </div>
            </ListGroup.Item>
          </ListGroup>
        ))}
      </div>
    </Container>
  );
};

export default CityCompare;
