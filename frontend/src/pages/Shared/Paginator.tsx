import { FC, ReactElement } from "react";
import classNames from "classnames";
import "./pagination.scss";

interface PaginatorProps {
  currentPage: number;
  totalCount: number;
  entitiesPerPage?: number;
  onPageChange: (page: number) => void;
}

export const INITIAL_PAGE_NUM = 1;
const NUM_ENTITIES_PER_PAGE = 10;
const NUM_PAGES_TO_SHOW = 5;

const Paginator: FC<PaginatorProps> = (props): ReactElement => {
  const { entitiesPerPage, totalCount, currentPage, onPageChange } = props;

  const numEntitiesPerPage = entitiesPerPage || NUM_ENTITIES_PER_PAGE;
  const totalPageCount = Math.ceil(totalCount / numEntitiesPerPage);

  const pages = getPageNumberRange(totalPageCount, currentPage);

  return (
    <div className="float-right">
      <ul className="pagination pr-5">
        <li className="page-item">
          <button
            className={classNames("page-link", {
              disabled: currentPage === INITIAL_PAGE_NUM,
            })}
            disabled={currentPage === INITIAL_PAGE_NUM}
            onClick={() => onPageChange(INITIAL_PAGE_NUM)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-chevron-double-left"
              viewBox="0 0 16 16"
            >
              <path
                fill-rule="evenodd"
                d="M8.354 1.646a.5.5 0 0 1 0 .708L2.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
              />
              <path
                fill-rule="evenodd"
                d="M12.354 1.646a.5.5 0 0 1 0 .708L6.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
              />
            </svg>
          </button>
        </li>
        <li className="page-item">
          <button
            className={classNames("page-link", {
              disabled: currentPage === INITIAL_PAGE_NUM,
            })}
            disabled={currentPage === INITIAL_PAGE_NUM}
            onClick={() => onPageChange(currentPage - 1)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-chevron-left"
              viewBox="0 0 16 16"
            >
              <path
                fill-rule="evenodd"
                d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
              />
            </svg>
          </button>
        </li>
        {pages.map((page) => (
          <li
            className={classNames("page-item", {
              active: page === currentPage,
            })}
          >
            <button className="page-link" onClick={() => onPageChange(page)}>
              {page}
            </button>
          </li>
        ))}
        <li className="page-item">
          <button
            className={classNames("page-link", {
              disabled: currentPage === totalPageCount,
            })}
            disabled={currentPage === totalPageCount}
            onClick={() => onPageChange(currentPage + 1)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-chevron-right"
              viewBox="0 0 16 16"
            >
              <path
                fill-rule="evenodd"
                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
              />
            </svg>
          </button>
        </li>
        <li>
          <button
            className={classNames("page-link", {
              disabled: currentPage === totalPageCount,
            })}
            disabled={currentPage === totalPageCount}
            onClick={() => onPageChange(totalPageCount)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-chevron-double-right"
              viewBox="0 0 16 16"
            >
              <path
                fill-rule="evenodd"
                d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z"
              />
              <path
                fill-rule="evenodd"
                d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z"
              />
            </svg>
          </button>
        </li>
        <li className="entity-counter  pt-2 pl-2">
          {(currentPage - 1) * numEntitiesPerPage + 1} to{" "}
          {currentPage * numEntitiesPerPage < totalCount
            ? currentPage * numEntitiesPerPage
            : totalCount}{" "}
          of {totalCount}
        </li>
      </ul>
    </div>
  );
};

const getPageNumberRange = (totalPageCount: number, currentPage: number) => {
  const adjustedCurrentPage = currentPage - 1;

  const bottomOfRange =
    Math.floor(adjustedCurrentPage / NUM_PAGES_TO_SHOW) * NUM_PAGES_TO_SHOW + 1;
  const topOfRange =
    bottomOfRange + NUM_PAGES_TO_SHOW - 1 < totalPageCount
      ? bottomOfRange + NUM_PAGES_TO_SHOW - 1
      : totalPageCount;

  const pageRange: number[] = [];
  for (let i = bottomOfRange; i <= topOfRange; i++) {
    pageRange.push(i);
  }

  return pageRange;
};

export default Paginator;
