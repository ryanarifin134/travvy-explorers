import { ReactElement } from "react";
import { Spinner } from "react-bootstrap";

const LoadingSpinner = (): ReactElement => {
  return (
    <div className="text-center">
      <Spinner animation="border" variant="primary" />
    </div>
  );
};

export default LoadingSpinner;
