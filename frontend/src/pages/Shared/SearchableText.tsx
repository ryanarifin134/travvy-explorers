import Highlighter from "react-highlight-words";
import { FC } from "react";

interface SearchableTextProps {
  searchQuery: string;
  textToSearch: string;
}

const SearchableText: FC<SearchableTextProps> = (props) => {
  const { searchQuery, textToSearch } = props;

  return (
    <Highlighter
      searchWords={[searchQuery]}
      textToHighlight={textToSearch}
      highlightStyle={{ backgroundColor: "#00ace6", color: "white" }}
    />
  );
};

export default SearchableText;
