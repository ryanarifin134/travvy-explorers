import { PureComponent } from "react";
import "./About.scss";
import { teamMembers } from "../../data/TeamMembers";
import TeamMemberCard from "../../components/TeamMemberCard";
import { Container, Row, Col } from "react-bootstrap";
import { toolsInfo, travvyInfo, apiInfo } from "../../data/Tools";
import ToolCard from "../../components/ToolCard";

interface TeamMember {
  name: string;
  username: string;
  emails: string[];
  description: string;
  role: string;
  commits: number;
  issues: number;
  tests: number;
}

interface AboutPageState {
  totalCommits: number;
  totalIssues: number;
  totalTests: number;
  teamMembers: TeamMember[];
  authorToCommits: Map<string, number>;
  usernamesToIssues: Map<string, number>;
}

interface Commit {
  author_email: string;
  author_name: string;
}

interface Author {
  username: string;
}

interface Issue {
  author: Author;
}

class About extends PureComponent<Record<string, unknown>, AboutPageState> {
  state = {
    totalCommits: 0,
    totalIssues: 0,
    totalTests: 108,
    teamMembers: teamMembers,
    authorToCommits: new Map<string, number>(),
    usernamesToIssues: new Map<string, number>(),
  };

  getCommits = (firstName: string): number => {
    let commits = 0;
    this.state.authorToCommits.forEach((commitNum, authorName) => {
      if (authorName.toLowerCase().includes(firstName)) {
        commits += commitNum;
      }
    });
    return commits;
  };

  getCommitsPerMember = (commitArray: Array<Commit>): Map<string, number> => {
    const newAuthorToCommits: Map<string, number> = new Map();
    commitArray.forEach((commit: Commit) => {
      newAuthorToCommits.set(
        commit.author_name,
        1 + (newAuthorToCommits.get(commit.author_name) ?? 0)
      );
    });
    return newAuthorToCommits;
  };

  getIssuesPerMember = (issueArray: Array<Issue>): Map<string, number> => {
    const newUsernamesToIssues: Map<string, number> = new Map();
    issueArray.forEach((issue: Issue) => {
      newUsernamesToIssues.set(
        issue.author.username,
        1 + (newUsernamesToIssues.get(issue.author.username) ?? 0)
      );
    });
    return newUsernamesToIssues;
  };

  getGitlabCommits = async (): Promise<Commit[]> => {
    const projId = "24723668";

    let finishedPagination = false;
    const commitArray: Commit[] = [];

    let currentPage = 1;

    while (!finishedPagination) {
      const response = await fetch(
        `https://gitlab.com/api/v4/projects/${projId}/repository/commits?page=${currentPage}&per_page=100`
      );
      const commits = await response.json();
      // commitArray.push.apply(commitArray, commits)
      commitArray.push(...commits);

      const nextPage = response.headers.get("x-next-page");
      if (nextPage) {
        currentPage = parseInt(nextPage);
      } else {
        finishedPagination = true;
      }
    }

    return commitArray;
  };

  getGitlabIssues = async (): Promise<Issue[]> => {
    const projId = "24723668";

    let finishedPagination = false;
    const issueArray: Issue[] = [];

    let currentPage = 1;

    while (!finishedPagination) {
      const response = await fetch(
        `https://gitlab.com/api/v4/projects/${projId}/issues?page=${currentPage}`
      );
      const issues = await response.json();
      // issueArray.push.apply(issueArray, issues)
      issueArray.push(...issues);

      const nextPage = response.headers.get("x-next-page");
      if (nextPage) {
        currentPage = parseInt(nextPage);
      } else {
        finishedPagination = true;
      }
    }

    return issueArray;
  };

  populateGitLabState = async (): Promise<void> => {
    const commits = await this.getGitlabCommits();
    const issues = await this.getGitlabIssues();

    const newAuthorToCommits = this.getCommitsPerMember(commits);
    const newUsernamesToIssues = this.getIssuesPerMember(issues);

    this.setState({
      totalCommits: commits.length,
      totalIssues: issues.length,
      authorToCommits: newAuthorToCommits,
      usernamesToIssues: newUsernamesToIssues,
    });
  };

  interval = setInterval(this.populateGitLabState, 5000);

  componentDidMount(): void {
    this.populateGitLabState();
  }

  componentWillUnmount(): void {
    this.stopPolling();
  }

  stopPolling = (): void => {
    if (this.interval) {
      clearInterval(this.interval);
    }
  };

  render(): JSX.Element {
    return (
      <Container>
        <div className="about-header">
          <div className="section-title">About Us</div>
          Travvy Explorers aims to promote cultural awareness by helping
          travellers find the best attractions to visit and the greatest hotels
          to stay at in cities around the world. It does this by providing
          international travel-oriented information such as attractions,
          lodging, and COVID data.
        </div>
        <div className="section-title">Our Team</div>
        <div className="about-members">
          <Row>
            {Object.entries(teamMembers).map((member) => (
              <Col>
                <TeamMemberCard
                  key={member[1].username}
                  name={member[1].name}
                  role={member[1].role}
                  description={member[1].description}
                  commits={this.getCommits(
                    member[1].name
                      .toLowerCase()
                      .substring(0, member[1].name.indexOf(" "))
                  )}
                  issues={
                    this.state.usernamesToIssues.get(member[1].username) ?? 0
                  }
                  tests={member[1].tests}
                />
              </Col>
            ))}
          </Row>
        </div>
        <div className="repo-stats">
          <div className="section-title">Repository Statistics</div>
          <p>
            Total Commits:{" "}
            {this.state.totalCommits === 0
              ? "loading..."
              : this.state.totalCommits}
          </p>
          <p>
            Total Issues:{" "}
            {this.state.totalIssues === 0
              ? "loading..."
              : this.state.totalIssues}
          </p>
          <p>Total Tests: {this.state.totalTests}</p>
        </div>
        <div className="apis">
          <div className="section-title">APIs Used</div>
          <div className="tool-cards">
            {apiInfo.map((tool) => {
              const { title, img, link, description } = tool;

              return (
                <ToolCard
                  key={title}
                  title={title}
                  img={img}
                  link={link}
                  description={description}
                />
              );
            })}
          </div>
        </div>
        <div className="technologies">
          <div className="section-title">Technologies Used</div>
          <div className="tool-cards">
            {toolsInfo.map((tool) => {
              const { title, img, link, description } = tool;
              return (
                <ToolCard
                  key={title}
                  title={title}
                  img={img}
                  link={link}
                  description={description}
                />
              );
            })}
          </div>
        </div>
        <div className="project-links">
          <div className="section-title">Project Links</div>
          <div className="tool-cards">
            {travvyInfo.map((tool) => {
              const { title, img, link, description } = tool;
              return (
                <ToolCard
                  key={title}
                  title={title}
                  img={img}
                  link={link}
                  description={description}
                />
              );
            })}
          </div>
        </div>
      </Container>
    );
  }
}

export default About;
