import { PureComponent } from "react";
import "./Home.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";

class Home extends PureComponent<Record<string, unknown>> {
  title = "Travvy Explorers\n";

  render(): JSX.Element {
    return (
      <div className="home">
        <div className="home-text">
          <FontAwesomeIcon icon={faPaperPlane} size={"3x"} />
          <div>
            <span className="title">{this.title}</span>
          </div>
          <div>Information for the travel-savvy explorer</div>
        </div>
      </div>
    );
  }
}

export default Home;
