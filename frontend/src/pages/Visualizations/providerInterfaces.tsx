export interface GetProviderResponse {
  _links: {
    next: string;
    prev: string;
    self: string;
  };
  _meta: {
    page: number;
    per_page: number;
    total_items: number;
    total_pages: number;
  };
  items: Provider[];
}

export interface GetFacilityResponse {
  _links: {
    next: string;
    prev: string;
    self: string;
  };
  _meta: {
    page: number;
    per_page: number;
    total_items: number;
    total_pages: number;
  };
  items: Facility[];
}

export interface Provider {
  county: {
    abbrev: string;
    name: string;
  }[];
  facilities: {
    name: string;
    orisCode: number;
  }[];
  fixed_monthly_charge: number;
  fuel_source: string[];
  name: string;
  net_generation: Array<Array<number | string>>;
  operator_id: number;
  outage_map: string;
  phone_number: string;
  sector: string;
  website: string;
}

export interface Facility {
  coords: {
    lat: number;
    long: number;
  };
  county: {
    code: string;
    name: string;
  };
  fuel_consumption: Array<Array<number | string>>;
  fuel_source: string;
  name: string;
  net_generation: Array<Array<number | string>>;
  oris_code: number;
  provider: {
    name: string;
    operatorID: number;
  };
  state: {
    abbrev: string;
    name: string;
  };
}

export const green = ["GEO", "HPS", "HYC", "NUC", "ORW", "SUN", "WND", "WWW"];
