export const hotelPriceRangeCounts = [
  { name: 'Less than $50', value: 755 },
  { name: '$50 to $100', value: 3746 },
  { name: '$100 to $150', value: 2800 },
  { name: '$150 to $200', value: 1535 },
  { name: 'Greater than $200', value: 3794 }
];