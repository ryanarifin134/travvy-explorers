/* eslint-disable  @typescript-eslint/no-explicit-any */
import { FC, useState, useEffect } from "react";
import { Spinner, Container } from "react-bootstrap";
import { getRequest, getOtherRequest } from "../../utils/requestHelper";
import { City } from "../Cities/City";
import { GetCitiesResponse } from "../Cities";
import {
  Provider,
  GetProviderResponse,
  GetFacilityResponse,
  green,
} from "./providerInterfaces";
import {
  Label,
  BarChart,
  ScatterChart,
  Scatter,
  Bar,
  XAxis,
  YAxis,
  ZAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  PieChart,
  Pie,
  Cell,
  AreaChart,
  Area,
} from "recharts";
import { curveCardinal } from "d3-shape";
import { hotelPriceRangeCounts } from "./data";

const Visualizations: FC = (): JSX.Element => {
  const [
    citiesWithHighestPopulation,
    setCitiesWithHighestPopulation,
  ] = useState<City[]>();
  const [usCities, setUSCities] = useState<City[]>();
  // for provider visualizations
  const [providers, setProviders] = useState<Provider[]>();
  // const[facilities, setfacilities] = useState<Facility[]>();
  const [fuel, setfuel] = useState<{ name: string; value: number }[]>();
  const [usage, setUsage] = useState<{ name: string; value: number }[]>();
  useEffect(() => {
    getCitiesWithHighestPopulation();
    getUSCities();
    getProviders();
    getFuel();
    getUsage();
  }, []);

  const getCitiesWithHighestPopulation = async (): Promise<void> => {
    const response = await getRequest<GetCitiesResponse>(
      "/api/cities?sort_by=population&order=descending"
    );
    setCitiesWithHighestPopulation(response.cities);
  };

  const getUSCities = async (): Promise<void> => {
    let citiesResponse = await getRequest<GetCitiesResponse>(
      "/api/cities?country=United States of America"
    );
    const cities = citiesResponse.cities;
    let page = 2;
    while (cities.length < 50) {
      citiesResponse = await getRequest<GetCitiesResponse>(
        `/api/cities?page=${page}&country=United States of America`
      );
      cities.push(...citiesResponse.cities);
      page++;
    }
    setUSCities(cities);
  };
  function checkName(cur) {
    return cur.fixed_monthly_charge > 0;
  }

  // these are the provider visualization data
  const getProviders = async (): Promise<void> => {
    const providersResponse = await getOtherRequest<GetProviderResponse>(
      "https://www.wattsupenergy.me/api/providers"
    );

    const providers = providersResponse.items;
    // setProviders(providersResponse.items);
    setProviders(providers.filter(checkName).slice(1, 6));
  };

  // these are the provider visualization data

  const getFuel = async (): Promise<void> => {
    const facilityResponse = await getOtherRequest<GetFacilityResponse>(
      "https://www.wattsupenergy.me/api/facilities"
    );

    const facilities = facilityResponse.items;
    const arrayLength = facilities.length;
    let greenCount = 0;
    let otherCount = 0;
    for (let i = 0; i < arrayLength; i++) {
      if (green.includes(facilities[i].fuel_source)) {
        greenCount++;
      } else {
        otherCount++;
      }
    }
    const data = [
      { name: "Low-Emission Fuels", value: greenCount },
      { name: "Fossil/Carbon Fuels", value: otherCount },
    ];
    setfuel(data);
  };

  const getUsage = async (): Promise<void> => {
    const facilityResponse = await getOtherRequest<GetFacilityResponse>(
      "https://www.wattsupenergy.me/api/facilities"
    );

    const curFac = facilityResponse.items[3];
    console.log(curFac.name);
    console.log(curFac.oris_code);
    // console.log(curFac.fuel_consumption)
    console.log(curFac.fuel_consumption[16]);
    const data: any[] = [];
    let index = 0;
    for (let i = 16; i >= 0; i--) {
      data[index] = {
        name: curFac.fuel_consumption[i][0],
        value: curFac.fuel_consumption[i][1],
      };
      index++;
    }
    console.log(data[0]);
    // const data = [
    //   { name: 'Low-Emission Fuels', value: greenCount },
    //   { name: 'Fossil/Carbon Fuels', value:  otherCount },

    // ];
    setUsage(data);
  };

  const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042", "#FF1940"];
  const cardinal = curveCardinal.tension(0.2);

  return (
    <Container>
      <h1>Our Visualizations</h1>
      <h3 className="text-center">Top 10 Cities with Highest Population</h3>
      {citiesWithHighestPopulation ? (
        <div style={{ width: "70vw", height: "70vh", margin: "0 auto" }}>
          <ResponsiveContainer width="100%" height="100%">
            <BarChart
              data={citiesWithHighestPopulation}
              margin={{
                top: 50,
                right: 35,
                left: 35,
                bottom: 50,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="population" fill="#00ace6" />
            </BarChart>
          </ResponsiveContainer>
        </div>
      ) : (
        <Spinner animation="border" variant="primary" />
      )}

      <h3 className="text-center">
        Breakdown of hotels in different price ranges
      </h3>
      <div style={{ width: "50vw", height: "60vh", margin: "0 auto" }}>
        <ResponsiveContainer width="100%" height="100%">
          <PieChart width={400} height={400}>
            <Pie
              data={hotelPriceRangeCounts}
              cx="50%"
              cy="50%"
              labelLine={false}
              outerRadius={180}
              fill="#8884d8"
              dataKey="value"
              label
            >
              {COLORS.map((color, index) => (
                <Cell key={`cell-${index}`} fill={color} />
              ))}
            </Pie>
            <Tooltip />
            <Legend />
          </PieChart>
        </ResponsiveContainer>
      </div>

      <h3 className="text-center">
        Number of Attractions and Hotels for Cities in the US
      </h3>
      <p className="text-center">
        Some data points are overlapping, so it will look like there are a lot
        fewer cities than the amount we have in our database.
      </p>
      {usCities ? (
        <div style={{ width: "70vw", height: "70vh", margin: "0 auto" }}>
          <ResponsiveContainer width="100%" height="100%">
            <ScatterChart
              margin={{
                top: 20,
                right: 50,
                bottom: 50,
                left: 50,
              }}
            >
              <CartesianGrid />
              <XAxis type="number" dataKey="num_hotels" name="Num hotels">
                <Label value="Num hotels" position="bottom" />
              </XAxis>
              <YAxis
                type="number"
                dataKey="num_attractions"
                name="Num attractions"
              >
                <Label
                  value="Num attractions"
                  position="insideLeft"
                  angle={-90}
                />
              </YAxis>
              <ZAxis dataKey="name" name="City" />
              <Tooltip cursor={{ strokeDasharray: "3 3" }} />
              <Scatter name="city" data={usCities} fill="#00ace6" />
            </ScatterChart>
          </ResponsiveContainer>
        </div>
      ) : (
        <Spinner animation="border" variant="primary" />
      )}
      <h1>Provider Visualizations</h1>
      <h3 className="text-center">
        Fixed Monthly Charge Rate for 5 different Providers
      </h3>
      <p className="text-center">
        Comparison of monthly charging rate of energy for 5 of the providers in
        our developer's database
      </p>
      {providers ? (
        <div style={{ width: "86vw", height: "70vh", margin: "0 auto" }}>
          <ResponsiveContainer width="100%" height="100%">
            <BarChart
              data={providers}
              margin={{
                top: 50,
                right: 25,
                left: 25,
                bottom: 50,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="fixed_monthly_charge" fill="#00ace6" />
            </BarChart>
          </ResponsiveContainer>
        </div>
      ) : (
        <Spinner animation="border" variant="primary" />
      )}

      <h3 className="text-center">Low-Emission vs. Fossil/Carbon Fuel</h3>
      <p className="text-center">
        Shows amount of facilities that run on Low-Emission vs. Fossil/Carbon
        Fuel in our developer's database
      </p>
      {fuel ? (
        <div style={{ width: "50vw", height: "60vh", margin: "0 auto" }}>
          <ResponsiveContainer width="100%" height="100%">
            <PieChart width={400} height={400}>
              <Pie
                data={fuel}
                cx="50%"
                cy="50%"
                labelLine={false}
                outerRadius={180}
                fill="#8884d8"
                dataKey="value"
              >
                {fuel.map((entry, index) => (
                  <Cell
                    key={`cell-${index}`}
                    fill={COLORS[index % COLORS.length]}
                  />
                ))}
              </Pie>
              <Legend />
            </PieChart>
          </ResponsiveContainer>
        </div>
      ) : (
        <Spinner animation="border" variant="primary" />
      )}

      <h3 className="text-center">
        Annual Fuel Consumption (2003- 2019) for the El Paso Electric Co Energy
        Facility
      </h3>
      {usage ? (
        <div style={{ width: "50vw", height: "50vh", margin: "0 auto" }}>
          <ResponsiveContainer width="100%" height="100%">
            <AreaChart
              width={500}
              height={400}
              data={usage}
              margin={{
                top: 10,
                right: 30,
                left: 0,
                bottom: 0,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Area
                type={cardinal}
                dataKey="value"
                stroke="#82ca9d"
                fill="#00ace6"
                fillOpacity={0.3}
              />
            </AreaChart>
          </ResponsiveContainer>
        </div>
      ) : (
        <Spinner animation="border" variant="primary" />
      )}
    </Container>
  );
};

export default Visualizations;
