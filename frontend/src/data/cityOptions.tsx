export const sortByOptions = [
  { value: "---", label: "---" },
  { value: "city", label: "City" },
  { value: "country", label: "Country" },
  { value: "region", label: "Region" },
  { value: "population", label: "Population" },
  { value: "elevation", label: "Elevation" },
  { value: "timezone", label: "Timezone" },
  { value: "covid_cases", label: "Covid Cases" },
];

export const orderOptions = [
  { value: "---", label: "---" },
  { value: "ascending", label: "Ascending" },
  { value: "descending", label: "Descending" },
];

export const countryFilter = [
  { value: "---", label: "---" },
  { value: "United States of America", label: "United States of America" },
  { value: "Japan", label: "Japan" },
  { value: "United Kingdom", label: "United Kingdom" },
  { value: "France", label: "France" },
  { value: "Canada", label: "Canada" },
  { value: "Australia", label: "Australia" },
  { value: "Germany", label: "Germany" },
  { value: "South Korea", label: "South Korea" },
  { value: "Spain", label: "Spain" },
  { value: "Austria", label: "Austria" },
  { value: "People's Republic of China", label: "People's Republic of China" },
  { value: "Italy", label: "Italy" },
  { value: "Netherlands", label: "Netherlands" },
  { value: "United Arab Emirates", label: "United Arab Emirates" },
  { value: "Sweden", label: "Sweden" },
  { value: "Russia", label: "Russia" },
  { value: "Israel", label: "Israel" },
  { value: "Norway", label: "Norway" },
  { value: "Turkey", label: "Turkey" },
  { value: "Denmark", label: "Denmark" },
  { value: "Ireland", label: "Ireland" },
  { value: "Finland", label: "Finland" },
  { value: "Switzerland", label: "Switzerland" },
  { value: "Brazil", label: "Brazil" },
  { value: "Argentina", label: "Argentina" },
  { value: "Thailand", label: "Thailand" },
  { value: "Greece", label: "Greece" },
  { value: "Hungary", label: "Hungary" },
  { value: "Malaysia", label: "Malaysia" },
  { value: "India", label: "India" },
  { value: "Mexico", label: "Mexico" },
  { value: "Portugal", label: "Portugal" },
  { value: "Poland", label: "Poland" },
  { value: "Indonesia", label: "Indonesia" },
  { value: "Chile", label: "Chile" },
  { value: "Colombia", label: "Colombia" },
  { value: "Romania", label: "Romania" },
  { value: "Luxembourg", label: "Luxembourg" },
  { value: "Estonia", label: "Estonia" },
  { value: "Bulgaria", label: "Bulgaria" },
  { value: "Belgium", label: "Belgium" },
  { value: "Saudi Arabia", label: "Saudi Arabia" },
  { value: "Qatar", label: "Qatar" },
  { value: "Latvia", label: "Latvia" },
  { value: "South Africa", label: "South Africa" },
  { value: "Iceland", label: "Iceland" },
  { value: "Peru", label: "Peru" },
  { value: "Lithuania", label: "Lithuania" },
  { value: "Slovenia", label: "Slovenia" },
  { value: "Sri Lanka", label: "Sri Lanka" },
  { value: "Kuwait", label: "Kuwait" },
  { value: "Serbia", label: "Serbia" },
  { value: "Czech Republic", label: "Czech Republic" },
  { value: "Slovakia", label: "Slovakia" },
  { value: "Croatia", label: "Croatia" },
  { value: "Pakistan", label: "Pakistan" },
  { value: "Iran", label: "Iran" },
  { value: "Vietnam", label: "Vietnam" },
  { value: "Egypt", label: "Egypt" },
  { value: "Uruguay", label: "Uruguay" },
  { value: "Georgia", label: "Georgia" },
  { value: "Oman", label: "Oman" },
  { value: "Kazakhstan", label: "Kazakhstan" },
  { value: "Bahrain", label: "Bahrain" },
  { value: "North Macedonia", label: "North Macedonia" },
  { value: "Azerbaijan", label: "Azerbaijan" },
  { value: "Lebanon", label: "Lebanon" },
  { value: "Moldova", label: "Moldova" },
  { value: "Jordan", label: "Jordan" },
  { value: "Morocco", label: "Morocco" },
  { value: "Bosnia and Herzegovina", label: "Bosnia and Herzegovina" },
  { value: "Kenya", label: "Kenya" },
  { value: "Bangladesh", label: "Bangladesh" },
  { value: "Venezuela", label: "Venezuela" },
  { value: "Belarus", label: "Belarus" },
  { value: "Ukraine", label: "Ukraine" },
  { value: "El Salvador", label: "El Salvador" },
  { value: "Jamaica", label: "Jamaica" },
  { value: "Albania", label: "Albania" },
  { value: "Mongolia", label: "Mongolia" },
  { value: "Bolivia", label: "Bolivia" },
  { value: "Nigeria", label: "Nigeria" },
  { value: "The Bahamas", label: "The Bahamas" },
  { value: "Tunisia", label: "Tunisia" },
  { value: "Paraguay", label: "Paraguay" },
  { value: "Trinidad and Tobago", label: "Trinidad and Tobago" },
  { value: "Algeria", label: "Algeria" },
  { value: "Kyrgyzstan", label: "Kyrgyzstan" },
  { value: "Honduras", label: "Honduras" },
  { value: "Mauritius", label: "Mauritius" },
  { value: "Ethiopia", label: "Ethiopia" },
  { value: "Senegal", label: "Senegal" },
  { value: "Tanzania", label: "Tanzania" },
  { value: "Angola", label: "Angola" },
  { value: "Tajikistan", label: "Tajikistan" },
  {
    value: "Democratic Republic of the Con",
    label: "Democratic Republic of the Con",
  },
  { value: "Sudan", label: "Sudan" },
];

export const regionFilter = [
  { value: "---", label: "---" },
  { value: "New York", label: "New York" },
  { value: "Tokyo", label: "Tokyo" },
  { value: "England", label: "England" },
  { value: "California", label: "California" },
  { value: "Île-de-France", label: "Île-de-France" },
  { value: "Illinois", label: "Illinois" },
  { value: "Massachusetts", label: "Massachusetts" },
  { value: "Ontario", label: "Ontario" },
  { value: "Victoria", label: "Victoria" },
  { value: "Berlin", label: "Berlin" },
  { value: "Seoul", label: "Seoul" },
  { value: "New South Wales", label: "New South Wales" },
  { value: "Washington", label: "Washington" },
  { value: "Texas", label: "Texas" },
  { value: "Georgia", label: "Georgia" },
  { value: "Florida", label: "Florida" },
  { value: "Catalonia", label: "Catalonia" },
  { value: "Quebec", label: "Quebec" },
  { value: "Pennsylvania", label: "Pennsylvania" },
  { value: "Vienna", label: "Vienna" },
  { value: "Beijing", label: "Beijing" },
  { value: "Bavaria", label: "Bavaria" },
  { value: "Community of Madrid", label: "Community of Madrid" },
  { value: "Lombardy", label: "Lombardy" },
  { value: "North Holland", label: "North Holland" },
  { value: "Dubai", label: "Dubai" },
  { value: "Stockholm County", label: "Stockholm County" },
  { value: "Shanghai", label: "Shanghai" },
  { value: "Colorado", label: "Colorado" },
  { value: "British Columbia", label: "British Columbia" },
  { value: "Moscow", label: "Moscow" },
  { value: "Oregon", label: "Oregon" },
  { value: "Nevada", label: "Nevada" },
  { value: "Tel Aviv District", label: "Tel Aviv District" },
  { value: "Oslo", label: "Oslo" },
  { value: "Istanbul Province", label: "Istanbul Province" },
  { value: "Lazio", label: "Lazio" },
  { value: "Hamburg", label: "Hamburg" },
  { value: "Capital Region of Denmark", label: "Capital Region of Denmark" },
  { value: "Queensland", label: "Queensland" },
  { value: "Maryland", label: "Maryland" },
  { value: "Leinster", label: "Leinster" },
  { value: "Guangdong", label: "Guangdong" },
  { value: "Michigan", label: "Michigan" },
  { value: "Åland Islands", label: "Åland Islands" },
  { value: "Arizona", label: "Arizona" },
  { value: "canton of Zürich", label: "canton of Zürich" },
  { value: "Brandenburg", label: "Brandenburg" },
  { value: "Tennessee", label: "Tennessee" },
  { value: "São Paulo", label: "São Paulo" },
  { value: "New Jersey", label: "New Jersey" },
  { value: "Buenos Aires", label: "Buenos Aires" },
  { value: "Bangkok", label: "Bangkok" },
  { value: "Abu Dhabi Emirate", label: "Abu Dhabi Emirate" },
  { value: "North Rhine-Westphalia", label: "North Rhine-Westphalia" },
  { value: "Attica Region", label: "Attica Region" },
  { value: "Budapest", label: "Budapest" },
  { value: "Kuala Lumpur", label: "Kuala Lumpur" },
  { value: "Baden-Württemberg", label: "Baden-Württemberg" },
  { value: "South Holland", label: "South Holland" },
  { value: "Maharashtra", label: "Maharashtra" },
  { value: "North Carolina", label: "North Carolina" },
  { value: "Mexico City", label: "Mexico City" },
  { value: "Alberta", label: "Alberta" },
  { value: "Busan", label: "Busan" },
  { value: "Lisbon District", label: "Lisbon District" },
  { value: "Rhode Island", label: "Rhode Island" },
  { value: "Scotland", label: "Scotland" },
  { value: "Hawaii", label: "Hawaii" },
  { value: "Western Australia", label: "Western Australia" },
  { value: "Indiana", label: "Indiana" },
  { value: "Utah", label: "Utah" },
  { value: "Saxony", label: "Saxony" },
  { value: "Canton of Geneva", label: "Canton of Geneva" },
  { value: "Louisiana", label: "Louisiana" },
  { value: "Ohio", label: "Ohio" },
  { value: "Masovian Voivodeship", label: "Masovian Voivodeship" },
  { value: "Rio de Janeiro", label: "Rio de Janeiro" },
  { value: "Wisconsin", label: "Wisconsin" },
  { value: "Jakarta", label: "Jakarta" },
  { value: "Community of Valencia", label: "Community of Valencia" },
  { value: "Kentucky", label: "Kentucky" },
  { value: "Delhi", label: "Delhi" },
  { value: "New Mexico", label: "New Mexico" },
  { value: "Virginia", label: "Virginia" },
  { value: "Provence-Alpes-Côte d'Azur", label: "Provence-Alpes-Côte d'Azur" },
  { value: "Basel-Stadt", label: "Basel-Stadt" },
  { value: "Iowa", label: "Iowa" },
  {
    value: "Santiago Metropolitan Region",
    label: "Santiago Metropolitan Region",
  },
  { value: "North Brabant", label: "North Brabant" },
  { value: "Karnataka", label: "Karnataka" },
  {
    value: "Basque Autonomous Community",
    label: "Basque Autonomous Community",
  },
  { value: "Fukuoka Prefecture", label: "Fukuoka Prefecture" },
  { value: "South Australia", label: "South Australia" },
  { value: "Northern Ireland", label: "Northern Ireland" },
  { value: "Tuscany", label: "Tuscany" },
  {
    value: "Australian Capital Territory",
    label: "Australian Capital Territory",
  },
  { value: "South Carolina", label: "South Carolina" },
  { value: "Cundinamarca Department", label: "Cundinamarca Department" },
  { value: "Bucharest", label: "Bucharest" },
  { value: "Bremen", label: "Bremen" },
  { value: "Wales", label: "Wales" },
  { value: "Utrecht", label: "Utrecht" },
  { value: "Salzburg", label: "Salzburg" },
  { value: "Manitoba", label: "Manitoba" },
  { value: "Västra Götaland County", label: "Västra Götaland County" },
  { value: "Canton of Luxembourg", label: "Canton of Luxembourg" },
  { value: "Oklahoma", label: "Oklahoma" },
  { value: "Nebraska", label: "Nebraska" },
  { value: "Connecticut", label: "Connecticut" },
  { value: "Incheon", label: "Incheon" },
  { value: "Pays de la Loire", label: "Pays de la Loire" },
  { value: "Idaho", label: "Idaho" },
  { value: "New Hampshire", label: "New Hampshire" },
  { value: "Skåne County", label: "Skåne County" },
  { value: "Nova Scotia", label: "Nova Scotia" },
  { value: "canton of Bern", label: "canton of Bern" },
  { value: "Alabama", label: "Alabama" },
  { value: "Harju County", label: "Harju County" },
  { value: "Arkansas", label: "Arkansas" },
  { value: "Maine", label: "Maine" },
  { value: "Tamil Nadu", label: "Tamil Nadu" },
  { value: "Mississippi", label: "Mississippi" },
  { value: "Jerusalem District", label: "Jerusalem District" },
  { value: "Chongqing", label: "Chongqing" },
  { value: "Zhejiang", label: "Zhejiang" },
  { value: "Tianjin", label: "Tianjin" },
  { value: "Hubei", label: "Hubei" },
  { value: "Jiangsu", label: "Jiangsu" },
  { value: "Styria", label: "Styria" },
  { value: "Emilia-Romagna", label: "Emilia-Romagna" },
  { value: "Brittany", label: "Brittany" },
  { value: "Andalusia", label: "Andalusia" },
  { value: "Sofia City Province", label: "Sofia City Province" },
  { value: "Lesser Poland Voivodeship", label: "Lesser Poland Voivodeship" },
  { value: "Kanagawa Prefecture", label: "Kanagawa Prefecture" },
  { value: "Schleswig-Holstein", label: "Schleswig-Holstein" },
  { value: "Canton of Lucerne", label: "Canton of Lucerne" },
  { value: "Alaska", label: "Alaska" },
  { value: "Central Macedonia", label: "Central Macedonia" },
  { value: "Kansas", label: "Kansas" },
  { value: "Delaware", label: "Delaware" },
  { value: "Upper Austria", label: "Upper Austria" },
  { value: "Liège", label: "Liège" },
  { value: "Nuevo León", label: "Nuevo León" },
  { value: "Missouri", label: "Missouri" },
  { value: "Riyadh Region", label: "Riyadh Region" },
  { value: "Doha", label: "Doha" },
  { value: "Veneto", label: "Veneto" },
  { value: "Riga", label: "Riga" },
  { value: "Western Cape", label: "Western Cape" },
  { value: "Tasmania", label: "Tasmania" },
  { value: "Pomeranian Voivodeship", label: "Pomeranian Voivodeship" },
  { value: "Capital Region", label: "Capital Region" },
  { value: "Silesian Voivodeship", label: "Silesian Voivodeship" },
  { value: "Haifa District", label: "Haifa District" },
  { value: "Ulsan", label: "Ulsan" },
  { value: "Lima Province", label: "Lima Province" },
  { value: "Region of Southern Denmark", label: "Region of Southern Denmark" },
  { value: "Minnesota", label: "Minnesota" },
  { value: "Sichuan", label: "Sichuan" },
  { value: "Fujian", label: "Fujian" },
  { value: "Vilnius County", label: "Vilnius County" },
  {
    value: "Ljubljana City Municipality",
    label: "Ljubljana City Municipality",
  },
  { value: "Western Province", label: "Western Province" },
  { value: "Capital Governorate", label: "Capital Governorate" },
  { value: "Kagoshima Prefecture", label: "Kagoshima Prefecture" },
  { value: "Central Banat District", label: "Central Banat District" },
  { value: "Flemish Brabant", label: "Flemish Brabant" },
  { value: "South Moravian Region", label: "South Moravian Region" },
  { value: "Paraná", label: "Paraná" },
  { value: "Sharjah Emirate", label: "Sharjah Emirate" },
  { value: "Minas Gerais", label: "Minas Gerais" },
  { value: "New Brunswick", label: "New Brunswick" },
  { value: "North Dakota", label: "North Dakota" },
  { value: "Navarre", label: "Navarre" },
  { value: "South Dakota", label: "South Dakota" },
  { value: "Liaoning", label: "Liaoning" },
  { value: "Vermont", label: "Vermont" },
  { value: "Shandong", label: "Shandong" },
  { value: "Friuli–Venezia Giulia", label: "Friuli–Venezia Giulia" },
  { value: "West Bengal", label: "West Bengal" },
  { value: "Gauteng", label: "Gauteng" },
  { value: "Makkah Region", label: "Makkah Region" },
  { value: "Jalisco", label: "Jalisco" },
  { value: "Gujarat", label: "Gujarat" },
  { value: "Rio Grande do Sul", label: "Rio Grande do Sul" },
  { value: "Shaanxi", label: "Shaanxi" },
  { value: "Yunnan", label: "Yunnan" },
  { value: "Heilongjiang", label: "Heilongjiang" },
  { value: "Jiangxi", label: "Jiangxi" },
  { value: "Shanxi", label: "Shanxi" },
  { value: "Bratislava Region", label: "Bratislava Region" },
  { value: "Zagreb", label: "Zagreb" },
  { value: "Shizuoka Prefecture", label: "Shizuoka Prefecture" },
  { value: "Antalya Province", label: "Antalya Province" },
  { value: "Bahia", label: "Bahia" },
  { value: "Córdoba Province", label: "Córdoba Province" },
  { value: "Montana", label: "Montana" },
  { value: "Sindh", label: "Sindh" },
  { value: "Puebla", label: "Puebla" },
  { value: "Ceará", label: "Ceará" },
  { value: "Henan", label: "Henan" },
  { value: "Tehran Province", label: "Tehran Province" },
  { value: "Timiș County", label: "Timiș County" },
  { value: "Pernambuco", label: "Pernambuco" },
  { value: "Hanoi", label: "Hanoi" },
  { value: "Alexandria Governorate", label: "Alexandria Governorate" },
  { value: "Wyoming", label: "Wyoming" },
  { value: "Anhui", label: "Anhui" },
  { value: "Košice Region", label: "Košice Region" },
  { value: "Rajasthan", label: "Rajasthan" },
  { value: "Montevideo Department", label: "Montevideo Department" },
  { value: "Tbilisi", label: "Tbilisi" },
  { value: "Selangor", label: "Selangor" },
  { value: "Muscat Governorate", label: "Muscat Governorate" },
  { value: "Almaty", label: "Almaty" },
  { value: "Greater Skopje", label: "Greater Skopje" },
  { value: "Baku", label: "Baku" },
  { value: "Beirut Governorate", label: "Beirut Governorate" },
  { value: "KwaZulu-Natal", label: "KwaZulu-Natal" },
  { value: "Chișinău Municipality", label: "Chișinău Municipality" },
  { value: "Amman Governorate", label: "Amman Governorate" },
  { value: "Casablanca-Settat", label: "Casablanca-Settat" },
  { value: "Punjab", label: "Punjab" },
  { value: "Dubrovnik-Neretva County", label: "Dubrovnik-Neretva County" },
  {
    value: "Federation of Bosnia and Herze",
    label: "Federation of Bosnia and Herze",
  },
  { value: "Nairobi County", label: "Nairobi County" },
  { value: "Canary Islands", label: "Canary Islands" },
  { value: "Valparaíso", label: "Valparaíso" },
  { value: "Uttar Pradesh", label: "Uttar Pradesh" },
  { value: "Krasnoyarsk Krai", label: "Krasnoyarsk Krai" },
  { value: "Samara Oblast", label: "Samara Oblast" },
  { value: "Perm Krai", label: "Perm Krai" },
  { value: "Ajman Emirate", label: "Ajman Emirate" },
  { value: "Volgograd Oblast", label: "Volgograd Oblast" },
  { value: "Dhaka Division", label: "Dhaka Division" },
  { value: "Primorsky Krai", label: "Primorsky Krai" },
  { value: "West Java", label: "West Java" },
  { value: "Omsk Oblast", label: "Omsk Oblast" },
  { value: "Capital District", label: "Capital District" },
  { value: "Minsk", label: "Minsk" },
  { value: "Odessa Oblast", label: "Odessa Oblast" },
  { value: "San Salvador Department", label: "San Salvador Department" },
  { value: "Kingston Parish", label: "Kingston Parish" },
  { value: "Tirana County", label: "Tirana County" },
  { value: "Tomsk Oblast", label: "Tomsk Oblast" },
  { value: "Ulaanbaatar", label: "Ulaanbaatar" },
  { value: "Bolívar Department", label: "Bolívar Department" },
  { value: "Saratov Oblast", label: "Saratov Oblast" },
  { value: "Rabat-Salé-Kénitra", label: "Rabat-Salé-Kénitra" },
  { value: "Lviv Oblast", label: "Lviv Oblast" },
  { value: "San Juan Province", label: "San Juan Province" },
  { value: "La Paz Department", label: "La Paz Department" },
  { value: "Orenburg Oblast", label: "Orenburg Oblast" },
  { value: "Lagos", label: "Lagos" },
  { value: "Altai Krai", label: "Altai Krai" },
  { value: "New Providence", label: "New Providence" },
  { value: "Tunis Governorate", label: "Tunis Governorate" },
  { value: "Port of Spain", label: "Port of Spain" },
  { value: "Udmurt Republic", label: "Udmurt Republic" },
  { value: "Algiers Province", label: "Algiers Province" },
  { value: "Bishkek", label: "Bishkek" },
  {
    value: "Francisco Morazán Department",
    label: "Francisco Morazán Department",
  },
  { value: "Port Louis District", label: "Port Louis District" },
  { value: "Donetsk Oblast", label: "Donetsk Oblast" },
  { value: "Addis Ababa", label: "Addis Ababa" },
  { value: "Dakar", label: "Dakar" },
  { value: "Dar es Salaam Region", label: "Dar es Salaam Region" },
  { value: "Luanda Province", label: "Luanda Province" },
  {
    value: "districts of Republican Subord",
    label: "districts of Republican Subord",
  },
  { value: "Rivers State", label: "Rivers State" },
  { value: "Kinshasa", label: "Kinshasa" },
  { value: "Khartoum", label: "Khartoum" },
];

export const timezoneFilter = [
  { value: "---", label: "---" },
  { value: "America__New_York", label: "America__New_York" },
  { value: "Asia__Tokyo", label: "Asia__Tokyo" },
  { value: "Europe__London", label: "Europe__London" },
  { value: "America__Los_Angeles", label: "America__Los_Angeles" },
  { value: "Europe__Paris", label: "Europe__Paris" },
  { value: "America__Chicago", label: "America__Chicago" },
  { value: "America__Toronto", label: "America__Toronto" },
  { value: "Australia__Melbourne", label: "Australia__Melbourne" },
  { value: "Europe__Berlin", label: "Europe__Berlin" },
  { value: "Asia__Seoul", label: "Asia__Seoul" },
  { value: "Australia__Sydney", label: "Australia__Sydney" },
  { value: "Europe__Madrid", label: "Europe__Madrid" },
  { value: "Europe__Vienna", label: "Europe__Vienna" },
  { value: "Asia__Shanghai", label: "Asia__Shanghai" },
  { value: "Europe__Rome", label: "Europe__Rome" },
  { value: "Europe__Amsterdam", label: "Europe__Amsterdam" },
  { value: "Asia__Dubai", label: "Asia__Dubai" },
  { value: "Europe__Stockholm", label: "Europe__Stockholm" },
  { value: "America__Denver", label: "America__Denver" },
  { value: "America__Vancouver", label: "America__Vancouver" },
  { value: "Europe__Moscow", label: "Europe__Moscow" },
  { value: "Asia__Jerusalem", label: "Asia__Jerusalem" },
  { value: "Europe__Oslo", label: "Europe__Oslo" },
  { value: "Europe__Istanbul", label: "Europe__Istanbul" },
  { value: "Europe__Copenhagen", label: "Europe__Copenhagen" },
  { value: "Australia__Brisbane", label: "Australia__Brisbane" },
  { value: "Europe__Dublin", label: "Europe__Dublin" },
  { value: "America__Detroit", label: "America__Detroit" },
  { value: "Europe__Helsinki", label: "Europe__Helsinki" },
  { value: "America__Phoenix", label: "America__Phoenix" },
  { value: "Europe__Zurich", label: "Europe__Zurich" },
  { value: "America__Sao_Paulo", label: "America__Sao_Paulo" },
  { value: "America__Argentina__", label: "America__Argentina__" },
  { value: "Asia__Bangkok", label: "Asia__Bangkok" },
  { value: "Europe__Athens", label: "Europe__Athens" },
  { value: "Europe__Budapest", label: "Europe__Budapest" },
  { value: "Asia__Kuala_Lumpur", label: "Asia__Kuala_Lumpur" },
  { value: "Asia__Kolkata", label: "Asia__Kolkata" },
  { value: "America__Mexico_City", label: "America__Mexico_City" },
  { value: "America__Edmonton", label: "America__Edmonton" },
  { value: "Europe__Lisbon", label: "Europe__Lisbon" },
  { value: "Pacific__Honolulu", label: "Pacific__Honolulu" },
  { value: "Australia__Perth", label: "Australia__Perth" },
  { value: "America__Indiana__In", label: "America__Indiana__In" },
  { value: "Europe__Warsaw", label: "Europe__Warsaw" },
  { value: "Asia__Jakarta", label: "Asia__Jakarta" },
  { value: "America__Kentucky__L", label: "America__Kentucky__L" },
  { value: "America__Santiago", label: "America__Santiago" },
  { value: "Australia__Adelaide", label: "Australia__Adelaide" },
  { value: "America__Bogota", label: "America__Bogota" },
  { value: "Europe__Bucharest", label: "Europe__Bucharest" },
  { value: "America__Winnipeg", label: "America__Winnipeg" },
  { value: "Europe__Luxembourg", label: "Europe__Luxembourg" },
  { value: "America__Boise", label: "America__Boise" },
  { value: "America__Halifax", label: "America__Halifax" },
  { value: "Europe__Tallinn", label: "Europe__Tallinn" },
  { value: "Europe__Sofia", label: "Europe__Sofia" },
  { value: "America__Anchorage", label: "America__Anchorage" },
  { value: "Europe__Brussels", label: "Europe__Brussels" },
  { value: "America__Monterrey", label: "America__Monterrey" },
  { value: "Asia__Riyadh", label: "Asia__Riyadh" },
  { value: "Asia__Qatar", label: "Asia__Qatar" },
  { value: "Europe__Riga", label: "Europe__Riga" },
  { value: "Africa__Johannesburg", label: "Africa__Johannesburg" },
  { value: "Australia__Hobart", label: "Australia__Hobart" },
  { value: "Atlantic__Reykjavik", label: "Atlantic__Reykjavik" },
  { value: "America__Lima", label: "America__Lima" },
  { value: "Europe__Vilnius", label: "Europe__Vilnius" },
  { value: "Europe__Ljubljana", label: "Europe__Ljubljana" },
  { value: "Asia__Colombo", label: "Asia__Colombo" },
  { value: "Asia__Kuwait", label: "Asia__Kuwait" },
  { value: "Europe__Belgrade", label: "Europe__Belgrade" },
  { value: "Europe__Prague", label: "Europe__Prague" },
  { value: "America__Moncton", label: "America__Moncton" },
  { value: "Europe__Bratislava", label: "Europe__Bratislava" },
  { value: "Europe__Zagreb", label: "Europe__Zagreb" },
  { value: "America__Bahia", label: "America__Bahia" },
  { value: "Asia__Karachi", label: "Asia__Karachi" },
  { value: "America__Fortaleza", label: "America__Fortaleza" },
  { value: "Asia__Tehran", label: "Asia__Tehran" },
  { value: "America__Recife", label: "America__Recife" },
  { value: "Africa__Cairo", label: "Africa__Cairo" },
  { value: "America__Montevideo", label: "America__Montevideo" },
  { value: "Asia__Tbilisi", label: "Asia__Tbilisi" },
  { value: "Asia__Muscat", label: "Asia__Muscat" },
  { value: "Asia__Almaty", label: "Asia__Almaty" },
  { value: "Asia__Bahrain", label: "Asia__Bahrain" },
  { value: "Europe__Skopje", label: "Europe__Skopje" },
  { value: "Asia__Baku", label: "Asia__Baku" },
  { value: "Asia__Beirut", label: "Asia__Beirut" },
  { value: "Europe__Chisinau", label: "Europe__Chisinau" },
  { value: "Asia__Amman", label: "Asia__Amman" },
  { value: "Africa__Casablanca", label: "Africa__Casablanca" },
  { value: "Europe__Sarajevo", label: "Europe__Sarajevo" },
  { value: "Africa__Nairobi", label: "Africa__Nairobi" },
  { value: "Atlantic__Canary", label: "Atlantic__Canary" },
  { value: "Asia__Krasnoyarsk", label: "Asia__Krasnoyarsk" },
  { value: "Europe__Samara", label: "Europe__Samara" },
  { value: "Asia__Yekaterinburg", label: "Asia__Yekaterinburg" },
  { value: "Europe__Volgograd", label: "Europe__Volgograd" },
  { value: "Asia__Dhaka", label: "Asia__Dhaka" },
  { value: "Asia__Vladivostok", label: "Asia__Vladivostok" },
  { value: "Asia__Omsk", label: "Asia__Omsk" },
  { value: "America__Caracas", label: "America__Caracas" },
  { value: "Europe__Minsk", label: "Europe__Minsk" },
  { value: "Europe__Kiev", label: "Europe__Kiev" },
  { value: "America__El_Salvador", label: "America__El_Salvador" },
  { value: "America__Jamaica", label: "America__Jamaica" },
  { value: "Europe__Tirane", label: "Europe__Tirane" },
  { value: "Asia__Tomsk", label: "Asia__Tomsk" },
  { value: "Asia__Ulaanbaatar", label: "Asia__Ulaanbaatar" },
  { value: "Europe__Saratov", label: "Europe__Saratov" },
  { value: "America__La_Paz", label: "America__La_Paz" },
  { value: "Africa__Lagos", label: "Africa__Lagos" },
  { value: "Asia__Barnaul", label: "Asia__Barnaul" },
  { value: "America__Nassau", label: "America__Nassau" },
  { value: "Africa__Tunis", label: "Africa__Tunis" },
  { value: "America__Asuncion", label: "America__Asuncion" },
  { value: "America__Port_of_Spa", label: "America__Port_of_Spa" },
  { value: "Africa__Algiers", label: "Africa__Algiers" },
  { value: "Asia__Bishkek", label: "Asia__Bishkek" },
  { value: "America__Tegucigalpa", label: "America__Tegucigalpa" },
  { value: "Indian__Mauritius", label: "Indian__Mauritius" },
  { value: "Africa__Addis_Ababa", label: "Africa__Addis_Ababa" },
  { value: "Africa__Dakar", label: "Africa__Dakar" },
  { value: "Africa__Dar_es_Salaa", label: "Africa__Dar_es_Salaa" },
  { value: "Africa__Luanda", label: "Africa__Luanda" },
  { value: "Asia__Dushanbe", label: "Asia__Dushanbe" },
  { value: "Africa__Kinshasa", label: "Africa__Kinshasa" },
  { value: "Africa__Khartoum", label: "Africa__Khartoum" },
];
