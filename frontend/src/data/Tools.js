import ReactLogo from "../images/About/ReactLogo.png"
import AWSLogo from "../images/About/AWSLogo.png"
import DockerLogo from "../images/About/DockerLogo.png"
import PostmanLogo from "../images/About/PostmanLogo.png"
import GitLabLogo from "../images/About/GitLabLogo.png"
import FlaskLogo from "../images/About/FlaskLogo.jpg"
import SlackLogo from "../images/About/SlackLogo.png"
import GeoDBLogo from "../images/About/GeoDBLogo.png"
import AmadeusLogo from "../images/About/AmadeusLogo.png"
import OpenTripMapLogo from '../images/About/OpenTripMapLogo.png'
import PrettierLogo from '../images/About/PrettierLogo.png'
import JestLogo from '../images/About/JestLogo.png'
import SeleniumLogo from '../images/About/SeleniumLogo.png'
import MySQLLogo from '../images/About/MySQLLogo.png'
import SQLAlchemyLogo from '../images/About/SQLAlchemyLogo.png'
import MarshmallowLogo from '../images/About/MarshmallowLogo.png'
import BlackLogo from '../images/About/BlackLogo.png'
import WikipediaLogo from '../images/About/WikipediaLogo.png'
import UnsplashLogo from '../images/About/UnsplashLogo.png'
import D3Logo from '../images/About/D3Logo.png'
import RechartsLogo from '../images/About/RechartsLogo.png'
import YouTubeLogo from '../images/About/YouTubeLogo.png'

export const toolsInfo = [
    {
		title: "React",
		img: ReactLogo,
		link: "https://reactjs.org/",
		description: "JavaScript library for front-end development"
	},
	{
		title: "Flask",
		img: FlaskLogo,
		link: "https://flask.palletsprojects.com/en/1.1.x/",
		description: "Python framework for back-end development"
	},
	{
		title: "Prettier",
		img: PrettierLogo,
		link: "https://prettier.io/",
		description: "JS code formatter"
	},
	{
		title: "SQLAlchemy",
		img: SQLAlchemyLogo,
		link: "https://www.sqlalchemy.org/",
		description: "Object-relational mapper for Python"
	},
	{
		title: "Black",
		img: BlackLogo,
		link: "https://github.com/psf/black",
		description: "Python code formatter"
	},
	{
		title: "Jest",
		img: JestLogo,
		link: "https://jestjs.io/",
		description: "JS testing framework"
	},
	{
		title: "Selenium",
		img: SeleniumLogo,
		link: "https://www.selenium.dev/",
		description: "UI testing framework"
	},
	{
		title: "Postman",
		img: PostmanLogo,
		link: "https://postman.com/",
		description: "API testing and designing software"
	},
	{
		title: "AWS",
		img: AWSLogo,
		link: "https://aws.amazon.com/",
		description: "Cloud hosting platform"
	},
	{
		title: "Docker",
		img: DockerLogo,
		link: "https://docker.com/",
		description: "Containerization for consistent runtime environments"
	},
	{
		title: "GitLab",
		img: GitLabLogo,
		link: "https://gitlab.com/",
		description: "Git repository; CI/CD"
	},
	{
		title: "Slack",
		img: SlackLogo,
		link: "https://slack.com/",
		description: "Platform for team communication"
	},
	{
		title: "Marshmallow",
		img: MarshmallowLogo,
		link: "https://marshmallow.readthedocs.io/en/stable/",
		description: "Library for JSON serialization"
	},
	{
		title: "Recharts",
		img: RechartsLogo,
		link: "https://recharts.org/en-US/",
		description: "Visualization/charting library on top of D3"
	},
	{
		title: "D3",
		img: D3Logo,
		link: "https://d3js.org/",
		description: "JS visualizations"
	},
	{
		title: "MySQL",
		img: MySQLLogo,
		link: "https://www.mysql.com/",
		description: "Relational database management system"
	},
]

export const travvyInfo = [
	{
		title: "Travvy Explorers Postman",
		img: PostmanLogo,
		link: "https://documenter.getpostman.com/view/5094658/Tz5jfLYw",
		description: "Documentation for Travvy Explorers API"
	},
	{
		title: "Travvy Explorers GitLab",
		img: GitLabLogo,
		link: "https://gitlab.com/ryanarifin134/travvy-explorers",
		description: "Repository for front-end and back-end code"
	},
	{
		title: "Travvy Explorers Video",
		img: YouTubeLogo,
		link: "https://www.youtube.com/watch?v=LmOMGAJM890",
		description: "YouTube presentation video"
	},
]

export const apiInfo = [
	{
		title: "GeoDB",
		img: GeoDBLogo,
		link: "http://geodb-cities-api.wirefreethought.com/",
		description: "Information about cities"
	},
	{
		title: "Amadeus",
		img: AmadeusLogo,
		link: "https://developers.amadeus.com/self-service/category/hotel/api-doc/hotel-search",
		description: "Information about hotels"
	},
	{
		title: "OpenTripMap",
		img: OpenTripMapLogo,
		link: "https://opentripmap.io/docs#/",
		description: "Information about attractions"
	},
	{
		title: "Unsplash",
		img: UnsplashLogo,
		link: "https://unsplash.com/",
		description: "Images for cities"
	},
	{
		title: "Wikipedia API",
		img: WikipediaLogo,
		link: "https://www.mediawiki.org/wiki/API:Main_page",
		description: "Descriptions for cities"
	}
]