const HOST_URL = 'https://www.travvyexplorers.com'

/* eslint-disable  @typescript-eslint/no-unused-vars */
function getHost(): string {
  return process.env.REACT_APP_API_URL || HOST_URL;
}

// TODO: FIX + refactor for query parameters!!
export async function getRequest<T>(
  path: string,
  page?: number | undefined
): Promise<T> {
  const host = getHost();

  const url = `${host}${path}`;
  // url = page ? `${url}?page=${page}` : url;
  const response = await fetch(url);

  return await response.json();
}
// TODO: FIX + refactor for query parameters!!
export async function getOtherRequest<T>(
  path: string,
  page?: number | undefined
): Promise<T> {

  const url = `${path}`;
  // url = page ? `${url}?page=${page}` : url;
  const response = await fetch(url);
  return await response.json();
}
