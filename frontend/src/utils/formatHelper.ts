export const formatNumber = (value: number): string => {
  const numberFormatter = new Intl.NumberFormat('en-US', { maximumFractionDigits: 2});

  return numberFormatter.format(value);
}