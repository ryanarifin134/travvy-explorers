# Travvy Explorers

CS 373 Project #4 Group 3

members (name: UTEID, GitLab ID)
- Ryan Arifin: raa2954, ryanarifin134
- Tristan Blake: tab3822, annoyed
- Pooja Chivukula: pc26626, poojachiv
- Jaden Hyde: jah8624, jadenh20
- Erika Tan: eat2335, erikaaatan

Git SHA: 432c57560a6b517b6a2ff650152344b1ee4f7d91

project leader: Pooja Chivukula

link to GitLab pipelines: https://gitlab.com/ryanarifin134/travvy-explorers/-/pipelines

link to website: https://www.travvyexplorers.com

link to video: https://www.youtube.com/watch?v=LmOMGAJM890

estimated completion time: (hours: int)
- Ryan Arifin: 11
- Tristan Blake: 12
- Pooja Chivukula: 11
- Jaden Hyde: 13
- Erika Tan: 12

actual completion time: (hours: int)
- Ryan Arifin: 15
- Tristan Blake: 17
- Pooja Chivukula: 14
- Jaden Hyde: 12
- Erika Tan: 17

comments: N/A
